<?php
ini_set('display_errors', 0);

class Controlleur
{
    public function __construct() {
        global $rep,$vues;

        try{
            $action=$_REQUEST['action'];
            switch($action){
                case NULL:
                    $this->homePage();
                    break;
                case "VoirScore":
                    $this->afficherScore();
                    break;
                default:
                    $dVueEreur[] =	"Erreur d'appel php";
                    require ($rep.$vues['error']);
                    break;
            }

        } catch (PDOException $e)
        {
            echo $e->getMessage();
            $dVueEreur[] =	"Erreur inattendue PDO!!! ";
            require ($rep.$vues['error']);

        }
        catch (Exception $e2)
        {
            $dVueEreur[] =	"Erreur inattendue!!! ";
            require ($rep.$vues['error']);
        }
    }

    public function homePage(){
        global $vues, $rep;

        require($rep . $vues['homePage']);
    }






    public function afficherScore()
    {
        global $vues, $rep;
       try {
           $listPlayer = new ModelScore();
           $listP = $listPlayer->getHScore();

       } catch (Exception $e)
        {
            $listP = array("error" => $e->getMessage());
        }

        require($rep . $vues['score']);
    }



}