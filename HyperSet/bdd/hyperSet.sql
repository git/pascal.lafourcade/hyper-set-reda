PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE HighScore ( id int PRIMARY KEY, pseudo varchar2(50) NOT NULL, score int NOT NULL);
INSERT INTO HighScore VALUES(2,'Jack',80);
INSERT INTO HighScore VALUES(1,'redko',100);
COMMIT;
