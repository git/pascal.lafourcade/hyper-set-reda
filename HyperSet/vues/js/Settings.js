/*var color = 'normal';
var nb = 0;

function changeColorMode() {
    let selectColor = document.getElementById('color-selector');
    selectColor.addEventListener('change', function() {
        var index = selectColor.selectedIndex;
        // Rapporter cette donnée au <p>
        if (index != null){
            nb= index;
        }

    })
   /* if(color !== selectColor.value){
        color = selectColor.value;
    }
    return nb;

}


console.log(nb);
/*var nb = 0;

let selectColor = document.getElementById('color-selector');

selectColor.addEventListener('change', function() {
    var index = selectColor.selectedIndex;
    // Rapporter cette donnée au <p>
    if (index != null){
        nb= index;
    }

})



 */
/*var mode;

let selectMode = document.getElementById('mode-selector');

selectMode.addEventListener('change', function() {
    var index = selectMode.selectedIndex;
    // Rapporter cette donnée au <p>
    if (index != null){
        mode= index;
    }

});*/


var popupSettings = document.getElementById('popupSettings');
var popupExplain = document.getElementById('popupExplain');

window.onclick = function(event) {
    if (event.target === popupSettings) {
        popupSettings.style.display = "none";
        saveSettings();
    }
    if (event.target === popupExplain) {
        popupExplain.style.display = "none";
    }
};

function showSettings(){
    document.getElementById('popupSettings').style.display='block';
}

function showExplain(){
    document.getElementById('popupExplain').style.display='block';
}


function goHome(){
    document.getElementById('home').style.display='block';
    document.getElementById('selectMode').style.display='none';
}





function showGameHS(){
    document.getElementById('selectModeHyperSet').style.display='none';
    document.getElementById('selectModeHyperSet3x4').style.display='block';

}
function showGameHS0(){
    document.getElementById('selectModeHyperSet').style.display='none';
    document.getElementById('selectModeHyperSet3x3').style.display='block';

}

function showGameHS1(){
    document.getElementById('selectModeHyperSet').style.display='none';
    document.getElementById('selectModeHyperSet3x5').style.display='block';

}
function showGameHS2(){
    document.getElementById('selectModeHyperSet').style.display='none';
    document.getElementById('selectModeHyperSet4x4').style.display='block';

}
function showGameHS3(){
    document.getElementById('selectModeHyperSet').style.display='none';
    document.getElementById('selectModeHyperSet4x5').style.display='block';

}
function showGameHS4(){
    document.getElementById('selectModeHyperSet').style.display='none';
    document.getElementById('selectModeHyperSet5x5').style.display='block';
}




function showGame0(){
    document.getElementById('selectModeSet').style.display='none';
    document.getElementById('selectModeSet3x3').style.display='block';

}

function showGame(){
    document.getElementById('selectModeSet').style.display='none';
    document.getElementById('selectModeSet3x4').style.display='block';
}

function showGame1(){
    document.getElementById('selectModeSet').style.display='none';
    document.getElementById('selectModeSet3x5').style.display='block';

}

function showGame2(){
    document.getElementById('selectModeSet').style.display='none';
    document.getElementById('selectModeSet4x4').style.display='block';


}
function showGame3(){
    document.getElementById('selectModeSet').style.display='none';
    document.getElementById('selectModeSet4x5').style.display='block';


}
function showGame4(){
    document.getElementById('selectModeSet').style.display='none';
    document.getElementById('selectModeSet5x5').style.display='block';

}




function showModeGame(){
    document.getElementById('selectModeSet').style.display='block';
    document.getElementById('selectMode').style.display='none';
}

function showModeGame1(){
    document.getElementById('selectModeHyperSet').style.display='block';
    document.getElementById('selectMode').style.display='none';
}



function closeGame(){
    window.location.reload();
}



function closeExplainPopup() {
    document.getElementById('popupExplain').style.display='none';
}

function closeSettingsPopup() {
    document.getElementById('popupSettings').style.display='none';
    saveSettings();
}


function saveSettings(){
    localStorage.setItem('isRootTheme', isRootTheme);
    localStorage.setItem('isBackgroundAnimated', isBackgroundAnimated);
    localStorage.setItem('lang', lang);

}


const html = document.getElementsByTagName('html')[0];
const themeSwitch = document.getElementById('themeSwitch');

themeSwitch.addEventListener('click', () => {
    html.classList.toggle('dark');

});

function setTheme() {
    if(!isRootTheme) {
        document.body.classList.toggle('dark');
        document.getElementById('themeSwitch').checked = true;
    }
    else{
        if(document.body.classList.contains('dark'))
            document.body.classList.toggle('dark');
        document.getElementById('themeSwitch').checked = false;

    }
}

function goBack(){
    document.getElementById('selectMode').style.display='block';
    document.getElementById('selectModeSet').style.display='none';
}
function goBack1(){
    document.getElementById('selectMode').style.display='block';
    document.getElementById('selectModeHyperSet').style.display='none';
}



function goSelect0(){
    document.getElementById('selectModeSet').style.display='block';
    document.getElementById('selectModeSet3x3').style.display='none';
}
function goSelect(){
    document.getElementById('selectModeSet').style.display='block';
    document.getElementById('selectModeSet3x4').style.display='none';
}
function goSelect1(){
    document.getElementById('selectModeSet').style.display='block';
    document.getElementById('selectModeSet3x5').style.display='none';
}
function goSelect2(){
    document.getElementById('selectModeSet').style.display='block';
    document.getElementById('selectModeSet4x4').style.display='none';
}
function goSelect3(){
    document.getElementById('selectModeSet').style.display='block';
    document.getElementById('selectModeSet4x5').style.display='none';
}
function goSelect4(){
    document.getElementById('selectModeSet').style.display='block';
    document.getElementById('selectModeSet5x5').style.display='none';
}




function showGameA() {
    document.getElementById('game0').style.display='block';
    document.getElementById('selectModeSet3x3').style.display='none';
    Game.deal0();
}
function showGameB() {
    document.getElementById('game').style.display='block';
    document.getElementById('selectModeSet3x4').style.display='none';
    Game.deal();
}
function showGameC() {
    document.getElementById('game1').style.display='block';
    document.getElementById('selectModeSet3x5').style.display='none';

    Game.deal1();
}
function showGameD() {
    document.getElementById('game2').style.display='block';
    document.getElementById('selectModeSet4x4').style.display='none';

    Game.deal2();
}
function showGameE() {
    document.getElementById('game3').style.display='block';
    document.getElementById('selectModeSet4x5').style.display='none';

    Game.deal3();
}
function showGameF() {
    document.getElementById('game4').style.display='block';
    document.getElementById('selectModeSet5x5').style.display='none';
    Game.deal4();
}


function goSelectMode(){
    window.location.reload();
}


function goSelectModeHyperSet(){
    window.location.reload();
}


function showGameA2() {
    document.getElementById('game0').style.display='block';
    document.getElementById('selectModeSet3x3').style.display='none';
    document.getElementById('progressBar0').style.display = 'none';
    Game.deal0A();
}
function showGameB2() {
    document.getElementById('game').style.display='block';
    document.getElementById('selectModeSet3x4').style.display='none';
    document.getElementById('progressBar').style.display = 'none';
    Game.dealA();
}
function showGameC2() {
    document.getElementById('game1').style.display='block';
    document.getElementById('selectModeSet3x5').style.display='none';
    document.getElementById('progressBar1').style.display = 'none';

    Game.deal1A();
}
function showGameD2() {
    document.getElementById('game2').style.display='block';
    document.getElementById('selectModeSet4x4').style.display='none';
    document.getElementById('progressBar2').style.display = 'none';

    Game.deal2A();
}
function showGameE2() {
    document.getElementById('game3').style.display='block';
    document.getElementById('selectModeSet4x5').style.display='none';
    document.getElementById('progressBar3').style.display = 'none';

    Game.deal3A();
}
function showGameF2() {
    document.getElementById('game4').style.display='block';
    document.getElementById('selectModeSet5x5').style.display='none';
    document.getElementById('progressBar4').style.display = 'none';
    Game.deal4A();
}

function showGameHSA2() {
    document.getElementById('gameHS0').style.display='block';
    document.getElementById('selectModeHyperSet3x3').style.display='none';
    document.getElementById('progressBarHS0').style.display = 'none';
    Game.dealHS0A();
}
function showGameHSB2() {
    document.getElementById('gameHS').style.display='block';
    document.getElementById('selectModeHyperSet3x4').style.display='none';
    document.getElementById('progressBarHS').style.display = 'none';
    Game.dealHSA();
}
function showGameHSC2() {
    document.getElementById('gameHS1').style.display='block';
    document.getElementById('selectModeHyperSet3x5').style.display='none';
    document.getElementById('progressBarHS1').style.display = 'none';

    Game.dealHS1A();
}
function showGameHSD2() {
    document.getElementById('gameHS2').style.display='block';
    document.getElementById('selectModeHyperSet4x4').style.display='none';
    document.getElementById('progressBarHS2').style.display = 'none';

    Game.dealHS2A();
}
function showGameHSE2() {
    document.getElementById('gameHS3').style.display='block';
    document.getElementById('selectModeHyperSet4x5').style.display='none';
    document.getElementById('progressBarHS3').style.display = 'none';

    Game.dealHS3A();
}
function showGameHSF2() {
    document.getElementById('gameHS4').style.display='block';
    document.getElementById('selectModeHyperSet5x5').style.display='none';
    document.getElementById('progressBarHS4').style.display = 'none';
    Game.dealHS4A();
}

function showGameHSA() {
    document.getElementById('gameHS0').style.display='block';
    document.getElementById('selectModeHyperSet3x3').style.display='none';

    Game.dealHS0();
}
function showGameHSB() {
    document.getElementById('gameHS').style.display='block';
    document.getElementById('selectModeHyperSet3x4').style.display='none';

    Game.dealHS();
}
function showGameHSC() {
    document.getElementById('gameHS1').style.display='block';
    document.getElementById('selectModeHyperSet3x5').style.display='none';

    Game.dealHS1();
}
function showGameHSD() {
    document.getElementById('gameHS2').style.display='block';
    document.getElementById('selectModeHyperSet4x4').style.display='none';

    Game.dealHS2();
}
function showGameHSE() {
    document.getElementById('gameHS3').style.display='block';
    document.getElementById('selectModeHyperSet4x5').style.display='none';

    Game.dealHS3();
}
function showGameHSF() {
    document.getElementById('gameHS4').style.display='block';
    document.getElementById('selectModeHyperSet5x5').style.display='none';

    Game.dealHS4();
}

function goSelectHS0(){
    document.getElementById('selectModeHyperSet').style.display='block';
    document.getElementById('selectModeHyperSet3x3').style.display='none';
}
function goSelectHS(){
    document.getElementById('selectModeHyperSet').style.display='block';
    document.getElementById('selectModeHyperSet3x4').style.display='none';
}
function goSelectHS1(){
    document.getElementById('selectModeHyperSet').style.display='block';
    document.getElementById('selectModeHyperSet3x5').style.display='none';
}
function goSelectHS2(){
    document.getElementById('selectModeHyperSet').style.display='block';
    document.getElementById('selectModeHyperSet4x4').style.display='none';
}
function goSelectHS3(){
    document.getElementById('selectModeHyperSet').style.display='block';
    document.getElementById('selectModeHyperSet4x5').style.display='none';
}
function goSelectHS4(){
    document.getElementById('selectModeHyperSet').style.display='block';
    document.getElementById('selectModeHyperSet5x5').style.display='none';
}



    function getValuescore() {
    // Sélectionner l'élément input et récupérer sa valeur
    var input = document.getElementById("in").value;
    // Afficher la valeur
    if(input > 27){
        Game.$nbScore0.html(9 + ' Sets');
        Game.nbScore0 = 9;
    }
    else if(input < 1){
        Game.$nbScore0.html(1 + ' Sets');
        Game.nbScore0 = 1;
    }
    else {
        Game.$nbScore0.html(input + ' Sets');
        Game.nbScore0 = input;
    }
}