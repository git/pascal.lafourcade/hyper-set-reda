
var Game = {
	cards: [],
	selected: [],
	score: 0,
	set: 0,
	$board: $('[data-display="game-board"]'),
	$score: $('[data-display="score"]'),
	$nbSets: $('[data-display="nbSets"]'),
    $nbScore: $('[data-display="nbScore"]'),



    cards1 : [],
	selected1: [],
	score1: 0 ,
	set1: 0,
	$score1: $('[data-display="score1"]'),
	$nbSets1: $('[data-display="nbSets1"]'),
	$board1: $('[data-display="game-board1"]'),
    $nbScore1: $('[data-display="nbScore1"]'),

	cards2 : [],
	selected2: [],
	score2: 0 ,
	set2: 0,
	$score2: $('[data-display="score2"]'),
	$nbSets2: $('[data-display="nbSets2"]'),
	$board2: $('[data-display="game-board2"]'),
    $nbScore2: $('[data-display="nbScore2"]'),


	cards3 : [],
	selected3: [],
	score3: 0 ,
	set3: 0,
	$score3: $('[data-display="score3"]'),
	$nbSets3: $('[data-display="nbSets3"]'),
	$board3: $('[data-display="game-board3"]'),
    $nbScore3: $('[data-display="nbScore3"]'),


	cards4: [],
	selected4 : [],
	score4: 0,
	set4: 0,
	$score4: $('[data-display="score4"]'),
	$nbSets4: $('[data-display="nbSets4"]'),
	$board4: $('[data-display="game-board4"]'),
    $nbScore4: $('[data-display="nbScore4"]'),

	cards0: [],
	selected0 : [],
	score0: 0,
	set0: 0,
	$score0: $('[data-display="score0"]'),
	$nbSets0: $('[data-display="nbSets0"]'),
	$board0: $('[data-display="game-board0"]'),
    $nbScore0: $('[data-display="nbScore0"]'),
    nbScore0: 9,

	cardsHS: [],
	selectedHS : [],
	scoreHS: 0,
	setHS: 0,
	$scoreHS: $('[data-display="scoreHS"]'),
	$nbSetsHS: $('[data-display="nbSetsHS"]'),
	$boardHS: $('[data-display="game-boardHS"]'),
    $nbScoreHS: $('[data-display="nbScoreHS"]'),

	cardsHS0: [],
	selectedHS0 : [],
	scoreHS0: 0,
	setHS0: 0,
	$scoreHS0: $('[data-display="scoreHS0"]'),
	$nbSetsHS0: $('[data-display="nbSetsHS0"]'),
	$boardHS0: $('[data-display="game-boardHS0"]'),
    $nbScoreHS0: $('[data-display="nbScoreHS0"]'),

	cardsHS1: [],
	selectedHS1 : [],
	scoreHS1: 0,
	setHS1: 0,
	$scoreHS1: $('[data-display="scoreHS1"]'),
	$nbSetsHS1: $('[data-display="nbSetsHS1"]'),
	$boardHS1: $('[data-display="game-boardHS1"]'),
    $nbScoreHS1: $('[data-display="nbScoreHS1"]'),

	cardsHS2: [],
	selectedHS2 : [],
	scoreHS2: 0,
	setHS2: 0,
	$scoreHS2: $('[data-display="scoreHS2"]'),
	$nbSetsHS2: $('[data-display="nbSetsHS2"]'),
	$boardHS2: $('[data-display="game-boardHS2"]'),
    $nbScoreHS2: $('[data-display="nbScoreHS2"]'),



    cardsHS3: [],
    selectedHS3 : [],
    scoreHS3: 0,
    setHS3: 0,
    $scoreHS3: $('[data-display="scoreHS3"]'),
    $nbSetsHS3: $('[data-display="nbSetsHS3"]'),
    $boardHS3: $('[data-display="game-boardHS3"]'),
    $nbScoreHS3: $('[data-display="nbScoreHS3"]'),


    cardsHS4: [],
    selectedHS4 : [],
    scoreHS4: 0,
    setHS4: 0,
    $scoreHS4: $('[data-display="scoreHS4"]'),
    $nbSetsHS4: $('[data-display="nbSetsHS4"]'),
    $boardHS4: $('[data-display="game-boardHS4"]'),
    $nbScoreHS4: $('[data-display="nbScoreHS4"]'),







    deal: function() {
        var self = this;
        var dealRequest;
        self.selected = [];
		self.score = 0;
		self.set = 0;
		self.$board=  $('[data-display="game-board"]');
		self.$score = $('[data-display="score"]');
		self.$nbSets = $('[data-display="nbSets"]');


			// ajax request to get initial set of cards
        dealRequest = $.ajax({
            url: 'set.php?action=deal',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cards = data;
                self.existingSet();
                self.displayCards.call(self);
                self.setCardListeners();


                function progress(timeleft, timetotal, $element) {
                    var progressBarWidth = timeleft * $element.width() / timetotal;
                    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
                    if(timeleft > 0) {
                        setTimeout(function() {
                            progress(timeleft - 1, timetotal, $element);
                        }, 1000);
                    }
                }

                progress(481, 481, $('#progressBar'));

                const startingMinutes = 8;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdown');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    if(time > 0 ) {
                        countdownEl.innerHTML = minutes + ' : ' + seconds;
                    }
                    if(time === -1  ){
                        countdownEl.innerHTML = ' TIME OVER !';

                    }
                    if( time === -2){
                        window.location.reload();
                    }
                    time--;

                }
            }
        });
    },




    deal1: function() {
        var self = this;
        var dealRequest1;
		self.cards1 = [];
		self.selected1= [];
		self.score1= 0 ;
		self.set1= 0;
		self.$score1= $('[data-display="score1"]');
		self.$nbSets1= $('[data-display="nbSets1"]');
		self.$board1= $('[data-display="game-board1"]');

        dealRequest1 = $.ajax({
            url: 'set.php?action=deal1',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cards1 = data;
                self.existingSet1();
                self.displayCards1.call(self);
                self.setCardListeners1();

                function progress(timeleft, timetotal, $element) {
                    var progressBarWidth = timeleft * $element.width() / timetotal;
                    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
                    if(timeleft > 0) {
                        setTimeout(function() {
                            progress(timeleft - 1, timetotal, $element);
                        }, 1000);
                    }
                }

                progress(720, 720, $('#progressBar1'));

                const startingMinutes = 12;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdown1');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    if(time > 0 ) {
                        countdownEl.innerHTML = minutes + ' : ' + seconds;
                    }
                    if(time === -1  ){
                        countdownEl.innerHTML = 'TIME OVER !';
                    }
                    if(time === -2){
                        document.getElementById('GameOver').style.display='block';

                    }
                    time--;
                }

            }
        });

    },


    deal2: function() {
        var dealRequest2;
        var self = this;
		self.cards2 = [];
		self.selected2= [];
		self.score2= 0 ;
		self.set2= 0;
		self.$score2= $('[data-display="score2"]');
		self.$nbSets2= $('[data-display="nbSets2"]');
		self.$board2= $('[data-display="game-board2"]');

        dealRequest2 = $.ajax({
            url: 'set.php?action=deal2',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cards2 = data;
                self.displayCards2.call(self);
                self.existingSet2();
                self.setCardListeners2();
                function progress(timeleft, timetotal, $element) {
                    var progressBarWidth = timeleft * $element.width() / timetotal;
                    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
                    if(timeleft > 0) {
                        setTimeout(function() {
                            progress(timeleft - 1, timetotal, $element);
                        }, 1000);
                    }
                }

                progress(961, 961, $('#progressBar2'));

                const startingMinutes = 16;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdown2');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    if(time > 0 ) {
                        countdownEl.innerHTML = minutes + ' : ' + seconds;
                    }
                    if(time === -1  ){
                        countdownEl.innerHTML = ' TIME OVER !';
                    }
                    if(time === -2){
                        document.getElementById('GameOver').style.display='block';

                    }
                    time--;
                }
            }
        });

    },

    deal3: function() {
        var dealRequest3;
        var self = this;
		self.cards3 = [];
		self.selected3= [];
		self.score3= 0 ;
		self.set3= 0;
		self.$score3= $('[data-display="score3"]');
		self.$nbSets3= $('[data-display="nbSets3"]');
		self.$board3= $('[data-display="game-board3"]');
        dealRequest3 = $.ajax({
            url: 'set.php?action=deal3',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cards3 = data;
                self.displayCards3.call(self);
                self.existingSet3();
                self.setCardListeners3();

                function progress(timeleft, timetotal, $element) {
                    var progressBarWidth = timeleft * $element.width() / timetotal;
                    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
                    if(timeleft > 0) {
                        setTimeout(function() {
                            progress(timeleft - 1, timetotal, $element);
                        }, 1000);
                    }
                }

                progress(1201, 1201, $('#progressBar3'));

                const startingMinutes = 20;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdown3');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    if(time > 0 ) {
                        countdownEl.innerHTML = minutes + ' : ' + seconds;
                    }
                    if(time === -1  ){
                        countdownEl.innerHTML = ' TIME OVER !';
                    }
                    if(time === -2){
                        document.getElementById('GameOver').style.display='block';

                    }
                    time--;
                }
            }
        });
    },

    deal4: function() {
        var dealRequest4;
        var self = this;
		self.cards4 = [];
		self.selected4= [];
		self.score4= 0 ;
		self.set4= 0;
		self.$score4= $('[data-display="score4"]');
		self.$nbSets4= $('[data-display="nbSets4"]');
		self.$board4= $('[data-display="game-board4"]');
        dealRequest4 = $.ajax({
            url: 'set.php?action=deal4',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cards4 = data;
                self.displayCards4.call(self);
                self.existingSet4();
                self.setCardListeners4();

                function progress(timeleft, timetotal, $element) {
                    var progressBarWidth = timeleft * $element.width() / timetotal;
                    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
                    if(timeleft > 0) {
                        setTimeout(function() {
                            progress(timeleft - 1, timetotal, $element);
                        }, 1000);
                    }
                }

                progress(1801, 1801, $('#progressBar4'));

                const startingMinutes = 30;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdown4');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    if(time > 0 ) {
                        countdownEl.innerHTML = minutes + ' : ' + seconds;
                    }
                    if(time === -1  ){
                        countdownEl.innerHTML = ' TIME OVER !';
                    }
                    if(time === -2){
                        document.getElementById('GameOver').style.display='block';

                    }
                    time--;
                }
            }
        });

    },

    deal0: function() {
        var dealRequest0;
        var self = this;
		self.cards0 = [];
		self.selected0= [];
		self.score0= 0 ;
		self.set0= 0;
		self.$score0= $('[data-display="score0"]');
		self.$nbSets0= $('[data-display="nbSets0"]');
		self.$board0= $('[data-display="game-board0"]');
        dealRequest0 = $.ajax({
            url: 'set.php?action=deal0',
            type: 'GET',
            dataType: 'json',
            success: function (data) {

                self.cards0 = data;
                self.displayCards0.call(self);
                self.existingSet0();


                self.setCardListeners0();
				function progress(timeleft, timetotal, $element) {
					var progressBarWidth = timeleft * $element.width() / timetotal;
					$element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
					if(timeleft > 0) {
						setTimeout(function() {
							progress(timeleft - 1, timetotal, $element);
						}, 1000);
					}
				}

				progress(180, 180, $('#progressBar0'));

				const startingMinutes = 3;
				let time = startingMinutes *60;

				const countdownEl = document.getElementById('countdown0');
				setInterval(updateCountDown,1000);



				function updateCountDown() {
					const minutes = Math.floor(time / 60);
					let seconds = time % 60;
					seconds = seconds < 10 ? '0' + seconds : seconds;
					if(time > 0 ) {
						countdownEl.innerHTML = minutes + ' : ' + seconds;
					}
					if(time === -1  ){
						countdownEl.innerHTML = ' TIME OVER !';
					}
                    if(time === -2){
                        document.getElementById('GameOver').style.display='block';;

                    }
					time--;
				}

            }
        });

    },
    dealHS: function() {
        var dealRequestHS;
        var self = this;
		self.cardsHS = [];
		self.selectedHS= [];
		self.scoreHS= 0 ;
		self.setHS= 0;
		self.$scoreHS= $('[data-display="scoreHS"]');
		self.$nbSetsHS= $('[data-display="nbSetsHS"]');
		self.$boardHS= $('[data-display="game-boardHS"]');

        dealRequestHS= $.ajax({
            url: 'set.php?action=dealHS',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cardsHS = data;
                self.existingHyperSet();
                self.displayCardsHS.call(self);
                self.setCardListenersHS();

                function progress(timeleft, timetotal, $element) {
                    var progressBarWidth = timeleft * $element.width() / timetotal;
                    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
                    if(timeleft > 0) {
                        setTimeout(function() {
                            progress(timeleft - 1, timetotal, $element);
                        }, 1000);
                    }
                }

                progress(540, 540, $('#progressBarHS'));

                const startingMinutes = 9;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    if(time > 0 ) {
                        countdownEl.innerHTML = minutes + ' : ' + seconds;
                    }
                    if(time === -1  ){
                        countdownEl.innerHTML = ' TIME OVER !';
                    }
                    if(time === -2){
                        document.getElementById('GameOver').style.display='block';

                    }
                    time--;
                }
            }
        });

    },
    dealHS0: function() {
        var self = this;
        var dealRequestHS0;
		self.cardsHS0 = [];
		self.selectedHS0= [];
		self.scoreHS0= 0 ;
		self.setHS0= 0;
		self.$scoreHS0= $('[data-display="scoreHS0"]');
		self.$nbSetsHS0= $('[data-display="nbSetsHS0"]');
		self.$boardHS0= $('[data-display="game-boardHS0"]');
        dealRequestHS0= $.ajax({
            url: 'set.php?action=dealHS0',
            type: 'GET',
            dataType: 'json',
            success: function (data) {


                self.cardsHS0 = data;
                self.existingHyperSet0();
                self.displayCardsHS0.call(self);
                self.setCardListenersHS0();

                function progress(timeleft, timetotal, $element) {
                    var progressBarWidth = timeleft * $element.width() / timetotal;
                    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
                    if(timeleft > 0) {
                        setTimeout(function() {
                            progress(timeleft - 1, timetotal, $element);
                        }, 1000);
                    }
                }

                progress(240, 240, $('#progressBarHS0'));

                const startingMinutes = 4;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS0');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    if(time > 0 ) {
                        countdownEl.innerHTML = minutes + ' : ' + seconds;
                    }
                    if(time === -1  ){
                        countdownEl.innerHTML = ' TIME OVER !';
                    }
                    if(time === -2){
                        document.getElementById('GameOver').style.display='block';

                    }
                    time--;
                }
            }
        });
    },
    dealHS1: function() {
        var self = this;
        var dealRequestHS1;
		self.cardsHS1 = [];
		self.selectedHS1= [];
		self.scoreHS1= 0 ;
		self.setHS1= 0;
		self.$scoreHS1= $('[data-display="scoreHS1"]');
		self.$nbSetsHS1= $('[data-display="nbSetsHS1"]');
		self.$boardHS1= $('[data-display="game-boardHS1"]');
        dealRequestHS1= $.ajax({
            url: 'set.php?action=dealHS1',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cardsHS1 = data;
                self.existingHyperSet1();
                self.displayCardsHS1.call(self);
                self.setCardListenersHS1();

                function progress(timeleft, timetotal, $element) {
                    var progressBarWidth = timeleft * $element.width() / timetotal;
                    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
                    if(timeleft > 0) {
                        setTimeout(function() {
                            progress(timeleft - 1, timetotal, $element);
                        }, 1000);
                    }
                }

                progress(900, 900, $('#progressBarHS1'));

                const startingMinutes = 15;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS1');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    if(time > 0 ) {
                        countdownEl.innerHTML = minutes + ' : ' + seconds;
                    }
                    if(time === -1  ){
                        countdownEl.innerHTML = ' TIME OVER !';
                    }
                    if(time === -2){
                        document.getElementById('GameOver').style.display='block';

                    }
                    time--;
                }
            }
        });
    },

    dealHS2: function() {
        var dealRequestHS2;
        var self = this;
		self.cardsHS2 = [];
		self.selectedHS2= [];
		self.scoreHS2= 0 ;
		self.setHS2= 0;
		self.$scoreHS2= $('[data-display="scoreHS2"]');
		self.$nbSetsHS2= $('[data-display="nbSetsHS2"]');
		self.$boardHS2= $('[data-display="game-boardHS2"]');

        dealRequestHS2= $.ajax({
            url: 'set.php?action=dealHS2',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cardsHS2 = data;
                self.existingHyperSet2();
                self.displayCardsHS2.call(self);
                self.setCardListenersHS2();

                function progress(timeleft, timetotal, $element) {
                    var progressBarWidth = timeleft * $element.width() / timetotal;
                    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
                    if(timeleft > 0) {
                        setTimeout(function() {
                            progress(timeleft - 1, timetotal, $element);
                        }, 1000);
                    }
                }

                progress(1080, 1080, $('#progressBarHS2'));

                const startingMinutes = 18;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS2');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    if(time > 0 ) {
                        countdownEl.innerHTML = minutes + ' : ' + seconds;
                    }
                    if(time === -1  ){
                        countdownEl.innerHTML = ' TIME OVER !';
                    }
                    if(time === -2){
                        document.getElementById('GameOver').style.display='block';

                    }
                    time--;
                }
            }
        });
    },
    dealHS3: function() {
        var dealRequestHS3;
        var self = this;
		self.cardsHS3 = [];
		self.selectedHS3= [];
		self.scoreHS3= 0 ;
		self.setHS3= 0;
		self.$scoreHS3= $('[data-display="scoreHS3"]');
		self.$nbSetsHS3= $('[data-display="nbSetsHS3"]');
		self.$boardHS3= $('[data-display="game-boardHS3"]');

        dealRequestHS3= $.ajax({
            url: 'set.php?action=dealHS3',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cardsHS3 = data;
                self.existingHyperSet3();
                self.displayCardsHS3.call(self);
                self.setCardListenersHS3();

                function progress(timeleft, timetotal, $element) {
                    var progressBarWidth = timeleft * $element.width() / timetotal;
                    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
                    if(timeleft > 0) {
                        setTimeout(function() {
                            progress(timeleft - 1, timetotal, $element);
                        }, 1000);
                    }
                }

                progress(1380, 1380, $('#progressBarHS3'));

                const startingMinutes = 23;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS3');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    if(time > 0 ) {
                        countdownEl.innerHTML = minutes + ' : ' + seconds;
                    }
                    if(time === -1  ){
                        countdownEl.innerHTML = ' TIME OVER !';
                    }
                    if(time === -2){
                        document.getElementById('GameOver').style.display='block';

                    }
                    time--;
                }
            }
        });

    },
    dealHS4: function() {
        var dealRequestHS4;
        var self = this;
		self.cardsHS4 = [];
		self.selectedHS4= [];
		self.scoreHS4= 0 ;
		self.setHS4= 0;
		self.$scoreHS4= $('[data-display="scoreHS4"]');
		self.$nbSetsHS4= $('[data-display="nbSetsHS4"]');
		self.$boardHS4= $('[data-display="game-boardHS4"]');

        dealRequestHS4= $.ajax({
            url: 'set.php?action=dealHS4',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cardsHS4 = data;
                self.existingHyperSet4();
                self.displayCardsHS4.call(self);
                self.setCardListenersHS4();
                function progress(timeleft, timetotal, $element) {
                    var progressBarWidth = timeleft * $element.width() / timetotal;
                    $element.find('div').animate({ width: progressBarWidth }, timeleft == timetotal ? 0 : 1000, "linear");
                    if(timeleft > 0) {
                        setTimeout(function() {
                            progress(timeleft - 1, timetotal, $element);
                        }, 1000);
                    }
                }

                progress(2700, 2700, $('#progressBarHS'));

                const startingMinutes = 45;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    if(time > 0 ) {
                        countdownEl.innerHTML = minutes + ' : ' + seconds;
                    }
                    if(time === -1  ){
                        countdownEl.innerHTML = ' TIME OVER !';
                    }
                    if(time === -2){
                        document.getElementById('GameOver').style.display='block';

                    }
                    time--;
                }
            }
        });

    },


    dealA: function() {
        var self = this;
        var dealRequest;
        self.selected = [];
        self.score = 0;
        self.set = 0;
        self.$board=  $('[data-display="game-board"]');
        self.$score = $('[data-display="score"]');
        self.$nbSets = $('[data-display="nbSets"]');


        // ajax request to get initial set of cards
        dealRequest = $.ajax({
            url: 'set.php?action=deal',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cards = data;
                self.existingSet();
                self.displayCards.call(self);
                self.setCardListeners();


                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdown');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }
            }
        });
    },




    deal1A: function() {
        var self = this;
        var dealRequest1;
        self.cards1 = [];
        self.selected1= [];
        self.score1= 0 ;
        self.set1= 0;
        self.$score1= $('[data-display="score1"]');
        self.$nbSets1= $('[data-display="nbSets1"]');
        self.$board1= $('[data-display="game-board1"]');

        dealRequest1 = $.ajax({
            url: 'set.php?action=deal1',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cards1 = data;
                self.existingSet1();
                self.displayCards1.call(self);
                self.setCardListeners1();

                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdown1');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }

            }
        });

    },


    deal2A: function() {
        var dealRequest2;
        var self = this;
        self.cards2 = [];
        self.selected2= [];
        self.score2= 0 ;
        self.set2= 0;
        self.$score2= $('[data-display="score2"]');
        self.$nbSets2= $('[data-display="nbSets2"]');
        self.$board2= $('[data-display="game-board2"]');

        dealRequest2 = $.ajax({
            url: 'set.php?action=deal2',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cards2 = data;
                self.displayCards2.call(self);
                self.existingSet2();
                self.setCardListeners2();
                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdown2');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }
            }
        });

    },

    deal3A: function() {
        var dealRequest3;
        var self = this;
        self.cards3 = [];
        self.selected3= [];
        self.score3= 0 ;
        self.set3= 0;
        self.$score3= $('[data-display="score3"]');
        self.$nbSets3= $('[data-display="nbSets3"]');
        self.$board3= $('[data-display="game-board3"]');
        dealRequest3 = $.ajax({
            url: 'set.php?action=deal3',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cards3 = data;
                self.displayCards3.call(self);
                self.existingSet3();
                self.setCardListeners3();

                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdown3');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }
            }
        });
    },

    deal4A: function() {
        var dealRequest4;
        var self = this;
        self.cards4 = [];
        self.selected4= [];
        self.score4= 0 ;
        self.set4= 0;
        self.$score4= $('[data-display="score4"]');
        self.$nbSets4= $('[data-display="nbSets4"]');
        self.$board4= $('[data-display="game-board4"]');
        dealRequest4 = $.ajax({
            url: 'set.php?action=deal4',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cards4 = data;
                self.displayCards4.call(self);
                self.existingSet4();
                self.setCardListeners4();

                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdown4');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }
            }
        });

    },

    deal0A: function() {
        var dealRequest0;
        var self = this;
        self.cards0 = [];
        self.selected0= [];
        self.score0= 0 ;
        self.set0= 0;
        self.$score0= $('[data-display="score0"]');
        self.$nbSets0= $('[data-display="nbSets0"]');
        self.$board0= $('[data-display="game-board0"]');

        dealRequest0 = $.ajax({
            url: 'set.php?action=deal0',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                document.getElementById('nbsc0').style.display = 'none';


                self.cards0 = data;
                self.displayCards0.call(self);
                self.existingSet0();

                self.setCardListeners0();
                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdown0');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }


            }
        });

    },
    dealHSA: function() {
        var dealRequestHS;
        var self = this;
        self.cardsHS = [];
        self.selectedHS= [];
        self.scoreHS= 0 ;
        self.setHS= 0;
        self.$scoreHS= $('[data-display="scoreHS"]');
        self.$nbSetsHS= $('[data-display="nbSetsHS"]');
        self.$boardHS= $('[data-display="game-boardHS"]');

        dealRequestHS= $.ajax({
            url: 'set.php?action=dealHS',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cardsHS = data;
                self.existingHyperSet();
                self.displayCardsHS.call(self);
                self.setCardListenersHS();

                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }
            }
        });

    },

    dealHS0A: function() {
        var self = this;
        var dealRequestHS0;
        self.cardsHS0 = [];
        self.selectedHS0= [];
        self.scoreHS0= 0 ;
        self.setHS0= 0;
        self.$scoreHS0= $('[data-display="scoreHS0"]');
        self.$nbSetsHS0= $('[data-display="nbSetsHS0"]');
        self.$boardHS0= $('[data-display="game-boardHS0"]');
        dealRequestHS0= $.ajax({
            url: 'set.php?action=dealHS0',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cardsHS0 = data;
                self.existingHyperSet0();
                self.displayCardsHS0.call(self);
                self.setCardListenersHS0();

                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS0');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }
            }
        });
    },
    dealHS1A: function() {
        var self = this;
        var dealRequestHS1;
        self.cardsHS1 = [];
        self.selectedHS1= [];
        self.scoreHS1= 0 ;
        self.setHS1= 0;
        self.$scoreHS1= $('[data-display="scoreHS1"]');
        self.$nbSetsHS1= $('[data-display="nbSetsHS1"]');
        self.$boardHS1= $('[data-display="game-boardHS1"]');
        dealRequestHS1= $.ajax({
            url: 'set.php?action=dealHS1',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cardsHS1 = data;
                self.existingHyperSet1();
                self.displayCardsHS1.call(self);
                self.setCardListenersHS1();

                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS1');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }
            }
        });
    },

    dealHS2A: function() {
        var dealRequestHS2;
        var self = this;
        self.cardsHS2 = [];
        self.selectedHS2= [];
        self.scoreHS2= 0 ;
        self.setHS2= 0;
        self.$scoreHS2= $('[data-display="scoreHS2"]');
        self.$nbSetsHS2= $('[data-display="nbSetsHS2"]');
        self.$boardHS2= $('[data-display="game-boardHS2"]');

        dealRequestHS2= $.ajax({
            url: 'set.php?action=dealHS2',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cardsHS2 = data;
                self.existingHyperSet2();
                self.displayCardsHS2.call(self);
                self.setCardListenersHS2();

                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS2');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }
            }
        });
    },
    dealHS3A: function() {
        var dealRequestHS3;
        var self = this;
        self.cardsHS3 = [];
        self.selectedHS3= [];
        self.scoreHS3= 0 ;
        self.setHS3= 0;
        self.$scoreHS3= $('[data-display="scoreHS3"]');
        self.$nbSetsHS3= $('[data-display="nbSetsHS3"]');
        self.$boardHS3= $('[data-display="game-boardHS3"]');

        dealRequestHS3= $.ajax({
            url: 'set.php?action=dealHS3',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cardsHS3 = data;
                self.existingHyperSet3();
                self.displayCardsHS3.call(self);
                self.setCardListenersHS3();

                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS3');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }
            }
        });

    },
    dealHS4A: function() {
        var dealRequestHS4;
        var self = this;
        self.cardsHS4 = [];
        self.selectedHS4= [];
        self.scoreHS4= 0 ;
        self.setHS4= 0;
        self.$scoreHS4= $('[data-display="scoreHS4"]');
        self.$nbSetsHS4= $('[data-display="nbSetsHS4"]');
        self.$boardHS4= $('[data-display="game-boardHS4"]');

        dealRequestHS4= $.ajax({
            url: 'set.php?action=dealHS4',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                self.cardsHS4 = data;
                self.existingHyperSet4();
                self.displayCardsHS4.call(self);
                self.setCardListenersHS4();
                const startingMinutes = 0;
                let time = startingMinutes *60;

                const countdownEl = document.getElementById('countdownHS4');
                setInterval(updateCountDown,1000);



                function updateCountDown() {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;
                    seconds = seconds < 10 ? '0' + seconds : seconds;
                    countdownEl.innerHTML = minutes + ' : ' + seconds;
                    time++;

                }
            }
        });

    },










    displayCards: function() {
		var self = this;
		if (self.cards.length == 0) {
			return false;
		}



        this.$score.html(this.score);
		$.each(self.cards, function(index, card){
			var cardNode = $('<div>', {
				class: "card"
			}).appendTo(self.$board);


			cardNode.data({
				'id': card.id,
				'shape': card.shape,
				'fill': card.fill,
				'color': card.color,
				'number': card.number
			});

			var shapeNode = $("<canvas></canvas>");
			shapeNode.addClass('shape ' + card.color + ' ' + card.shape + ' ' + card.fill);

			for (var i = 0; i < card.number; i++) {
				cardNode.append(shapeNode.clone());
			}
			self.$board.append(cardNode);

			// display 4 cards per row
			if ((index+1) % 3 === 0) {
				self.$board.append($('<div>'));
			}

		});


	},

	displayCards1: function() {
		var self = this;
		if (self.cards1.length == 0) {
			return false;
		}

        this.$score1.html(this.score1);
		$.each(self.cards1, function(index, card){
			var cardNode = $('<div>', {
				class: "card"
			}).appendTo(self.$board1);


			cardNode.data({
				'id': card.id,
				'shape': card.shape,
				'fill': card.fill,
				'color': card.color,
				'border': card.border,
				'number': card.number
			});

			var shapeNode = $("<canvas></canvas>");
			shapeNode.addClass('shape ' + card.color + ' ' + card.shape + ' ' + card.fill + ' ' + card.border);

			for (var i = 0; i < card.number; i++) {
				cardNode.append(shapeNode.clone());
			}
			self.$board1.append(cardNode);

			// display 4 cards per row
			if ((index+1) % 4 === 0) {
				self.$board1.append($('<div>'));
			}

		});

	},

	displayCards2: function() {
		var self = this;
		if (self.cards2.length == 0) {
			return false;
		}
        this.$score2.html(this.score2);

		$.each(self.cards2, function(index, card){
			var cardNode = $('<div>', {
				class: "card"
			}).appendTo(self.$board2);


			cardNode.data({
				'id': card.id,
				'shape': card.shape,
				'fill': card.fill,
				'color': card.color,
				'number': card.number
			});

			var shapeNode = $("<canvas></canvas>");
			shapeNode.addClass('shape ' + card.color + ' ' + card.shape + ' ' + card.fill );

			for (var i = 0; i < card.number; i++) {
				cardNode.append(shapeNode.clone());
			}
			self.$board2.append(cardNode);

			// display 4 cards per row
			if ((index+1) % 4 === 0) {
				self.$board2.append($('<div>'));
			}

		});

	},
	displayCards3: function() {
		var self = this;
		if (self.cards3.length == 0) {
			return false;
		}
        this.$score3.html(this.score3);
		$.each(self.cards3, function(index, card){
			var cardNode = $('<div>', {
				class: "card"
			}).appendTo(self.$board3);


			cardNode.data({
				'id': card.id,
				'shape': card.shape,
				'fill': card.fill,
				'color': card.color,
				'border': card.border,
				'number': card.number
			});

			var shapeNode = $("<div></div>");
			shapeNode.append(card.color + '<br /> ' + card.shape + '<br /> ' + card.fill + '<br /> ' + card.border + '<br /> '+ card.number);

			/*for (var i = 0; i < card.number; i++) {
				cardNode.append(shapeNode.clone());
			}*/
			cardNode.append(shapeNode);
			self.$board3.append(cardNode);

			// display 4 cards per row
			if ((index+1) % 8 === 0) {
				self.$board3.append($('<div>'));
			}

		});

	},
	displayCards4: function() {
		var self = this;
		if (self.cards4.length == 0) {
			return false;
		}

        this.$score4.html(this.score4);
		$.each(self.cards4, function(index, card){
			var cardNode = $('<div>', {
				class: "card"
			}).appendTo(self.$board4);


			cardNode.data({
				'id': card.id,
				'shape': card.shape,
				'fill': card.fill,
				'color': card.color,
				'border': card.border,
				'number': card.number
			});

			var shapeNode = $("<div></div>");
			shapeNode.append( card.color + ' <br />' + card.shape + '<br /> ' + card.fill + '<br /> ' + card.border + '<br /> ' + card.number);

			/*for (var i = 0; i < card.number; i++) {
				cardNode.append(shapeNode.clone());
			}*/
            cardNode.append(shapeNode);

            self.$board4.append(cardNode);

			// display 4 cards per row
			if ((index+1) %10 === 0) {
				self.$board4.append($('<div>'));
			}

		});

	},


	displayCards0: function() {
		var self = this;
		if (self.cards0.length == 0) {
			return false;
		}
        this.$score0.html(this.score0);

		$.each(self.cards0, function(index, card){
			var cardNode = $('<div>', {
				class: "card"
			}).appendTo(self.$board0);


			cardNode.data({
				'id': card.id,
				'shape': card.shape,
				'fill': card.fill,
				'color': card.color,
				'number': card.number
			});

			var shapeNode = $("<canvas></canvas>");
			shapeNode.addClass('shape ' + card.color + ' ' + card.shape + ' ' + card.fill);

			for (var i = 0; i < card.number; i++) {
				cardNode.append(shapeNode.clone());
			}
			self.$board0.append(cardNode);

			// display 4 cards per row
			if ((index+1) % 2 === 0) {
				self.$board0.append($('<div>'));
			}

		});

	},
	displayCardsHS: function() {
		var self = this;
		if (self.cardsHS.length == 0) {
			return false;
		}
        this.$scoreHS.html(this.scoreHS);

		$.each(self.cardsHS, function(index, card){
			var cardNode = $('<div>', {
				class: "card"
			}).appendTo(self.$boardHS);


			cardNode.data({
				'id': card.id,
				'shape': card.shape,
				'fill': card.fill,
				'color': card.color,
				'number': card.number
			});

			var shapeNode = $("<canvas></canvas>");
			shapeNode.addClass('shape ' + card.color + ' ' + card.shape + ' ' + card.fill);

			for (var i = 0; i < card.number; i++) {
				cardNode.append(shapeNode.clone());
			}
			self.$boardHS.append(cardNode);

			// display 4 cards per row
			if ((index+1) % 2 === 0) {
				self.$boardHS.append($('<div>'));
			}

		});



	},


	displayCardsHS0: function() {
		var self = this;
		if (self.cardsHS0.length == 0) {
			return false;
		}
        this.$scoreHS0.html(this.scoreHS0);

		$.each(self.cardsHS0, function(index, card){
			var cardNode = $('<div>', {
				class: "card"
			}).appendTo(self.$boardHS0);


			cardNode.data({
				'id': card.id,
				'shape': card.shape,
				'fill': card.fill,
				'color': card.color,
				'number': card.number
			});

			var shapeNode = $("<canvas></canvas>");
			shapeNode.addClass('shape ' + card.color + ' ' + card.shape + ' ' + card.fill);

			for (var i = 0; i < card.number; i++) {
				cardNode.append(shapeNode.clone());
			}
			self.$boardHS0.append(cardNode);

			// display 4 cards per row
			if ((index+1) % 2 === 0) {
				self.$boardHS0.append($('<div>'));
			}

		});

	},

	displayCardsHS1: function() {
		var self = this;
		if (self.cardsHS1.length == 0) {
			return false;
		}

        this.$scoreHS1.html(this.scoreHS1);
		$.each(self.cardsHS1, function(index, card){
			var cardNode = $('<div>', {
				class: "card"
			}).appendTo(self.$boardHS1);


			cardNode.data({
				'id': card.id,
				'shape': card.shape,
				'fill': card.fill,
				'color': card.color,
				'border': card.border,
				'number': card.number
			});

			var shapeNode = $("<canvas></canvas>");
			shapeNode.addClass('shape ' + card.color + ' ' + card.shape + ' ' + card.fill + ' '+ card.border);

			for (var i = 0; i < card.number; i++) {
				cardNode.append(shapeNode.clone());
			}
			self.$boardHS1.append(cardNode);

			// display 4 cards per row
			if ((index+1) % 3 === 0) {
				self.$boardHS1.append($('<div>'));
			}

		});
	},

	displayCardsHS2: function() {
		var self = this;
		if (self.cardsHS2.length == 0) {
			return false;
		}
        this.$scoreHS2.html(this.scoreHS2);

		$.each(self.cardsHS2, function(index, card){
			var cardNode = $('<div>', {
				class: "card"
			}).appendTo(self.$boardHS2);


			cardNode.data({
				'id': card.id,
				'shape': card.shape,
				'fill': card.fill,
				'color': card.color,
				'number': card.number
			});

			var shapeNode = $("<canvas></canvas>");
			shapeNode.addClass('shape ' + card.color + ' ' + card.shape + ' ' + card.fill);

			for (var i = 0; i < card.number; i++) {
				cardNode.append(shapeNode.clone());
			}
			self.$boardHS2.append(cardNode);

			// display 4 cards per row
			if ((index+1) % 4 === 0) {
				self.$boardHS2.append($('<div>'));
			}

		});

	},

    displayCardsHS3: function() {
        var self = this;
        if (self.cardsHS3.length == 0) {
            return false;
        }

        this.$scoreHS3.html(this.scoreHS3);

        $.each(self.cardsHS3, function(index, card){
            var cardNode = $('<div>', {
                class: "card"
            }).appendTo(self.$boardHS3);


            cardNode.data({
                'id': card.id,
                'shape': card.shape,
                'fill': card.fill,
                'color': card.color,
                'border': card.border,
                'number': card.number
            });

            var shapeNode = $("<div></div>");
            shapeNode.append(card.color + '<br /> ' + card.shape + '<br /> ' + card.fill + '<br /> ' + card.border + '<br /> '+ card.number);

            /*for (var i = 0; i < card.number; i++) {
                cardNode.append(shapeNode.clone());
            }*/
            cardNode.append(shapeNode);
            self.$boardHS3.append(cardNode);

            // display 4 cards per row
            if ((index+1) % 8 === 0) {
                self.$boardHS3.append($('<div>'));
            }

        });

    },

    displayCardsHS4: function() {
        var self = this;
        if (self.cardsHS4.length == 0) {
            return false;
        }
        this.$scoreHS4.html(this.scoreHS4);

        $.each(self.cardsHS4, function(index, card){
            var cardNode = $('<div>', {
                class: "card"
            }).appendTo(self.$boardHS4);


            cardNode.data({
                'id': card.id,
                'shape': card.shape,
                'fill': card.fill,
                'color': card.color,
                'border': card.border,
                'number': card.number
            });

            var shapeNode = $("<div></div>");
            shapeNode.append( card.color + ' <br/>' + card.shape + '<br/> ' + card.fill + '<br/> ' + card.border + '<br/> ' + card.number);

            /*for (var i = 0; i < card.number; i++) {
                cardNode.append(shapeNode.clone());
            }*/
            cardNode.append(shapeNode);

            self.$boardHS4.append(cardNode);

            // display 4 cards per row
            if ((index+1) %6 === 0) {
                self.$boardHS4.append($('<div>'));
            }
        });
    },


// PARLER SUR CET ALGO SUR LE RAPPORT


	/*existingSet: function() {
		var colors = [];
		var shapes =[];
		var fills= [];
		var numbers= [];
		var valid;
		var self = this;
		var cartes = self.cards;
		var cartes1 = cartes;
		var cartes2 = cartes;
		var cpt=0;


		var carte1;
		var carte2;
		var carte3;

		var i, j, k;



		for(i=0; i<cartes.length-2; i++) {
			for (j = i+1; j < cartes1.length-1; j++) {
				for (k = j+1; k < cartes2.length; k++) {
					carte1 = cartes[i];
					carte2 = cartes1[j];
					carte3 = cartes2[k];

					colors.splice(0,1,carte1.color);
					colors.splice(1,1,carte2.color);
					colors.splice(2,1,carte3.color);

					shapes.splice(0,1,carte1.shape);
					shapes.splice(1,1,carte2.shape);
					shapes.splice(2,1,carte3.shape);

					fills.splice(0,1,carte1.fill);
					fills.splice(1,1,carte2.fill);
					fills.splice(2,1,carte3.fill);

					numbers.splice(0,1,carte1.number);
					numbers.splice(1,1,carte2.number);
					numbers.splice(2,1,carte3.number);


					valid = self.isSet(colors) && self.isSet(shapes) && self.isSet(fills) && self.isSet(numbers);

					if (valid) {
						self.set = self.set + 1;
					}

				}
			}
		}
		this.$nbSets.html(self.set);
	},*/

	existingSet: function() {
		var colors1 = [];
		var shapes1 =[];
		var fills1= [];
		var numbers1= [];
		var self = this;
		var cartes = self.cards;
		var cartes1 = cartes;


		var carte1;
		var carte2;

		var card3 = [];

		var tCards =[];



		var i, j, a;

		var colorsall= $.map(this.cards, function(el) { return el.color;});
		var shapesall = $.map(this.cards, function(el) { return el.shape; });
		var fillsall= $.map(this.cards,function(el) { return el.fill; });
		var numbersall = $.map(this.cards, function(el) { return el.number;});


		for(i=0; i<cartes.length; i++) {
			for (j = i+1; j < cartes1.length; j++) {

				carte1 = cartes[i];
				carte2 = cartes1[j];

                tCards[0] = carte1;
				tCards[1]= carte2;

                card3 = this.generateVirtualCard34(tCards);

				colors1.splice(0,1,carte1.color);
				colors1.splice(1,1,carte2.color);
				colors1.splice(2, 1, card3[0].color);

				shapes1.splice(0,1,carte1.shape);
				shapes1.splice(1,1,carte2.shape);
				shapes1.splice(2, 1, card3[0].shape);


				fills1.splice(0,1,carte1.fill);
				fills1.splice(1,1,carte2.fill);
				fills1.splice(2, 1, card3[0].fill);

				numbers1.splice(0,1,carte1.number);
				numbers1.splice(1,1,carte2.number);
				numbers1.splice(2, 1, card3[0].number);



				for (a = j+1; a<this.cards.length; a++) {
                    if (shapes1[2] !== undefined  && colors1[2] !== undefined && fills1[2] !== undefined && numbers1[2] !== undefined) {
                        if (shapes1[2] === shapesall[a] && colors1[2] === colorsall[a] && fills1[2] === fillsall[a] && numbers1[2] === numbersall[a]) {
                            if (this.isSet(shapes1) && this.isSet(colors1) && this.isSet(fills1) && this.isSet(numbers1)) {
                                self.set = self.set + 1;
                            }
                        }
                    }
				}

			}

		}
		this.$nbSets.html(self.set);
	},

	existingSet0: function() {
		var colors1 = [];
		var shapes1 =[];
		var numbers1= [];
		var self = this;
		var cartes = self.cards0;
		var cartes1 = cartes;


		var carte1;
		var carte2;
		var card3 = [];

		var tCards = [];


		var i, j, a;

		var colorsall= $.map(this.cards0, function(el) { return el.color;});
		var shapesall = $.map(this.cards0, function(el) { return el.shape; });
		var numbersall = $.map(this.cards0, function(el) { return el.number;});


		for(i=0; i<cartes.length-1; i++) {
			for (j = i+1; j < cartes1.length; j++) {

				carte1 = cartes[i];
				carte2 = cartes1[j];

				tCards[0] = carte1;
				tCards[1]= carte2;

				card3 = this.generateVirtualCard33(tCards);

				colors1.splice(0,1,carte1.color);
				colors1.splice(1,1,carte2.color);
				colors1.splice(2, 1, card3[0].color);

				shapes1.splice(0,1,carte1.shape);
				shapes1.splice(1,1,carte2.shape);
				shapes1.splice(2, 1, card3[0].shape);

				numbers1.splice(0,1,carte1.number);
				numbers1.splice(1,1,carte2.number);
				numbers1.splice(2, 1, card3[0].number);



				for (a = j+1; a<this.cards0.length; a++) {
                    if (shapes1[2] !== undefined  && colors1[2] !== undefined && numbers1[2] !== undefined) {
                        if (shapes1[2] === shapesall[a] && colors1[2] === colorsall[a] && numbers1[2] === numbersall[a]) {
                            if (this.isSet(shapes1) && this.isSet(colors1) && this.isSet(numbers1)) {
                                self.set0 = self.set0 + 1;
                            }
                        }
                    }
				}

			}

		}

		this.$nbSets0.html(self.set0);
	},

	existingSet1: function() {
		var colors1 = [];
		var shapes1 =[];
		var fills1= [];
		var borders1= [];
		var numbers1= [];
		var self = this;
		var cartes = self.cards1;
		var cartes1 = cartes;


		var carte1, card3;
		var carte2;
		var tCards =[];

		var i, j, a;

		var colorsall= $.map(this.cards1, function(el) { return el.color;});
		var shapesall = $.map(this.cards1, function(el) { return el.shape; });
		var fillsall= $.map(this.cards1,function(el) { return el.fill; });
		var bordersall= $.map(this.cards1,function(el) { return el.border; });
		var numbersall = $.map(this.cards1, function(el) { return el.number;});


		for(i=0; i<cartes.length-1; i++) {
			for (j = i+1; j < cartes1.length; j++) {

				carte1 = cartes[i];
				carte2 = cartes1[j];

				tCards[0] = carte1;
				tCards[1]= carte2;

				card3 = this.generateVirtualCard35(tCards);

				colors1.splice(0,1,carte1.color);
				colors1.splice(1,1,carte2.color);
				colors1.splice(2, 1, card3[0].color);

				shapes1.splice(0,1,carte1.shape);
				shapes1.splice(1,1,carte2.shape);
				shapes1.splice(2, 1, card3[0].shape);


				fills1.splice(0,1,carte1.fill);
				fills1.splice(1,1,carte2.fill);
				fills1.splice(2, 1, card3[0].fill);

				borders1.splice(0,1,carte1.border);
				borders1.splice(1,1,carte2.border);
				borders1.splice(2, 1, card3[0].border);

				numbers1.splice(0,1,carte1.number);
				numbers1.splice(1,1,carte2.number);
				numbers1.splice(2, 1, card3[0].number);


				for (a = j+1; a<this.cards1.length; a++) {
                    if (shapes1[2] !== undefined && colors1[2] !== undefined && fills1[2] !== undefined && numbers1[2] !== undefined &&  borders1[2] !== undefined) {
                        if (shapes1[2] === shapesall[a] && colors1[2] === colorsall[a] && fills1[2] === fillsall[a] && borders1[2] === bordersall[a] && numbers1[2] === numbersall[a]) {
                            if (this.isSet(shapes1) && this.isSet(colors1) && this.isSet(fills1) && this.isSet(borders1) && this.isSet(numbers1)) {
                                self.set1 = self.set1 + 1;
                            }
                        }
                    }
                }

			}

		}

		this.$nbSets1.html(self.set1);
	},


	existingSet2: function() {
		var colors1 = [];
		var shapes1 =[];
		var fills1= [];
		var numbers1= [];
		var self = this;
		var cartes = self.cards2;
		var cartes1 = cartes;
		var cartes2 = cartes;


		var carte1, carte3;
		var carte2;
		var card4 = [];
		var tCards = [];


		var i, j, a, k;

		var colorsall= $.map(this.cards2, function(el) { return el.color;});
		var shapesall = $.map(this.cards2, function(el) { return el.shape; });
		var fillsall= $.map(this.cards2,function(el) { return el.fill; });
		var numbersall = $.map(this.cards2, function(el) { return el.number;});


		for(i=0; i < cartes.length-2; i++) {
			for (j = i + 1; j < cartes1.length - 1; j++) {
				for (k = j + 1; k < cartes2.length; k++) {

					carte1 = cartes[i];
					carte2 = cartes1[j];
					carte3 = cartes2[k];

					tCards[0] = carte1;
					tCards[1]= carte2;
					tCards[2]= carte3;


					card4 = this.generateVirtualCard44(tCards);

					colors1.splice(0,1,carte1.color);
					colors1.splice(1,1,carte2.color);
					colors1.splice(2,1,carte3.color);
					colors1.splice(3, 1, card4[0].color);

					shapes1.splice(0,1,carte1.shape);
					shapes1.splice(1,1,carte2.shape);
					shapes1.splice(2,1,carte3.shape);
					shapes1.splice(3, 1, card4[0].shape);


					fills1.splice(0,1,carte1.fill);
					fills1.splice(1,1,carte2.fill);
					fills1.splice(2,1,carte3.fill);
					fills1.splice(3, 1, card4[0].fill);

					numbers1.splice(0,1,carte1.number);
					numbers1.splice(1,1,carte2.number);
					numbers1.splice(2,1,carte3.number);
					numbers1.splice(3, 1, card4[0].number);



					//if(this.cardInListOfCards44(shapes1[3],colors1[3],fills1[3],numbers1[3] )){}

					for (a = k+1; a < self.cards2.length; a++) {
                        if (shapes1[3] !== undefined  && colors1[3] !== undefined && fills1[3] !== undefined && numbers1[3] !== undefined) {
                            if (shapes1[3] === shapesall[a] && colors1[3] === colorsall[a] && fills1[3] === fillsall[a] && numbers1[3] === numbersall[a]) {
                                if (this.isSet4(shapes1) && this.isSet4(colors1) && this.isSet4(fills1) && this.isSet4(numbers1)) {
                                    //console.log('Set 4x4 : ');
                                    //console.log(shapes1, colors1, fills1, numbers1);
                                    self.set2 = self.set2 + 1;
                                }

                            }
                        }
					}
				}
			}
		}
		console.log('-----------------------------------------------------------------');

		this.$nbSets2.html(self.set2);
	},


    existingSet3: function() {
        var colors1 = [];
        var shapes1 =[];
        var fills1= [];
        var borders1= [];
        var numbers1= [];
        var self = this;
        var cartes = self.cards3;
        var cartes1 = cartes;
        var cartes2 = cartes;


        var carte1, carte3;
        var carte2;
        var card4 = [];
        var tCards = [];


        var i, j, a, k;

        var colorsall= $.map(this.cards3, function(el) { return el.color;});
        var shapesall = $.map(this.cards3, function(el) { return el.shape; });
        var fillsall= $.map(this.cards3,function(el) { return el.fill; });
        var bordersall= $.map(this.cards3,function(el) { return el.border; });
        var numbersall = $.map(this.cards3, function(el) { return el.number;});


        for(i=0; i < cartes.length-2; i++) {
            for (j = i + 1; j < cartes1.length - 1; j++) {
                for (k = j + 1; k < cartes2.length; k++) {

                    carte1 = cartes[i];
                    carte2 = cartes1[j];
                    carte3 = cartes2[k];

                    tCards[0] = carte1;
                    tCards[1]= carte2;
                    tCards[2]= carte3;


                    card4 = this.generateVirtualCard45(tCards);

                    colors1.splice(0,1,carte1.color);
                    colors1.splice(1,1,carte2.color);
                    colors1.splice(2,1,carte3.color);
                    colors1.splice(3, 1, card4[0].color);

                    shapes1.splice(0,1,carte1.shape);
                    shapes1.splice(1,1,carte2.shape);
                    shapes1.splice(2,1,carte3.shape);
                    shapes1.splice(3, 1, card4[0].shape);


                    fills1.splice(0,1,carte1.fill);
                    fills1.splice(1,1,carte2.fill);
                    fills1.splice(2,1,carte3.fill);
                    fills1.splice(3, 1, card4[0].fill);

                    borders1.splice(0,1,carte1.border);
                    borders1.splice(1,1,carte2.border);
                    borders1.splice(2,1,carte3.border);
                    borders1.splice(3, 1, card4[0].border);


                    numbers1.splice(0,1,carte1.number);
                    numbers1.splice(1,1,carte2.number);
                    numbers1.splice(2,1,carte3.number);
                    numbers1.splice(3, 1, card4[0].number);



                    //if(this.cardInListOfCards44(shapes1[3],colors1[3],fills1[3],numbers1[3] )){}

                    for (a = k+1; a < self.cards3.length; a++) {
                        if (shapes1[3] !== undefined  && colors1[3] !== undefined && fills1[3] !== undefined && borders1[3] !== undefined && numbers1[3] !== undefined) {
                            if (shapes1[3] === shapesall[a] && colors1[3] === colorsall[a] && fills1[3] === fillsall[a] && borders1[3] === bordersall[a] && numbers1[3] === numbersall[a]) {
                                if (this.isSet4(shapes1) && this.isSet4(colors1) && this.isSet4(fills1) && this.isSet4(borders1) && this.isSet4(numbers1)) {
                                    console.log('Set 4x5 : ');
                                    console.log(shapes1, colors1, fills1, borders1, numbers1);
                                    self.set3 = self.set3 + 1;
                                }

                            }
                        }
                    }
                }
            }
        }
        console.log('-----------------------------------------------------------------');

        this.$nbSets3.html(self.set3);
    },



    existingSet4: function() {
        var colors1 = [];
        var shapes1 =[];
        var fills1= [];
        var borders1= [];
        var numbers1= [];
        var self = this;
        var cartes = self.cards4;
        var cartes1 = cartes;
        var cartes2 = cartes;
        var cartes3 = cartes;


        var carte1, carte3, carte4;
        var carte2;
        var card4 = [];
        var card5 ;
        var tCards = [];


        var i, j, a, k, l;

        var colorsall= $.map(this.cards4, function(el) { return el.color;});
        var shapesall = $.map(this.cards4, function(el) { return el.shape; });
        var fillsall= $.map(this.cards4,function(el) { return el.fill; });
        var bordersall= $.map(this.cards4,function(el) { return el.border; });
        var numbersall = $.map(this.cards4, function(el) { return el.number;});


        for(i=0; i < cartes.length-3; i++) {
            for (j = i + 1; j < cartes1.length - 2; j++) {
                for (k = j + 1; k < cartes2.length - 1; k++) {


                    carte1 = cartes[i];
                    carte2 = cartes1[j];
                    carte3 = cartes2[k];


                    tCards[0] = carte1;
                    tCards[1] = carte2;
                    tCards[2] = carte3;

                    colors1.splice(0, 1, carte1.color);
                    colors1.splice(1, 1, carte2.color);
                    colors1.splice(2, 1, carte3.color);

                    shapes1.splice(0, 1, carte1.shape);
                    shapes1.splice(1, 1, carte2.shape);
                    shapes1.splice(2, 1, carte3.shape);

                    fills1.splice(0, 1, carte1.fill);
                    fills1.splice(1, 1, carte2.fill);
                    fills1.splice(2, 1, carte3.fill);

                    borders1.splice(0, 1, carte1.border);
                    borders1.splice(1, 1, carte2.border);
                    borders1.splice(2, 1, carte3.border);

                    numbers1.splice(0, 1, carte1.number);
                    numbers1.splice(1, 1, carte2.number);
                    numbers1.splice(2, 1, carte3.number);


                    card4 = self.generer16Cartes55(colors1, shapes1, fills1, borders1, numbers1, this.cards4);


                    if (card4 !== undefined) {
                        if (card4.length > 0) {
                            for ( l = 0; l < card4.length; l++) {

                                tCards[0] = carte1;
                                tCards[1] = carte2;
                                tCards[2] = carte3;
                                tCards[3] = card4[l][0];

                                card5 = this.generateVirtualCard55(tCards);


                                colors1.splice(0, 1, carte1.color);
                                colors1.splice(1, 1, carte2.color);
                                colors1.splice(2, 1, carte3.color);
                                colors1.splice(3, 1, card4[l][0].color);
                                colors1.splice(4, 1, card5[0].color);


                                shapes1.splice(0, 1, carte1.shape);
                                shapes1.splice(1, 1, carte2.shape);
                                shapes1.splice(2, 1, carte3.shape);
                                shapes1.splice(3, 1, card4[l][0].shape);
                                shapes1.splice(4, 1, card5[0].shape);


                                fills1.splice(0, 1, carte1.fill);
                                fills1.splice(1, 1, carte2.fill);
                                fills1.splice(2, 1, carte3.fill);
                                fills1.splice(3, 1, card4[l][0].fill);
                                fills1.splice(4, 1, card5[0].fill);


                                borders1.splice(0, 1, carte1.border);
                                borders1.splice(1, 1, carte2.border);
                                borders1.splice(2, 1, carte3.border);
                                borders1.splice(3, 1, card4[l][0].border);
                                borders1.splice(4, 1, card5[0].border);


                                numbers1.splice(0, 1, carte1.number);
                                numbers1.splice(1, 1, carte2.number);
                                numbers1.splice(2, 1, carte3.number);
                                numbers1.splice(3, 1, card4[l][0].number);
                                numbers1.splice(4, 1, card5[0].number);

                                //if(this.cardInListOfCards44(shapes1[3],colors1[3],fills1[3],numbers1[3] )){}


                                for (a = j + 1; a < self.cards4.length; a++) {
                                    if (shapes1[3] !== undefined && colors1[3] !== undefined && fills1[3] !== undefined && borders1[3] !== undefined && numbers1[3] !== undefined) {
                                        if (shapes1[4] === shapesall[a] && colors1[4] === colorsall[a] && fills1[4] === fillsall[a] && borders1[4] === bordersall[a] && numbers1[4] === numbersall[a]) {
                                            if (this.isSet5(shapes1) && this.isSet5(colors1) && this.isSet5(fills1) && this.isSet5(borders1) && this.isSet5(numbers1)) {
                                                console.log('Set 5x5 : ');
                                                console.log(shapes1, colors1, fills1, borders1, numbers1);
                                                self.set4 = self.set4 + 1;
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        this.$nbSets4.html(self.set4);
    },


/*
	cardInListOfCards44 : function (shapes, colors, fills, numbers){
		var colorsall= $.map(this.cards2, function(el) { return el.color;});
		var shapesall = $.map(this.cards2, function(el) { return el.shape; });
		var fillsall= $.map(this.cards2,function(el) { return el.fill; });
		var numbersall = $.map(this.cards2, function(el) { return el.number;});

		var a = 0;

		for (a =0; a < this.cards2.length; a++) {
			return shapes === shapesall[a] && colors === colorsall[a] && fills === fillsall[a] && numbers === numbersall[a];
		}

	},


*/



	/*existingHyperSet: function() {
		var colors1 = [];
		var shapes1 =[];
		var fills1= [];
		var numbers1= [];
		var colors2 = [];
		var shapes2 =[];
		var fills2= [];
		var numbers2= [];
		var colors3 = [];
		var shapes3 =[];
		var fills3= [];
		var numbers3= [];

		var self = this;
		var cartes = self.cardsHS;
		var cartes1 = cartes;
		var cartes2 = cartes;


		var carte1, carte3;
		var carte2;
		var card4AB = [];
		var card4AC = [];
		var card4BC = [];
		var cardVirtualAB = [];
		var cardVirtualAC = [];
		var cardVirtualBC = [];
		var tCardsAB = [];
		var tCardsAC = [];
		var tCardsBC = [];





		var colors14 = [];
		var shapes14 =[];
		var fills14= [];
		var numbers14= [];
		var colors24 = [];
		var shapes24 =[];
		var fills24= [];
		var numbers24= [];
		var colors34 = [];
		var shapes34 =[];
		var fills34= [];
		var numbers34= [];


		var i, j, a, k;

		var colorsall= $.map(this.cardsHS, function(el) { return el.color;});
		var shapesall = $.map(this.cardsHS, function(el) { return el.shape; });
		var fillsall= $.map(this.cardsHS,function(el) { return el.fill; });
		var numbersall = $.map(this.cardsHS, function(el) { return el.number;});


		for(i=0; i < cartes.length-2; i++) {
			for (j = i + 1; j < cartes1.length - 1; j++) {
				for (k = j + 1; k < cartes2.length; k++) {

					carte1 = cartes[i];
					carte2 = cartes1[j];
					carte3 = cartes2[k];

					tCardsAB[0] = carte1;
					tCardsAB[1]= carte2;

					tCardsAC[0] = carte1;
					tCardsAC[1]= carte3;

					tCardsBC[0] = carte2;
					tCardsBC[1]= carte3;



					cardVirtualAB = this.generateVirtualCard34(tCardsAB);
					cardVirtualAC = this.generateVirtualCard34(tCardsAC);
					cardVirtualBC = this.generateVirtualCard34(tCardsBC);


					tCardsAB[0]= carte3;
					tCardsAB[1] = cardVirtualAB[0];

					tCardsAC[0]= carte2;
					tCardsAC[1] = cardVirtualAC[0];

					tCardsBC[0]= carte1;
					tCardsBC[1] = cardVirtualBC[0];

					card4AB = this.generateVirtualCard34(tCardsAB);
					card4AC = this.generateVirtualCard34(tCardsAC);
					card4BC = this.generateVirtualCard34(tCardsBC);



					colors14.splice(0, 1, card4AB[0].color);
					shapes14.splice(1, 1, card4AB[0].shape);
					fills14.splice(2, 1, card4AB[0].fill);
					numbers14.splice(3, 1, card4AB[0].number);

					colors24.splice(0, 1, card4AB[0].color);
					shapes24.splice(1, 1, card4AB[0].shape);
					fills24.splice(2, 1, card4AB[0].fill);
					numbers24.splice(3, 1, card4AB[0].number);

					colors34.splice(0, 1, card4AB[0].color);
					shapes34.splice(1, 1, card4AB[0].shape);
					fills34.splice(2, 1, card4AB[0].fill);
					numbers34.splice(3, 1, card4AB[0].number);


					colors1.splice(0, 1, carte1.color);
					colors1.splice(1, 1, carte2.color);
					colors1.splice(2, 1, cardVirtualAB[0].color);

					shapes1.splice(0, 1, carte1.shape);
					shapes1.splice(1, 1, carte2.shape);
					shapes1.splice(2, 1, cardVirtualAB[0].shape);

					fills1.splice(0, 1, carte1.fill);
					fills1.splice(1, 1, carte2.fill);
					fills1.splice(2, 1, cardVirtualAB[0].fill);

					numbers1.splice(0, 1, carte1.number);
					numbers1.splice(1, 1, carte2.number);
					numbers1.splice(2, 1, cardVirtualAB[0].number);




					colors2.splice(0, 1, carte1.color);
					colors2.splice(1, 1, carte3.color);
					colors2.splice(2, 1, cardVirtualAC[0].color);

					shapes2.splice(0, 1, carte1.shape);
					shapes2.splice(1, 1, carte3.shape);
					shapes2.splice(2, 1, cardVirtualAC[0].shape);

					fills2.splice(0, 1, carte1.fill);
					fills2.splice(1, 1, carte3.fill);
					fills2.splice(2, 1, cardVirtualAC[0].fill);

					numbers2.splice(0, 1, carte1.number);
					numbers2.splice(1, 1, carte3.number);
					numbers2.splice(2, 1, cardVirtualAC[0].number);




					colors3.splice(0, 1, carte2.color);
					colors3.splice(1, 1, carte3.color);
					colors3.splice(2, 1, cardVirtualBC[0].color);

					shapes3.splice(0, 1, carte2.shape);
					shapes3.splice(1, 1, carte3.shape);
					shapes3.splice(2, 1, cardVirtualBC[0].shape);

					fills3.splice(0, 1, carte2.fill);
					fills3.splice(1, 1, carte3.fill);
					fills3.splice(2, 1, cardVirtualBC[0].fill);

					numbers3.splice(0, 1, carte2.number);
					numbers3.splice(1, 1, carte3.number);
					numbers3.splice(2, 1, cardVirtualBC[0].number);


					//console.log('Card4 : ' + shapes1[0] + colors1[0] +fills1[0]+ numbers1[0] );

					//if(this.cardInListOfCards44(shapes1[3],colors1[3],fills1[3],numbers1[3] )){}
					a = k+1;


						if ( colors14[0] !== undefined  && shapes14[0] !== undefined && fills14[0] !== undefined && numbers14[0] !== undefined) {
								if(shapes14[0] === shapesall[a] && colors14[0] === colorsall[a] && fills14[0] === fillsall[a] && numbers14[0] === numbersall[a]){
									//console.log(carte1.color, carte1.fill, carte1.number, carte1.shape);
                                    //console.log(carte2.color, carte2.fill, carte2.number, carte2.shape);
									if( this.isSet(shapes1) &&  this.isSet(colors1) && this.isSet(fills1) && this.isSet(numbers1) ){
										console.log('ab');
										console.log(shapes1,colors1,fills1,numbers1);
										console.log(colors14,shapes14,fills14,numbers14);
										self.setHS = self.setHS + 1;

									}
								}

						}

					if (colors24[0] !== undefined  && shapes24[0] !== undefined && fills24[0] !== undefined && numbers24[0] !== undefined) {
						if(shapes24[0] === shapesall[a] && colors24[0] === colorsall[a] && fills24[0] === fillsall[a] && numbers24[0] === numbersall[a]){
								if( this.isSet(shapes2) &&  this.isSet(colors2) && this.isSet(fills2) && this.isSet(numbers2) ) {
									console.log('ac');
									console.log(shapes2,colors2,fills2,numbers2);
									console.log(colors14,shapes14,fills14,numbers14);
									self.setHS = self.setHS + 1;
								}
							}

					}
					if (colors34[0] !== undefined  && shapes34[0] !== undefined && fills34[0] !== undefined && numbers34[0] !== undefined) {
						if(shapes34[0] === shapesall[a] && colors34[0] === colorsall[a] && fills34[0] === fillsall[a] && numbers34[0] === numbersall[a]){
								if( this.isSet(shapes3) &&  this.isSet(colors3) && this.isSet(fills3) && this.isSet(numbers3) ) {
									console.log('bc');
									console.log(shapes3,colors3,fills3,numbers3);
									console.log(colors14,shapes14,fills14,numbers14);
									self.setHS = self.setHS + 1;
								}
							}
					}
				}
			}
		}

		this.$nbSetsHS.html(self.setHS);
	},


*/

		existingHyperSet: function() {
			var colors1 = [];
			var shapes1 =[];
			var fills1= [];
			var numbers1= [];
			var colors2 = [];
			var shapes2 =[];
			var fills2= [];
			var numbers2= [];
			var colors3 = [];
			var shapes3 =[];
			var fills3= [];
			var numbers3= [];

			var self = this;
			var cartes = self.cardsHS;
			var cartes1 = cartes;
			var cartes2 = cartes;


			var carte1, carte3;
			var carte2;
			var card4AB = [];
			var card4AC = [];
			var card4BC = [];
			var cardVirtualAB = [];
			var cardVirtualAC = [];
			var cardVirtualBC = [];
			var tCardsAB = [];
			var tCardsAC = [];
			var tCardsBC = [];



			var i, j, a, k;

			var colorsall= $.map(this.cardsHS, function(el) { return el.color;});
			var shapesall = $.map(this.cardsHS, function(el) { return el.shape; });
			var fillsall= $.map(this.cardsHS,function(el) { return el.fill; });
			var numbersall = $.map(this.cardsHS, function(el) { return el.number;});


			for(i=0; i < cartes.length-2; i++) {
				for (j = i + 1; j < cartes1.length - 1; j++) {
					for (k = j + 1; k < cartes2.length; k++) {
						carte1 = cartes[i];
						carte2 = cartes1[j];
						carte3 = cartes2[k];

						tCardsAB[0] = carte1;
						tCardsAB[1]= carte2;

						tCardsAC[0] = carte1;
						tCardsAC[1]= carte3;

						tCardsBC[0] = carte2;
						tCardsBC[1]= carte3;



						cardVirtualAB = this.generateVirtualCard34(tCardsAB);
						cardVirtualAC = this.generateVirtualCard34(tCardsAC);
						cardVirtualBC = this.generateVirtualCard34(tCardsBC);


						tCardsAB[0]= carte3;
						tCardsAB[1] = cardVirtualAB[0];

						tCardsAC[0]= carte2;
						tCardsAC[1] = cardVirtualAC[0];

						tCardsBC[0]= carte1;
						tCardsBC[1] = cardVirtualBC[0];

						card4AB = this.generateVirtualCard34(tCardsAB);
						card4AC = this.generateVirtualCard34(tCardsAC);
						card4BC = this.generateVirtualCard34(tCardsBC);

						colors1.splice(0, 1, carte3.color);
						colors1.splice(1, 1, card4AB[0].color);
						colors1.splice(2, 1, cardVirtualAB[0].color);

						shapes1.splice(0, 1, carte3.shape);
						shapes1.splice(1, 1, card4AB[0].shape);
						shapes1.splice(2, 1, cardVirtualAB[0].shape);

						fills1.splice(0, 1, carte3.fill);
						fills1.splice(1, 1, card4AB[0].fill);
						fills1.splice(2, 1, cardVirtualAB[0].fill);

						numbers1.splice(0, 1, carte3.number);
						numbers1.splice(1, 1, card4AB[0].number);
						numbers1.splice(2, 1, cardVirtualAB[0].number);


						colors2.splice(0, 1, carte2.color);
						colors2.splice(1, 1, card4AC[0].color);
						colors2.splice(2, 1, cardVirtualAC[0].color);

						shapes2.splice(0, 1, carte2.shape);
						shapes2.splice(1, 1, card4AC[0].shape);
						shapes2.splice(2, 1, cardVirtualAC[0].shape);

						fills2.splice(0, 1, carte2.fill);
						fills2.splice(1, 1, card4AC[0].fill);
						fills2.splice(2, 1, cardVirtualAC[0].fill);

						numbers2.splice(0, 1, carte2.number);
						numbers2.splice(1, 1, card4AC[0].number);
						numbers2.splice(2, 1, cardVirtualAC[0].number);


						colors3.splice(0, 1, carte1.color);
						colors3.splice(1, 1, card4BC[0].color);
						colors3.splice(2, 1, cardVirtualBC[0].color);

						shapes3.splice(0, 1, carte1.shape);
						shapes3.splice(1, 1, card4BC[0].shape);
						shapes3.splice(2, 1, cardVirtualBC[0].shape);

						fills3.splice(0, 1, carte1.fill);
						fills3.splice(1, 1, card4BC[0].fill);
						fills3.splice(2, 1, cardVirtualBC[0].fill);

						numbers3.splice(0, 1, carte1.number);
						numbers3.splice(1, 1, card4BC[0].number);
						numbers3.splice(2, 1, cardVirtualBC[0].number);


						//console.log('Card4 : ' + shapes1[0] + colors1[0] +fills1[0]+ numbers1[0] );

						//if(this.cardInListOfCards44(shapes1[3],colors1[3],fills1[3],numbers1[3] )){}

						for (a = k+1; a < self.cardsHS.length; a++) {
							if (shapes1[2] !== undefined  && colors1[2] !== undefined && fills1[2] !== undefined && numbers1[2] !== undefined) {
								if (shapes1[1] !== undefined && colors1[1] !== undefined && fills1[1] !== undefined && numbers1[1] !== undefined) {
									if(shapes1[1] === shapesall[a] && colors1[1]=== colorsall[a] && fills1[1] === fillsall[a] && numbers1[1] === numbersall[a]){
										//console.log('ab');
										//console.log(shapes1, colors1, fills1, numbers1);
										//console.log(carte1);
										//console.log(carte2);
										self.setHS = self.setHS + 1;
									}
								}
							}
						}


						for (a = k+1; a < self.cardsHS.length; a++) {
							if (shapes2[2] !== undefined  && colors2[2] !== undefined && fills2[2] !== undefined && numbers2[2] !== undefined) {
								if (shapes2[1] !== undefined && colors2[1] !== undefined && fills2[1] !== undefined && numbers2[1] !== undefined) {
									if(shapes2[1] === shapesall[a] && colors2[1]=== colorsall[a] && fills2[1] === fillsall[a] && numbers2[1] === numbersall[a]){
										//console.log('ac');
										//console.log(shapes2, colors2, fills2, numbers2);
										self.setHS = self.setHS + 1;
									}
								}
							}
						}


						for (a = k+1; a < self.cardsHS.length; a++) {
							if (shapes3[2] !== undefined  && colors3[2] !== undefined && fills3[2] !== undefined && numbers3[2] !== undefined) {
								if (shapes3[1] !== undefined && colors3[1] !== undefined && fills3[1] !== undefined && numbers3[1] !== undefined) {
									if(shapes3[1] === shapesall[a] && colors3[1]=== colorsall[a] && fills3[1] === fillsall[a] && numbers3[1] === numbersall[a]){
										//console.log('bc');
										//console.log(shapes3, colors3, fills3, numbers3);
										self.setHS = self.setHS + 1;
									}
								}
							}
						}


					}
				}
			}

			this.$nbSetsHS.html(self.setHS);
		},




	existingHyperSet0: function() {
		var colors1 = [];
		var shapes1 =[];
		var numbers1= [];
		var colors2 = [];
		var shapes2 =[];
		var numbers2= [];
		var colors3 = [];
		var shapes3 =[];
		var numbers3= [];

		var self = this;
		var cartes = self.cardsHS0;
		var cartes1 = cartes;
		var cartes2 = cartes;


		var carte1, carte3;
		var carte2;
		var card4AB = [];
		var card4AC = [];
		var card4BC = [];
		var cardVirtualAB = [];
		var cardVirtualAC = [];
		var cardVirtualBC = [];
		var tCardsAB = [];
		var tCardsAC = [];
		var tCardsBC = [];



		var i, j, a, k;

		var colorsall= $.map(this.cardsHS0, function(el) { return el.color;});
		var shapesall = $.map(this.cardsHS0, function(el) { return el.shape; });
		var numbersall = $.map(this.cardsHS0, function(el) { return el.number;});


		for(i=0; i < cartes.length-2; i++) {
			for (j = i + 1; j < cartes1.length - 1; j++) {
				for (k = j + 1; k < cartes2.length; k++) {
					carte1 = cartes[i];
					carte2 = cartes1[j];
					carte3 = cartes2[k];

					tCardsAB[0] = carte1;
					tCardsAB[1]= carte2;

					tCardsAC[0] = carte1;
					tCardsAC[1]= carte3;

					tCardsBC[0] = carte2;
					tCardsBC[1]= carte3;



					cardVirtualAB = this.generateVirtualCard33(tCardsAB);
					cardVirtualAC = this.generateVirtualCard33(tCardsAC);
					cardVirtualBC = this.generateVirtualCard33(tCardsBC);


					tCardsAB[0]= carte3;
					tCardsAB[1] = cardVirtualAB[0];

					tCardsAC[0]= carte2;
					tCardsAC[1] = cardVirtualAC[0];

					tCardsBC[0]= carte1;
					tCardsBC[1] = cardVirtualBC[0];

					card4AB = this.generateVirtualCard33(tCardsAB);
					card4AC = this.generateVirtualCard33(tCardsAC);
					card4BC = this.generateVirtualCard33(tCardsBC);

					colors1.splice(0, 1, carte3.color);
					colors1.splice(1, 1, card4AB[0].color);
					colors1.splice(2, 1, cardVirtualAB[0].color);

					shapes1.splice(0, 1, carte3.shape);
					shapes1.splice(1, 1, card4AB[0].shape);
					shapes1.splice(2, 1, cardVirtualAB[0].shape);

					numbers1.splice(0, 1, carte3.number);
					numbers1.splice(1, 1, card4AB[0].number);
					numbers1.splice(2, 1, cardVirtualAB[0].number);


					colors2.splice(0, 1, carte2.color);
					colors2.splice(1, 1, card4AC[0].color);
					colors2.splice(2, 1, cardVirtualAC[0].color);

					shapes2.splice(0, 1, carte2.shape);
					shapes2.splice(1, 1, card4AC[0].shape);
					shapes2.splice(2, 1, cardVirtualAC[0].shape);


					numbers2.splice(0, 1, carte2.number);
					numbers2.splice(1, 1, card4AC[0].number);
					numbers2.splice(2, 1, cardVirtualAC[0].number);




					colors3.splice(0, 1, carte1.color);
					colors3.splice(1, 1, card4BC[0].color);
					colors3.splice(2, 1, cardVirtualBC[0].color);

					shapes3.splice(0, 1, carte1.shape);
					shapes3.splice(1, 1, card4BC[0].shape);
					shapes3.splice(2, 1, cardVirtualBC[0].shape);


					numbers3.splice(0, 1, carte1.number);
					numbers3.splice(1, 1, card4BC[0].number);
					numbers3.splice(2, 1, cardVirtualBC[0].number);


					//console.log('Card4 : ' + shapes1[0] + colors1[0] +fills1[0]+ numbers1[0] );

					//if(this.cardInListOfCards44(shapes1[3],colors1[3],fills1[3],numbers1[3] )){}

					for (a = k+1; a < self.cardsHS0.length; a++) {
						if (shapes1[2] !== undefined  && colors1[2] !== undefined  && numbers1[2] !== undefined) {
							if (shapes1[1] !== undefined && colors1[1] !== undefined && numbers1[1] !== undefined) {
								if(shapes1[1] === shapesall[a] && colors1[1]=== colorsall[a]  && numbers1[1] === numbersall[a]){
									//console.log('ab');
									//console.log(shapes1, colors1, numbers1);
									//console.log(carte1);
									//console.log(carte2);
									self.setHS0 = self.setHS0 + 1;
								}
							}
						}
					}


					for (a = k+1; a < self.cardsHS0.length; a++) {
						if (shapes2[2] !== undefined  && colors2[2] !== undefined && numbers2[2] !== undefined) {
							if (shapes2[1] !== undefined && colors2[1] !== undefined && numbers2[1] !== undefined) {
								if(shapes2[1] === shapesall[a] && colors2[1]=== colorsall[a]  && numbers2[1] === numbersall[a]){
									//console.log('ac');
									//console.log(shapes2, colors2, numbers2);
									//console.log(carte1);
									//console.log(carte3);
									self.setHS0 = self.setHS0 + 1;
								}
							}
						}
					}


					for (a = k+1; a < self.cardsHS0.length; a++) {
						if (shapes3[2] !== undefined  && colors3[2] !== undefined && numbers3[2] !== undefined) {
							if (shapes3[1] !== undefined && colors3[1] !== undefined &&  numbers3[1] !== undefined) {
								if(shapes3[1] === shapesall[a] && colors3[1]=== colorsall[a]  && numbers3[1] === numbersall[a]){
									//console.log('bc');
									//console.log(shapes3, colors3, numbers3);
									//console.log(carte2);
									//console.log(carte3);
									self.setHS0 = self.setHS0 + 1;
								}
							}
						}
					}


				}
			}
		}

		this.$nbSetsHS0.html(self.setHS0);
	},



	existingHyperSet1: function() {
		var colors1 = [];
		var shapes1 =[];
		var fills1= [];
		var borders1 = [];
		var numbers1= [];
		var colors2 = [];
		var shapes2 =[];
		var fills2= [];
		var borders2 = [];
		var numbers2= [];
		var colors3 = [];
		var shapes3 =[];
		var fills3= [];
		var borders3 = [];
		var numbers3= [];

		var self = this;
		var cartes = self.cardsHS1;
		var cartes1 = cartes;
		var cartes2 = cartes;


		var carte1, carte3;
		var carte2;
		var card4AB = [];
		var card4AC = [];
		var card4BC = [];
		var cardVirtualAB = [];
		var cardVirtualAC = [];
		var cardVirtualBC = [];
		var tCardsAB = [];
		var tCardsAC = [];
		var tCardsBC = [];



		var i, j, a, k;

		var colorsall= $.map(this.cardsHS1, function(el) { return el.color;});
		var shapesall = $.map(this.cardsHS1, function(el) { return el.shape; });
		var fillsall= $.map(this.cardsHS1,function(el) { return el.fill; });
		var bordersall= $.map(this.cardsHS1,function(el) { return el.border; });
		var numbersall = $.map(this.cardsHS1, function(el) { return el.number;});


		for(i=0; i < cartes.length-2; i++) {
			for (j = i + 1; j < cartes1.length - 1; j++) {
				for (k = j + 1; k < cartes2.length; k++) {
					carte1 = cartes[i];
					carte2 = cartes1[j];
					carte3 = cartes2[k];

					tCardsAB[0] = carte1;
					tCardsAB[1]= carte2;

					tCardsAC[0] = carte1;
					tCardsAC[1]= carte3;

					tCardsBC[0] = carte2;
					tCardsBC[1]= carte3;



					cardVirtualAB = this.generateVirtualCard35(tCardsAB);
					cardVirtualAC = this.generateVirtualCard35(tCardsAC);
					cardVirtualBC = this.generateVirtualCard35(tCardsBC);


					tCardsAB[0]= carte3;
					tCardsAB[1] = cardVirtualAB[0];

					tCardsAC[0]= carte2;
					tCardsAC[1] = cardVirtualAC[0];

					tCardsBC[0]= carte1;
					tCardsBC[1] = cardVirtualBC[0];

					card4AB = this.generateVirtualCard35(tCardsAB);
					card4AC = this.generateVirtualCard35(tCardsAC);
					card4BC = this.generateVirtualCard35(tCardsBC);

					colors1.splice(0, 1, carte3.color);
					colors1.splice(1, 1, card4AB[0].color);
					colors1.splice(2, 1, cardVirtualAB[0].color);

					shapes1.splice(0, 1, carte3.shape);
					shapes1.splice(1, 1, card4AB[0].shape);
					shapes1.splice(2, 1, cardVirtualAB[0].shape);

					fills1.splice(0, 1, carte3.fill);
					fills1.splice(1, 1, card4AB[0].fill);
					fills1.splice(2, 1, cardVirtualAB[0].fill);

					borders1.splice(0, 1, carte3.border);
					borders1.splice(1, 1, card4AB[0].border);
					borders1.splice(2, 1, cardVirtualAB[0].border);

					numbers1.splice(0, 1, carte3.number);
					numbers1.splice(1, 1, card4AB[0].number);
					numbers1.splice(2, 1, cardVirtualAB[0].number);


					colors2.splice(0, 1, carte2.color);
					colors2.splice(1, 1, card4AC[0].color);
					colors2.splice(2, 1, cardVirtualAC[0].color);

					shapes2.splice(0, 1, carte2.shape);
					shapes2.splice(1, 1, card4AC[0].shape);
					shapes2.splice(2, 1, cardVirtualAC[0].shape);

					fills2.splice(0, 1, carte2.fill);
					fills2.splice(1, 1, card4AC[0].fill);
					fills2.splice(2, 1, cardVirtualAC[0].fill);

					borders2.splice(0, 1, carte2.border);
					borders2.splice(1, 1, card4AC[0].border);
					borders2.splice(2, 1, cardVirtualAC[0].border);

					numbers2.splice(0, 1, carte2.number);
					numbers2.splice(1, 1, card4AC[0].number);
					numbers2.splice(2, 1, cardVirtualAC[0].number);




					colors3.splice(0, 1, carte1.color);
					colors3.splice(1, 1, card4BC[0].color);
					colors3.splice(2, 1, cardVirtualBC[0].color);

					shapes3.splice(0, 1, carte1.shape);
					shapes3.splice(1, 1, card4BC[0].shape);
					shapes3.splice(2, 1, cardVirtualBC[0].shape);

					fills3.splice(0, 1, carte1.fill);
					fills3.splice(1, 1, card4BC[0].fill);
					fills3.splice(2, 1, cardVirtualBC[0].fill);

					borders3.splice(0, 1, carte1.border);
					borders3.splice(1, 1, card4BC[0].border);
					borders3.splice(2, 1, cardVirtualBC[0].border);

					numbers3.splice(0, 1, carte1.number);
					numbers3.splice(1, 1, card4BC[0].number);
					numbers3.splice(2, 1, cardVirtualBC[0].number);


					//console.log('Card4 : ' + shapes1[0] + colors1[0] +fills1[0]+ numbers1[0] );

					//if(this.cardInListOfCards44(shapes1[3],colors1[3],fills1[3],numbers1[3] )){}

					for (a = k+1; a < self.cardsHS1.length; a++) {
						if (shapes1[2] !== undefined  && colors1[2] !== undefined && fills1[2] !== undefined && borders1[2] !== undefined && numbers1[2] !== undefined) {
							if (shapes1[1] !== undefined && colors1[1] !== undefined && fills1[1] !== undefined && borders1[1] !== undefined && numbers1[1] !== undefined) {
								if(shapes1[1] === shapesall[a] && colors1[1]=== colorsall[a] && fills1[1] === fillsall[a] && borders1[1] === bordersall[a] && numbers1[1] === numbersall[a]){
									//console.log('ab');
									//console.log(shapes1, colors1, fills1, borders1, numbers1);
									//console.log(carte1);
									//console.log(carte2);
									self.setHS1 = self.setHS1 + 1;
								}
							}
						}
					}


					for (a = k+1; a < self.cardsHS1.length; a++) {
						if (shapes2[2] !== undefined  && colors2[2] !== undefined && fills2[2] !== undefined  && borders2[2] !== undefined  && numbers2[2] !== undefined) {
							if (shapes2[1] !== undefined && colors2[1] !== undefined && fills2[1] !== undefined && borders2[1] !== undefined && numbers2[1] !== undefined) {
								if(shapes2[1] === shapesall[a] && colors2[1]=== colorsall[a] && fills2[1] === fillsall[a] && borders2[1]=== bordersall[a] && numbers2[1] === numbersall[a]){
									//console.log('ac');
									//console.log(shapes2, colors2, fills2, borders2, numbers2);
									self.setHS1 = self.setHS1 + 1;
								}
							}
						}
					}


					for (a = k+1; a < self.cardsHS1.length; a++) {
						if (shapes3[2] !== undefined  && colors3[2] !== undefined && fills3[2] !== undefined && borders3[2] !== undefined && numbers3[2] !== undefined) {
							if (shapes3[1] !== undefined && colors3[1] !== undefined && fills3[1] !== undefined && borders3[1] !== undefined && numbers3[1] !== undefined) {
								if(shapes3[1] === shapesall[a] && colors3[1]=== colorsall[a] && fills3[1] === fillsall[a] && borders3[1]=== bordersall[a] && numbers3[1] === numbersall[a]){
									//console.log('bc');
								    //console.log(shapes3, colors3, fills3, borders3, numbers3);
									self.setHS1 = self.setHS1 + 1;
								}
							}
						}
					}


				}
			}
		}

		this.$nbSetsHS1.html(self.setHS1);
	},
/*
    existingHyperSet2: function() {
        var colors1 = [];
        var shapes1 =[];
        var fills1= [];
        var numbers1= [];
        var colors2 = [];
        var shapes2 =[];
        var fills2= [];
        var numbers2= [];
        var colors3 = [];
        var shapes3 =[];
        var fills3= [];
        var numbers3= [];

        var colors4 = [];
        var shapes4 =[];
        var fills4= [];
        var numbers4= [];
        var colors5 = [];
        var shapes5 =[];
        var fills5= [];
        var numbers5= [];
        var colors6 = [];
        var shapes6 =[];
        var fills6= [];
        var numbers6= [];
        var colors7 = [];
        var shapes7 =[];
        var fills7= [];
        var numbers7= [];
        var colors8 = [];
        var shapes8 =[];
        var fills8= [];
        var numbers8= [];



        var self = this;
        var cartes = self.cardsHS2;
        var cartes1 = cartes;
        var cartes2 = cartes;
        var cartes3 = cartes;
        var cartes4 = cartes;


        var carte1, carte3, carte4, carte5;
        var carte2;
        var card6ABC = [];
        var card6ABD = [];
        var card6ABE = [];
        var card6ACD = [];
        var card6ACE = [];
        var card6BCD = [];
        var card6BCE = [];
        var card6CDE = [];

        var cardVirtualABC = [];
        var cardVirtualABD = [];
        var cardVirtualABE = [];
        var cardVirtualACD = [];
        var cardVirtualACE = [];
        var cardVirtualBCD = [];
        var cardVirtualBCE = [];
        var cardVirtualCDE = [];

        var tCardsABC = [];
        var tCardsABD = [];
        var tCardsABE = [];
        var tCardsACD = [];
        var tCardsACE = [];
        var tCardsBCD = [];
        var tCardsBCE = [];
        var tCardsCDE = [];



        var i, j, a,l, m, k;

        var colorsall= $.map(this.cardsHS2, function(el) { return el.color;});
        var shapesall = $.map(this.cardsHS2, function(el) { return el.shape; });
        var fillsall= $.map(this.cardsHS2,function(el) { return el.fill; });
        var numbersall = $.map(this.cardsHS2, function(el) { return el.number;});


        for(i=0; i < cartes.length-4; i++) {
            for (j = i + 1; j < cartes1.length - 3; j++) {
                for (k = j + 1; k < cartes2.length-2; k++) {
                    for (l = k + 1; l < cartes3.length-1; l++) {
                        for (m = l + 1; m < cartes4.length; m++) {


                            carte1 = cartes[i];
                            carte2 = cartes1[j];
                            carte3 = cartes2[k];
                            carte4 = cartes3[l];
                            carte5 = cartes4[m];


                            tCardsABC[0] = carte1;
                            tCardsABC[1] = carte2;
                            tCardsABC[2] = carte3;


                            tCardsABD[0] = carte1;
                            tCardsABD[1] = carte2;
                            tCardsABD[2] = carte4;

                            tCardsABE[0] = carte1;
                            tCardsABE[1] = carte2;
                            tCardsABE[2] = carte5;

                            tCardsACD[0] = carte1;
                            tCardsACD[1] = carte3;
                            tCardsACD[2] = carte4;

                            tCardsACE[0] = carte1;
                            tCardsACE[1] = carte3;
                            tCardsACE[2] = carte5;

                            tCardsBCD[0] = carte2;
                            tCardsBCD[1] = carte3;
                            tCardsBCD[2] = carte4;

                            tCardsBCE[0] = carte2;
                            tCardsBCE[1] = carte3;
                            tCardsBCE[2] = carte5;

                            tCardsCDE[0] = carte3;
                            tCardsCDE[1] = carte4;
                            tCardsCDE[2] = carte5;

                            cardVirtualABC = this.generateVirtualCard44(tCardsABC);
                            cardVirtualABD = this.generateVirtualCard44(tCardsABD);
                            cardVirtualABE = this.generateVirtualCard44(tCardsABE);
                            cardVirtualACD = this.generateVirtualCard44(tCardsACD);
                            cardVirtualACE = this.generateVirtualCard44(tCardsACE);
                            cardVirtualBCD = this.generateVirtualCard44(tCardsBCD);
                            cardVirtualBCE = this.generateVirtualCard44(tCardsBCE);
                            cardVirtualCDE = this.generateVirtualCard44(tCardsCDE);


                            tCardsABC[0] = carte4;
                            tCardsABC[1] = carte5;
                            tCardsABC[2] = cardVirtualABC[0];

                            tCardsABD[0] = carte3;
                            tCardsABD[1] = carte5;
                            tCardsABD[2] = cardVirtualABD[0];

                            tCardsABE[0] = carte3;
                            tCardsABE[1] = carte4;
                            tCardsABE[2] = cardVirtualABE[0];

                            tCardsACD[0] = carte2;
                            tCardsACD[1] = carte5;
                            tCardsACD[2] = cardVirtualACD[0];

                            tCardsACE[0] = carte2;
                            tCardsACE[1] = carte4;
                            tCardsACE[2] = cardVirtualACE[0];

                            tCardsBCD[0] = carte1;
                            tCardsBCD[1] = carte5;
                            tCardsBCD[2] = cardVirtualBCD[0];

                            tCardsBCE[0] = carte1;
                            tCardsBCE[1] = carte4;
                            tCardsBCE[2] = cardVirtualBCE[0];

                            tCardsCDE[0] = carte1;
                            tCardsCDE[1] = carte2;
                            tCardsCDE[2] = cardVirtualCDE[0];

                            card6ABC = this.generateVirtualCard44(tCardsABC);
                            card6ABD = this.generateVirtualCard44(tCardsABD);
                            card6ABE = this.generateVirtualCard44(tCardsABE);
                            card6ACD = this.generateVirtualCard44(tCardsACD);
                            card6ACE = this.generateVirtualCard44(tCardsACE);
                            card6BCD = this.generateVirtualCard44(tCardsBCD);
                            card6BCE = this.generateVirtualCard44(tCardsBCE);
                            card6CDE = this.generateVirtualCard44(tCardsCDE);

                            colors1.splice(0, 1, carte4.color);
                            colors1.splice(1, 1, carte5.color);
                            colors1.splice(2, 1, card6ABC[0].color);
                            colors1.splice(3, 1, cardVirtualABC[0].color);

                            shapes1.splice(0, 1, carte4.shape);
                            shapes1.splice(1, 1, carte5.shape);
                            shapes1.splice(2, 1, card6ABC[0].shape);
                            shapes1.splice(3, 1, cardVirtualABC[0].shape);

                            fills1.splice(0, 1, carte4.fill);
                            fills1.splice(1, 1, carte5.fill);
                            fills1.splice(2, 1, card6ABC[0].fill);
                            fills1.splice(3, 1, cardVirtualABC[0].fill);

                            numbers1.splice(0, 1, carte4.number);
                            numbers1.splice(1, 1, carte5.number);
                            numbers1.splice(2, 1, card6ABC[0].number);
                            numbers1.splice(3, 1, cardVirtualABC[0].number);


                            colors2.splice(0, 1, carte3.color);
                            colors1.splice(1, 1, carte5.color);
                            colors2.splice(2, 1, card6ABD[0].color);
                            colors2.splice(3, 1, cardVirtualABD[0].color);

                            shapes2.splice(0, 1, carte3.shape);
                            shapes1.splice(1, 1, carte5.shape);
                            shapes2.splice(2, 1, card6ABD[0].shape);
                            shapes2.splice(3, 1, cardVirtualABD[0].shape);

                            fills2.splice(0, 1, carte3.fill);
                            fills1.splice(1, 1, carte5.fill);
                            fills2.splice(2, 1, card6ABD[0].fill);
                            fills2.splice(3, 1, cardVirtualABD[0].fill);

                            numbers2.splice(0, 1, carte3.number);
                            numbers1.splice(1, 1, carte5.number);
                            numbers2.splice(2, 1, card6ABD[0].number);
                            numbers2.splice(3, 1, cardVirtualABD[0].number);


                            colors3.splice(0, 1, carte3.color);
                            colors3.splice(1, 1, carte4.color);
                            colors3.splice(2, 1, card6ABE[0].color);
                            colors3.splice(3, 1, cardVirtualABE[0].color);

                            shapes3.splice(0, 1, carte3.shape);
                            shapes3.splice(1, 1, carte4.shape);
                            shapes3.splice(2, 1, card6ABE[0].shape);
                            shapes3.splice(3, 1, cardVirtualABE[0].shape);

                            fills3.splice(0, 1, carte3.fill);
                            fills3.splice(1, 1, carte4.fill);
                            fills3.splice(2, 1, card6ABE[0].fill);
                            fills3.splice(3, 1, cardVirtualABE[0].fill);

                            numbers3.splice(0, 1, carte3.number);
                            numbers3.splice(1, 1, carte4.number);
                            numbers3.splice(2, 1, card6ABE[0].number);
                            numbers3.splice(3, 1, cardVirtualABE[0].number);




                            colors4.splice(0, 1, carte2.color);
                            colors4.splice(1, 1, carte5.color);
                            colors4.splice(2, 1, card6ACD[0].color);
                            colors4.splice(3, 1, cardVirtualACD[0].color);

                            shapes4.splice(0, 1, carte2.shape);
                            shapes4.splice(1, 1, carte5.shape);
                            shapes4.splice(2, 1, card6ACD[0].shape);
                            shapes4.splice(3, 1, cardVirtualACD[0].shape);

                            fills4.splice(0, 1, carte2.fill);
                            fills4.splice(1, 1, carte5.fill);
                            fills4.splice(2, 1, card6ACD[0].fill);
                            fills4.splice(3, 1, cardVirtualACD[0].fill);

                            numbers4.splice(0, 1, carte2.number);
                            numbers4.splice(1, 1, carte5.number);
                            numbers4.splice(2, 1, card6ACD[0].number);
                            numbers4.splice(3, 1, cardVirtualACD[0].number);


                            colors5.splice(0, 1, carte2.color);
                            colors5.splice(1, 1, carte4.color);
                            colors5.splice(2, 1, card6ACE[0].color);
                            colors5.splice(3, 1, cardVirtualACE[0].color);

                            shapes5.splice(0, 1, carte2.shape);
                            shapes5.splice(1, 1, carte4.shape);
                            shapes5.splice(2, 1, card6ACE[0].shape);
                            shapes5.splice(3, 1, cardVirtualACE[0].shape);

                            fills5.splice(0, 1, carte2.fill);
                            fills5.splice(1, 1, carte4.fill);
                            fills5.splice(2, 1, card6ACE[0].fill);
                            fills5.splice(3, 1, cardVirtualACE[0].fill);

                            numbers5.splice(0, 1, carte2.number);
                            numbers5.splice(1, 1, carte4.number);
                            numbers5.splice(2, 1, card6ACE[0].number);
                            numbers5.splice(3, 1, cardVirtualACE[0].number);


                            colors6.splice(0, 1, carte1.color);
                            colors6.splice(1, 1, carte5.color);
                            colors6.splice(2, 1, card6BCD[0].color);
                            colors6.splice(3, 1, cardVirtualBCD[0].color);

                            shapes6.splice(0, 1, carte1.shape);
                            shapes6.splice(1, 1, carte5.shape);
                            shapes6.splice(2, 1, card6BCD[0].shape);
                            shapes6.splice(3, 1, cardVirtualBCD[0].shape);

                            fills6.splice(0, 1, carte1.fill);
                            fills6.splice(1, 1, carte5.fill);
                            fills6.splice(2, 1, card6BCD[0].fill);
                            fills6.splice(3, 1, cardVirtualBCD[0].fill);

                            numbers6.splice(0, 1, carte1.number);
                            numbers6.splice(1, 1, carte5.number);
                            numbers6.splice(2, 1, card6BCD[0].number);
                            numbers6.splice(3, 1, cardVirtualBCD[0].number);



                            colors7.splice(0, 1, carte1.color);
                            colors7.splice(1, 1, carte4.color);
                            colors7.splice(2, 1, card6BCE[0].color);
                            colors7.splice(3, 1, cardVirtualBCE[0].color);

                            shapes7.splice(0, 1, carte1.shape);
                            shapes7.splice(1, 1, carte4.shape);
                            shapes7.splice(2, 1, card6BCE[0].shape);
                            shapes7.splice(3, 1, cardVirtualBCE[0].shape);

                            fills7.splice(0, 1, carte1.fill);
                            fills7.splice(1, 1, carte4.fill);
                            fills7.splice(2, 1, card6BCE[0].fill);
                            fills7.splice(3, 1, cardVirtualBCE[0].fill);

                            numbers7.splice(0, 1, carte1.number);
                            numbers7.splice(1, 1, carte4.number);
                            numbers7.splice(2, 1, card6BCE[0].number);
                            numbers7.splice(3, 1, cardVirtualBCE[0].number);



                            colors8.splice(0, 1, carte1.color);
                            colors8.splice(1, 1, carte2.color);
                            colors8.splice(2, 1, card6CDE[0].color);
                            colors8.splice(3, 1, cardVirtualCDE[0].color);

                            shapes8.splice(0, 1, carte1.shape);
                            shapes8.splice(1, 1, carte2.shape);
                            shapes8.splice(2, 1, card6CDE[0].shape);
                            shapes8.splice(3, 1, cardVirtualCDE[0].shape);

                            fills8.splice(0, 1, carte1.fill);
                            fills8.splice(1, 1, carte2.fill);
                            fills8.splice(2, 1, card6CDE[0].fill);
                            fills8.splice(3, 1, cardVirtualCDE[0].fill);

                            numbers8.splice(0, 1, carte1.number);
                            numbers8.splice(1, 1, carte2.number);
                            numbers8.splice(2, 1, card6CDE[0].number);
                            numbers8.splice(3, 1, cardVirtualCDE[0].number);

                            //console.log('Card4 : ' + shapes1[0] + colors1[0] +fills1[0]+ numbers1[0] );

                            //if(this.cardInListOfCards44(shapes1[3],colors1[3],fills1[3],numbers1[3] )){}

                            for (a = k + 1; a < self.cardsHS2.length; a++) {
                                if (shapes1[2] !== undefined && colors1[2] !== undefined && fills1[2] !== undefined  && numbers1[2] !== undefined) {
                                    if (shapes1[3] !== undefined && colors1[3] !== undefined && fills1[3] !== undefined  && numbers1[3] !== undefined) {
                                        if (shapes1[2] === shapesall[a] && colors1[2] === colorsall[a] && fills1[2] === fillsall[a] && numbers1[2] === numbersall[a]) {
                                            //console.log('ab');
                                            //console.log(shapes1, colors1, fills1, borders1, numbers1);
                                            //console.log(carte1);
                                            //console.log(carte2);
                                            self.setHS2 = self.setHS2 + 1;
                                        }
                                    }
                                }
                            }


                            for (a = k + 1; a < self.cardsHS2.length; a++) {
                                if (shapes2[2] !== undefined && colors2[2] !== undefined && fills2[2] !== undefined && numbers2[2] !== undefined) {
                                    if (shapes2[3] !== undefined && colors2[3] !== undefined && fills2[3] !== undefined  && numbers2[3] !== undefined) {
                                        if (shapes2[2] === shapesall[a] && colors2[2] === colorsall[a] && fills2[2] === fillsall[a] && numbers2[2] === numbersall[a]) {
                                            //console.log('ac');
                                            //console.log(shapes2, colors2, fills2, borders2, numbers2);
                                            self.setHS2 = self.setHS2 + 1;
                                        }
                                    }
                                }
                            }


                            for (a = k + 1; a < self.cardsHS2.length; a++) {
                                if (shapes3[2] !== undefined && colors3[2] !== undefined && fills3[2] !== undefined  && numbers3[2] !== undefined) {
                                    if (shapes3[3] !== undefined && colors3[3] !== undefined && fills3[3] !== undefined  && numbers3[3] !== undefined) {
                                        if (shapes3[2] === shapesall[a] && colors3[2] === colorsall[a] && fills3[2] === fillsall[a] && numbers3[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS2 = self.setHS2 + 1;
                                        }
                                    }
                                }
                            }



                            for (a = k + 1; a < self.cardsHS2.length; a++) {
                                if (shapes4[2] !== undefined && colors4[2] !== undefined && fills4[2] !== undefined  && numbers4[2] !== undefined) {
                                    if (shapes4[3] !== undefined && colors4[3] !== undefined && fills4[3] !== undefined  && numbers4[3] !== undefined) {
                                        if (shapes4[2] === shapesall[a] && colors4[2] === colorsall[a] && fills4[2] === fillsall[a] && numbers4[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS2 = self.setHS2 + 1;
                                        }
                                    }
                                }
                            }
                            for (a = k + 1; a < self.cardsHS2.length; a++) {
                                if (shapes5[2] !== undefined && colors5[2] !== undefined && fills5[2] !== undefined  && numbers5[2] !== undefined) {
                                    if (shapes5[3] !== undefined && colors5[3] !== undefined && fills5[3] !== undefined  && numbers5[3] !== undefined) {
                                        if (shapes5[2] === shapesall[a] && colors5[2] === colorsall[a] && fills5[2] === fillsall[a] && numbers5[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS2 = self.setHS2 + 1;
                                        }
                                    }
                                }
                            }
                            for (a = k + 1; a < self.cardsHS2.length; a++) {
                                if (shapes6[2] !== undefined && colors6[2] !== undefined && fills6[2] !== undefined  && numbers6[2] !== undefined) {
                                    if (shapes6[3] !== undefined && colors6[3] !== undefined && fills6[3] !== undefined  && numbers6[3] !== undefined) {
                                        if (shapes6[2] === shapesall[a] && colors6[2] === colorsall[a] && fills6[2] === fillsall[a] && numbers6[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS2 = self.setHS2 + 1;
                                        }
                                    }
                                }
                            }

                            for (a = k + 1; a < self.cardsHS2.length; a++) {
                                if (shapes7[2] !== undefined && colors7[2] !== undefined && fills7[2] !== undefined  && numbers7[2] !== undefined) {
                                    if (shapes7[3] !== undefined && colors7[3] !== undefined && fills7[3] !== undefined  && numbers7[3] !== undefined) {
                                        if (shapes7[2] === shapesall[a] && colors7[2] === colorsall[a] && fills7[2] === fillsall[a] && numbers7[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS2 = self.setHS2 + 1;
                                        }
                                    }
                                }
                            }


                            for (a = k + 1; a < self.cardsHS2.length; a++) {
                                if (shapes8[2] !== undefined && colors8[2] !== undefined && fills8[2] !== undefined  && numbers8[2] !== undefined) {
                                    if (shapes8[3] !== undefined && colors8[3] !== undefined && fills8[3] !== undefined  && numbers8[3] !== undefined) {
                                        if (shapes8[2] === shapesall[a] && colors8[2] === colorsall[a] && fills8[2] === fillsall[a] && numbers8[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS2 = self.setHS2 + 1;
                                        }
                                    }
                                }
                            }


                        }
                    }
                }
            }
        }

        this.$nbSetsHS2.html(self.setHS2);
    },*/


	generer16Cartes: function(colors, shapes, fills, numbers, allCards){
	    var i = 0;
		var possibleCards = [];
		var colors1 = [];
		var shapes1 =[];
		var fills1= [];
		var numbers1= [];
		var colors2 = [];
		var shapes2 =[];
		var fills2= [];
		var numbers2= [];

		var card1 = [];
		var card2 = [];
		var card3 = [];
		var card4 = [];
		var card5 = [];
		var card6 = [];
		var card7 = [];
		var card8 = [];
		var card9 = [];
		var card10 = [];
		var card11 = [];
		var card12 = [];
		var card13 = [];
		var card14 = [];
		var card15 = [];
		var card16 = [];

		var colorsall= $.map(allCards, function(el) { return el.color;});
		var shapesall = $.map(allCards, function(el) { return el.shape; });
		var fillsall= $.map(allCards,function(el) { return el.fill; });
		var numbersall = $.map(allCards, function(el) { return el.number;});

		var colorsSet = new Set(colors);
		var shapesSet = new Set(shapes);
		var fillsSet = new Set(fills);
		var numbersSet = new Set(numbers);

		if( colors[0] === colors[1]){
			colors1.push(colors[1]);
		}else {
			if(colorsSet.has('green') && colorsSet.has('red') ){
				colors1.push('purple');
				colors2.push('lightblue');
			}
			if(colorsSet.has('green') && colorsSet.has('purple')){
				colors1.push('red');
				colors2.push('lightblue');
			}
			if(colorsSet.has('green') && colorsSet.has('lightblue')){
				colors1.push('red');
				colors2.push('purple');
			}
			if(colorsSet.has('red') && colorsSet.has('purple')){
				colors1.push('green');
				colors2.push('lightblue');
			}
			if(colorsSet.has('red') && colorsSet.has('lightblue')){
				colors1.push('green');
				colors2.push('purple');
			}
			if(colorsSet.has('purple') && colorsSet.has('lightblue')){
				colors1.push('green');
				colors2.push('red');
			}
		}
		if( shapes[0] === shapes[1]){
			shapes1.push(shapes[1]);
		}else {
			if(shapesSet.has('oval') && shapesSet.has('diamond') ){
				shapes1.push('rectangle');
				shapes2.push('wave');
			}
			if(shapesSet.has('oval') && shapesSet.has('rectangle')){
				shapes1.push('diamond');
				shapes2.push('wave');
			}
			if(shapesSet.has('oval') && shapesSet.has('wave')){
				shapes1.push('diamond');
				shapes2.push('rectangle');
			}
			if(shapesSet.has('diamond') && shapesSet.has('rectangle')){
				shapes1.push('oval');
				shapes2.push('wave');
			}
			if(shapesSet.has('diamond') && shapesSet.has('wave')){
				shapes1.push('oval');
				shapes2.push('rectangle');
			}
			if(shapesSet.has('rectangle') && shapesSet.has('wave')){
				shapes1.push('diamond');
				shapes2.push('oval');
			}
		}

		if( fills[0] === fills[1]){
			fills1.push(fills[1]);
		}else {
			if(fillsSet.has('solid') && fillsSet.has('open') ){
				fills1.push('quadrillage');
				fills2.push('stripped');
			}
			if(fillsSet.has('solid') && fillsSet.has('quadrillage')){
				fills1.push('open');
				fills2.push('stripped');
			}
			if(fillsSet.has('solid') && fillsSet.has('stripped')){
				fills1.push('quadrillage');
				fills2.push('stripped');
			}
			if(fillsSet.has('open') && fillsSet.has('quadrillage')){
				fills1.push('solid');
				fills2.push('wave');
			}
			if(fillsSet.has('open') && fillsSet.has('stripped')){
				fills1.push('solid');
				fills2.push('quadrillage');
			}
			if(fillsSet.has('stripped') && fillsSet.has('quadrillage')){
				fills1.push('solid');
				fills2.push('open');
			}
		}

		if( numbers[0] === numbers[1]){
			numbers1.push(numbers[1]);
		}else {
			if(numbersSet.has(1) && numbersSet.has(2) ){
				numbers1.push(3);
				numbers2.push(4);
			}
			if(numbersSet.has(1) && numbersSet.has(3)){
				numbers1.push(2);
				numbers2.push(4);
			}
			if(numbersSet.has(1) && numbersSet.has(4)){
				numbers1.push(2);
				numbers2.push(3);
			}
			if(numbersSet.has(2) && numbersSet.has(3)){
				numbers1.push(1);
				numbers2.push(4);
			}
			if(numbersSet.has(2) && numbersSet.has(4)){
				numbers1.push(1);
				numbers2.push(3);
			}
			if(numbersSet.has(3) && numbersSet.has(4)){
				numbers1.push(1);
				numbers2.push(2);
			}
		}

		for (var a = 0 ; a < allCards.length; a++) {
				if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && numbers1[0] === numbersall[a]) {
					card1.push({
						color : colors1[0],
						shape : shapes1[0],
						fill : fills1[0],
						number: numbers1[0]
					});
					possibleCards[i] = card1;
					i = i +1;
				}
			if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && numbers1[0] === numbersall[a]) {
				if(colors2[0] !== undefined) {
					card2.push({
						color: colors2[0],
						shape: shapes1[0],
						fill: fills1[0],
						number: numbers1[0]
					});
                    possibleCards[i] = card2;
                    i = i +1;
				}
			}
				if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && numbers1[0] === numbersall[a]) {
					if(shapes2[0] !== undefined) {
						card3.push({
							color: colors1[0],
							shape: shapes2[0],
							fill: fills1[0],
							number: numbers1[0]
						});
                        possibleCards[i] = card3;
                        i = i +1;
					}
				}
				if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && numbers1[0] === numbersall[a]) {
					if(fills2[0] !== undefined) {
						card4.push({
							color: colors1[0],
							shape: shapes1[0],
							fill: fills2[0],
							number: numbers1[0]
						});
                        possibleCards[i] = card4;
                        i = i +1;
					}
				}
				if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && numbers2[0] === numbersall[a]) {
					if (numbers2[0] !== undefined) {
						card5.push({
							color: colors1[0],
							shape: shapes1[0],
							fill: fills1[0],
							number: numbers2[0]
						});
                        possibleCards[i] = card5;
                        i = i +1;
					}
				}






			if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && numbers2[0] === numbersall[a]) {
				if(fills2[0] !== undefined && numbers2[0] !== undefined) {
					card6.push({
						color: colors1[0],
						shape: shapes1[0],
						fill: fills2[0],
						number: numbers2[0]
					});
                    possibleCards[i] = card6;
                    i = i +1;
				}
			}
			if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && numbers2[0] === numbersall[a]) {
				if(shapes2[0] !== undefined && numbers2[0] !== undefined) {
					card7.push({
						color: colors1[0],
						shape: shapes2[0],
						fill: fills1[0],
						number: numbers2[0]
					});
                    possibleCards[i] = card7;
                    i = i +1;
				}
			}
			if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && numbers1[0] === numbersall[a]) {
				if(fills2[0] !== undefined && shapes2[0] !== undefined ) {
					card8.push({
						color: colors1[0],
						shape: shapes2[0],
						fill: fills2[0],
						number: numbers1[0]
					});
                    possibleCards[i] = card8;
                    i = i +1;
				}
			}
			if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && numbers1[0] === numbersall[a]) {
				if (colors2[0] !== undefined && fills2[0] !== undefined) {
					card9.push({
						color: colors2[0],
						shape: shapes1[0],
						fill: fills2[0],
						number: numbers1[0]
					});
                    possibleCards[i] = card9;
                    i = i +1;
				}
			}
			if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && numbers2[0] === numbersall[a]) {
				if (colors2[0] !== undefined && numbers2[0] !== undefined) {
					card10.push({
						color: colors2[0],
						shape: shapes1[0],
						fill: fills1[0],
						number: numbers2[0]
					});
                    possibleCards[i] = card10;
                    i = i +1;
				}
			}
			if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && numbers1[0] === numbersall[a]) {
				if (colors2[0] !== undefined && shapes2[0] !== undefined) {
					card11.push({
						color: colors2[0],
						shape: shapes2[0],
						fill: fills1[0],
						number: numbers1[0]
					});
                    possibleCards[i] = card11;
                    i = i +1;
				}
			}


			if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && numbers2[0] === numbersall[a]) {
				if (colors2[0] !== undefined && shapes2[0] !== undefined && fills2[0] !== undefined && numbers2[0] !== undefined ) {
					card12.push({
						color: colors2[0],
						shape: shapes2[0],
						fill: fills2[0],
						number: numbers2[0]
					});
                    possibleCards[i] = card12;
                    i = i +1;
				}
			}
			if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && numbers1[0] === numbersall[a]) {
				if(colors2[0] !== undefined && shapes2[0] !== undefined && fills2[0] !== undefined) {
					card13.push({
						color: colors2[0],
						shape: shapes2[0],
						fill: fills2[0],
						number: numbers1[0]
					});
                    possibleCards[i] = card13;
                    i = i +1;
				}
			}
			if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && numbers2[0] === numbersall[a]) {
				if(colors2[0] !== undefined && shapes2[0] !== undefined && numbers2[0] !== undefined ) {
					card14.push({
						color: colors2[0],
						shape: shapes2[0],
						fill: fills1[0],
						number: numbers2[0]
					});
                    possibleCards[i] = card14;
                    i = i +1;
				}
			}
			if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && numbers2[0] === numbersall[a]) {
				if(colors2[0] !== undefined && fills2[0] !== undefined && numbers2[0] !== undefined ) {
					card15.push({
						color: colors2[0],
						shape: shapes1[0],
						fill: fills2[0],
						number: numbers2[0]
					});
                    possibleCards[i] = card15;
                    i = i +1;
				}
			}
			if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && numbers2[0] === numbersall[a]) {
				if (shapes2[0] !== undefined && fills2[0] !== undefined && numbers2[0] !== undefined) {
					card16.push({
						color: colors1[0],
						shape: shapes2[0],
						fill: fills2[0],
						number: numbers2[0]
					});
                    possibleCards[i] = card16;
                    i = i +1;
				}
			}

		}
		return possibleCards;
	},


    generer16Cartes5: function(colors, shapes, fills, borders, numbers, allCards) {
        var i = 0;
        var possibleCards = [];
        var colors1 = [];
        var shapes1 = [];
        var fills1 = [];
        var borders1 = [];
        var numbers1 = [];
        var colors2 = [];
        var shapes2 = [];
        var fills2 = [];
        var borders2 = [];
        var numbers2 = [];

        var card1 = [];
        var card2 = [];
        var card3 = [];
        var card4 = [];
        var card5 = [];
        var card6 = [];
        var card7 = [];
        var card8 = [];
        var card9 = [];
        var card10 = [];
        var card11 = [];
        var card12 = [];
        var card13 = [];
        var card14 = [];
        var card15 = [];
        var card16 = [];
        var card17 = [];
        var card18 = [];
        var card19 = [];
        var card20 = [];
        var card21 = [];
        var card22 = [];
        var card23 = [];
        var card24 = [];
        var card25 = [];
        var card26 = [];
        var card27 = [];
        var card28 = [];
        var card29 = [];
        var card30 = [];
        var card31 = [];
        var card32 = [];




        var colorsall = $.map(allCards, function (el) {
            return el.color;
        });
        var shapesall = $.map(allCards, function (el) {
            return el.shape;
        });
        var fillsall = $.map(allCards, function (el) {
            return el.fill;
        });
        var bordersall = $.map(allCards, function (el) {
            return el.border;
        });
        var numbersall = $.map(allCards, function (el) {
            return el.number;
        });

        var colorsSet = new Set(colors);
        var shapesSet = new Set(shapes);
        var fillsSet = new Set(fills);
        var bordersSet = new Set(borders);
        var numbersSet = new Set(numbers);

        if (colors[0] === colors[1]) {
            colors1.push(colors[1]);
        } else {
            if (colorsSet.has('green') && colorsSet.has('red')) {
                colors1.push('purple');
                colors2.push('lightblue');
            }
            if (colorsSet.has('green') && colorsSet.has('purple')) {
                colors1.push('red');
                colors2.push('lightblue');
            }
            if (colorsSet.has('green') && colorsSet.has('lightblue')) {
                colors1.push('red');
                colors2.push('purple');
            }
            if (colorsSet.has('red') && colorsSet.has('purple')) {
                colors1.push('green');
                colors2.push('lightblue');
            }
            if (colorsSet.has('red') && colorsSet.has('lightblue')) {
                colors1.push('green');
                colors2.push('purple');
            }
            if (colorsSet.has('purple') && colorsSet.has('lightblue')) {
                colors1.push('green');
                colors2.push('red');
            }
        }
        if (shapes[0] === shapes[1]) {
            shapes1.push(shapes[1]);
        } else {
            if (shapesSet.has('oval') && shapesSet.has('diamond')) {
                shapes1.push('rectangle');
                shapes2.push('wave');
            }
            if (shapesSet.has('oval') && shapesSet.has('rectangle')) {
                shapes1.push('diamond');
                shapes2.push('wave');
            }
            if (shapesSet.has('oval') && shapesSet.has('wave')) {
                shapes1.push('diamond');
                shapes2.push('rectangle');
            }
            if (shapesSet.has('diamond') && shapesSet.has('rectangle')) {
                shapes1.push('oval');
                shapes2.push('wave');
            }
            if (shapesSet.has('diamond') && shapesSet.has('wave')) {
                shapes1.push('oval');
                shapes2.push('rectangle');
            }
            if (shapesSet.has('rectangle') && shapesSet.has('wave')) {
                shapes1.push('diamond');
                shapes2.push('oval');
            }
        }

        if (fills[0] === fills[1]) {
            fills1.push(fills[1]);
        } else {
            if (fillsSet.has('solid') && fillsSet.has('open')) {
                fills1.push('quadrillage');
                fills2.push('stripped');
            }
            if (fillsSet.has('solid') && fillsSet.has('quadrillage')) {
                fills1.push('open');
                fills2.push('stripped');
            }
            if (fillsSet.has('solid') && fillsSet.has('stripped')) {
                fills1.push('quadrillage');
                fills2.push('open');
            }
            if (fillsSet.has('open') && fillsSet.has('quadrillage')) {
                fills1.push('solid');
                fills2.push('stripped');
            }
            if (fillsSet.has('open') && fillsSet.has('stripped')) {
                fills1.push('solid');
                fills2.push('quadrillage');
            }
            if (fillsSet.has('stripped') && fillsSet.has('quadrillage')) {
                fills1.push('solid');
                fills2.push('open');
            }
        }


        if (borders[0] === borders[1]) {
            borders1.push(borders[1]);
        } else {
            if (bordersSet.has('plein') && bordersSet.has('point')) {
                borders1.push('rond');
                borders2.push('zigzag');
            }
            if (bordersSet.has('plein') && bordersSet.has('rond')) {
                borders1.push('point');
                borders2.push('zigzag');
            }
            if (bordersSet.has('plein') && bordersSet.has('zigzag')) {
                borders1.push('point');
                borders2.push('rond');
            }
            if (bordersSet.has('point') && bordersSet.has('rond')) {
                borders1.push('plein');
                borders2.push('zigzag');
            }
            if (bordersSet.has('point') && bordersSet.has('zigzag')) {
                borders1.push('plein');
                borders2.push('rond');
            }
            if (bordersSet.has('rond') && bordersSet.has('zigzag')) {
                borders1.push('plein');
                borders2.push('point');
            }
        }


        if (numbers[0] === numbers[1]) {
            numbers1.push(numbers[1]);
        } else {
            if (numbersSet.has(1) && numbersSet.has(2)) {
                numbers1.push(3);
                numbers2.push(4);
            }
            if (numbersSet.has(1) && numbersSet.has(3)) {
                numbers1.push(2);
                numbers2.push(4);
            }
            if (numbersSet.has(1) && numbersSet.has(4)) {
                numbers1.push(2);
                numbers2.push(3);
            }
            if (numbersSet.has(2) && numbersSet.has(3)) {
                numbers1.push(1);
                numbers2.push(4);
            }
            if (numbersSet.has(2) && numbersSet.has(4)) {
                numbers1.push(1);
                numbers2.push(3);
            }
            if (numbersSet.has(3) && numbersSet.has(4)) {
                numbers1.push(1);
                numbers2.push(2);
            }
        }


        for (var a = 0; a < allCards.length; a++) {
            if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                card1.push({
                    color: colors1[0],
                    shape: shapes1[0],
                    fill: fills1[0],
                    border: borders1[0],
                    number: numbers1[0]
                });
                possibleCards[i] = card1;
                i = i + 1;
            }
            if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (colors2[0] !== undefined) {
                    card2.push({
                        color: colors2[0],
                        shape: shapes1[0],
                        fill: fills1[0],
                        border: borders1[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card2;
                    i = i + 1;
                }
            }
            if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (shapes2[0] !== undefined) {
                    card3.push({
                        color: colors1[0],
                        shape: shapes2[0],
                        fill: fills1[0],
                        border: borders1[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card3;
                    i = i + 1;
                }
            }
            if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (fills2[0] !== undefined) {
                    card4.push({
                        color: colors1[0],
                        shape: shapes1[0],
                        fill: fills2[0],
                        border: borders1[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card4;
                    i = i + 1;
                }
            }
            if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (borders2[0] !== undefined) {
                    card5.push({
                        color: colors1[0],
                        shape: shapes1[0],
                        fill: fills1[0],
                        border: borders2[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card5;
                    i = i + 1;
                }
            }
            if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (numbers2[0] !== undefined) {
                    card6.push({
                        color: colors1[0],
                        shape: shapes1[0],
                        fill: fills1[0],
                        border: borders1[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card6;
                    i = i + 1;
                }
            }


            if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (borders2[0] !== undefined && numbers2[0] !== undefined) {
                    card7.push({
                        color: colors1[0],
                        shape: shapes1[0],
                        fill: fills1[0],
                        border: borders2[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card7;
                    i = i + 1;
                }
            }
            if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (fills2[0] !== undefined && numbers2[0] !== undefined) {
                    card8.push({
                        color: colors1[0],
                        shape: shapes1[0],
                        fill: fills2[0],
                        border: borders1[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card8;
                    i = i + 1;
                }
            }
            if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (shapes2[0] !== undefined && numbers2[0] !== undefined) {
                    card9.push({
                        color: colors1[0],
                        shape: shapes2[0],
                        fill: fills1[0],
                        border: borders1[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card9;
                    i = i + 1;
                }
            }
            if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (colors2[0] !== undefined && numbers2[0] !== undefined) {
                    card10.push({
                        color: colors2[0],
                        shape: shapes1[0],
                        fill: fills1[0],
                        border: borders1[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card10;
                    i = i + 1;
                }
            }


            if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (fills2[0] !== undefined && borders2[0] !== undefined) {
                    card11.push({
                        color: colors1[0],
                        shape: shapes1[0],
                        fill: fills2[0],
                        border: borders2[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card11;
                    i = i + 1;
                }
            }
            if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (shapes2[0] !== undefined && borders2[0] !== undefined) {
                    card12.push({
                        color: colors1[0],
                        shape: shapes2[0],
                        fill: fills1[0],
                        border: borders2[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card12;
                    i = i + 1;
                }
            }
            if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (colors2[0] !== undefined && borders2[0] !== undefined) {
                    card13.push({
                        color: colors2[0],
                        shape: shapes1[0],
                        fill: fills1[0],
                        border: borders2[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card13;
                    i = i + 1;
                }
            }


            if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (shapes2[0] !== undefined && fills2[0] !== undefined) {
                    card14.push({
                        color: colors1[0],
                        shape: shapes2[0],
                        fill: fills2[0],
                        border: borders1[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card14;
                    i = i + 1;
                }
            }
            if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (fills2[0] !== undefined && colors2[0] !== undefined) {
                    card15.push({
                        color: colors2[0],
                        shape: shapes1[0],
                        fill: fills2[0],
                        border: borders1[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card15;
                    i = i + 1;
                }
            }

            if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (colors2[0] !== undefined && shapes2[0] !== undefined) {
                    card16.push({
                        color: colors2[0],
                        shape: shapes2[0],
                        fill: fills1[0],
                        border: borders1[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card16;
                    i = i + 1;
                }
            }


            if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (colors2[0] !== undefined && shapes2[0] !== undefined && fills2[0] !== undefined) {
                    card17.push({
                        color: colors2[0],
                        shape: shapes2[0],
                        fill: fills2[0],
                        border: borders1[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card17;
                    i = i + 1;
                }
            }
            if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (colors2[0] !== undefined && shapes2[0] !== undefined && borders2[0] !== undefined) {
                    card18.push({
                        color: colors2[0],
                        shape: shapes2[0],
                        fill: fills1[0],
                        border: borders2[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card18;
                    i = i + 1;
                }
            }

            if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (colors2[0] !== undefined && shapes2[0] !== undefined && numbers2[0] !== undefined) {
                    card19.push({
                        color: colors2[0],
                        shape: shapes2[0],
                        fill: fills1[0],
                        border: borders1[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card19;
                    i = i + 1;
                }
            }
            if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (colors2[0] !== undefined && fills2[0] !== undefined && borders2[0] !== undefined) {
                    card20.push({
                        color: colors2[0],
                        shape: shapes1[0],
                        fill: fills2[0],
                        border: borders2[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card20;
                    i = i + 1;
                }
            }
            if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (colors2[0] !== undefined && fills2[0] !== undefined && numbers2[0] !== undefined) {
                    card21.push({
                        color: colors2[0],
                        shape: shapes1[0],
                        fill: fills2[0],
                        border: borders1[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card21;
                    i = i + 1;
                }
            }

            if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (colors2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                    card22.push({
                        color: colors2[0],
                        shape: shapes1[0],
                        fill: fills1[0],
                        border: borders2[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card22;
                    i = i + 1;
                }
            }

                if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (shapes2[0] !== undefined && fills2[0] !== undefined && borders2[0] !== undefined) {
                        card23.push({
                            color: colors1[0],
                            shape: shapes2[0],
                            fill: fills2[0],
                            border: borders2[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card23;
                        i = i + 1;
                    }
                }


            if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (shapes2[0] !== undefined && fills2[0] !== undefined && numbers2[0] !== undefined) {
                    card24.push({
                        color: colors1[0],
                        shape: shapes2[0],
                        fill: fills2[0],
                        border: borders1[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card24;
                    i = i + 1;
                }
            }
            if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (shapes2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                    card25.push({
                        color: colors1[0],
                        shape: shapes2[0],
                        fill: fills1[0],
                        border: borders2[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card25;
                    i = i + 1;
                }
            }

            if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (fills2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                    card26.push({
                        color: colors1[0],
                        shape: shapes1[0],
                        fill: fills2[0],
                        border: borders2[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card26;
                    i = i + 1;
                }
            }


            if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (colors2[0] !== undefined && shapes2[0] !== undefined && fills2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                    card27.push({
                        color: colors2[0],
                        shape: shapes2[0],
                        fill: fills2[0],
                        border: borders2[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card27;
                    i = i + 1;
                }
            }


            if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (shapes2[0] !== undefined && fills2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                    card28.push({
                        color: colors1[0],
                        shape: shapes2[0],
                        fill: fills2[0],
                        border: borders2[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card28;
                    i = i + 1;
                }
            }

            if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (colors2[0] !== undefined && borders2[0] !== undefined && fills2[0] !== undefined && numbers2[0] !== undefined) {
                    card29.push({
                        color: colors2[0],
                        shape: shapes1[0],
                        fill: fills2[0],
                        border: borders2[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card29;
                    i = i + 1;
                }
            }

            if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (colors2[0] !== undefined && shapes2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                    card30.push({
                        color: colors2[0],
                        shape: shapes2[0],
                        fill: fills1[0],
                        border: borders2[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card30;
                    i = i + 1;
                }
            }

            if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                if (colors2[0] !== undefined && shapes2[0] !== undefined && fills2[0] !== undefined && numbers2[0] !== undefined) {
                    card31.push({
                        color: colors2[0],
                        shape: shapes2[0],
                        fill: fills2[0],
                        border: borders1[0],
                        number: numbers2[0]
                    });
                    possibleCards[i] = card31;
                    i = i + 1;
                }
            }

            if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                if (colors2[0] !== undefined && shapes2[0] !== undefined && fills2[0] !== undefined && borders2[0] !== undefined) {
                    card32.push({
                        color: colors2[0],
                        shape: shapes2[0],
                        fill: fills2[0],
                        border: borders2[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card32;
                    i = i + 1;
                }
            }
        }
        return possibleCards;
    },

    existingHyperSet2: function() {
        var colors1 = [];
        var shapes1 =[];
        var fills1= [];
        var numbers1= [];
        var colors2 = [];
        var shapes2 =[];
        var fills2= [];
        var numbers2= [];




        var self = this;
        var cartes = self.cardsHS2;
        var cartes1 = cartes;
        var cartes2 = cartes;



        var carte1, carte4, carte6 =[];
        var carte2;


        var cardVirtualABC = [];


        var tCardsABC = [];




        var tCardsDGE = [];
        var pCards = [];
		var pCards2 = [];


        var i, j, a, k ,z;

        var colorsall= $.map(this.cardsHS2, function(el) { return el.color;});
        var shapesall = $.map(this.cardsHS2, function(el) { return el.shape; });
        var fillsall= $.map(this.cardsHS2,function(el) { return el.fill; });
        var numbersall = $.map(this.cardsHS2, function(el) { return el.number;});


        for(i=0; i < cartes.length-2; i++) {
            for (j = i + 1; j < cartes1.length - 1; j++) {
                for (k = j + 1; k < cartes2.length; k++) {

                    carte1 = cartes[i];
                    carte2 = cartes1[j];
                    carte4 = cartes2[k];


                    colors1.splice(0, 1, carte1.color);
                    colors1.splice(1, 1, carte2.color);

                    shapes1.splice(0, 1, carte1.shape);
                    shapes1.splice(1, 1, carte2.shape);

                    fills1.splice(0, 1, carte1.fill);
                    fills1.splice(1, 1, carte2.fill);

                    numbers1.splice(0, 1, carte1.number);
                    numbers1.splice(1, 1, carte2.number);


                    pCards = self.generer16Cartes(colors1, shapes1, fills1, numbers1, this.cardsHS2);

                    if (pCards.length > 0) {

                        for (z = 0; z < pCards.length; z++) {
                            tCardsABC[0] = carte1;
                            tCardsABC[1] = carte2;
                            tCardsABC[2] = pCards[z][0];

                            console.log('HS 4x4 : ');
                            console.log('carte1 :', tCardsABC[0]);
                            console.log('carte2 :', tCardsABC[1]);
                            console.log('carte3 :', tCardsABC[2]);


                            cardVirtualABC[z] = this.generateVirtualCard44(tCardsABC);

                            console.log('carteVirtuelle :', cardVirtualABC[z]);

                            if (carte4.color !== tCardsABC[2].color && carte4.shape !== tCardsABC[2].shape && carte4.fill !== tCardsABC[2].fill && carte4.number !== tCardsABC[2].number) {


                                tCardsDGE[0] = carte4;
                                tCardsDGE[1] = cardVirtualABC[z][0];
                                tCardsDGE[2] = [];

                                console.log('carte4 :', tCardsDGE[0]);


                                colors2 = $.map(tCardsDGE, function (el) {
                                    return el.color;
                                });
                                shapes2 = $.map(tCardsDGE, function (el) {
                                    return el.shape;
                                });
                                fills2 = $.map(tCardsDGE, function (el) {
                                    return el.fill;
                                });
                                numbers2 = $.map(tCardsDGE, function (el) {
                                    return el.number;
                                });

                                pCards2 = self.generer16Cartes(colors2, shapes2, fills2, numbers2, this.cardsHS2);


                                for (var w = 0; w < pCards2.length; w++) {
                                    tCardsDGE[2] = pCards2[w][0];
                                    carte6[w] = this.generateVirtualCard44(tCardsDGE);
                                    console.log('carte5 :', tCardsDGE[2]);

                                }

                            }
                        }
                    }


                    for(var x=0; x< carte6.length; x++) {
                        for (a = k + 1; a < self.cardsHS2.length; a++) {
                            if (carte6[x][0].color !== undefined && carte6[x][0].shape !== undefined && carte6[x][0].fill !== undefined && carte6[x][0].number !== undefined) {
                                if (carte6[x][0].shape === shapesall[a] && carte6[x][0].color === colorsall[a] && carte6[x][0].fill === fillsall[a] && carte6[x][0].number === numbersall[a]) {
                                    console.log('carte6 :', carte6[x][0]);
                                    //console.log('ab');
                                   //console.log(carte6[x][0].shape, carte6[x][0].color, carte6[x][0].fill, carte6[x][0].number);
                                    //console.log(carte1);
                                    //console.log(carte2);
                                    self.setHS2 = self.setHS2 + 1;
                                }
                            }
                        }
                    }

                    carte6 = [];


                }
            }
        }

        this.$nbSetsHS2.html(self.setHS2);
    },


   existingHyperSet3: function() {
        var colors1 = [];
        var shapes1 =[];
        var fills1= [];
        var borders1 =[];
        var numbers1= [];
        var colors2 = [];
        var shapes2 =[];
        var fills2= [];
        var borders2 =[];
        var numbers2= [];


        var self = this;
        var cartes = self.cardsHS3;
        var cartes1 = cartes;
        var cartes2 = cartes;

        var carte1, carte4, carte6 =[];
        var carte2;

        var cardVirtualABC = [];

        var tCardsABC = [];

        var tCardsDGE = [];
        var pCards = [];
        var pCards2 = [];

        var i, j, a, k ,z;

        var colorsall= $.map(this.cardsHS3, function(el) { return el.color;});
        var shapesall = $.map(this.cardsHS3, function(el) { return el.shape; });
        var fillsall= $.map(this.cardsHS3,function(el) { return el.fill; });
        var bordersall= $.map(this.cardsHS3,function(el) { return el.border; });
        var numbersall = $.map(this.cardsHS3, function(el) { return el.number;});


        for(i=0; i < cartes.length-2; i++) {
            for (j = i + 1; j < cartes1.length - 1; j++) {
                for (k = j + 1; k < cartes2.length; k++) {

                    carte1 = cartes[i];
                    carte2 = cartes1[j];
                    carte4 = cartes2[k];


                    colors1.splice(0, 1, carte1.color);
                    colors1.splice(1, 1, carte2.color);

                    shapes1.splice(0, 1, carte1.shape);
                    shapes1.splice(1, 1, carte2.shape);

                    fills1.splice(0, 1, carte1.fill);
                    fills1.splice(1, 1, carte2.fill);

                    borders1.splice(0, 1, carte1.border);
                    borders1.splice(1, 1, carte2.border);

                    numbers1.splice(0, 1, carte1.number);
                    numbers1.splice(1, 1, carte2.number);

                    pCards = self.generer16Cartes5(colors1, shapes1, fills1, borders1, numbers1, this.cardsHS3);


                    if (pCards.length > 0) {

                        for (z = 0; z < pCards.length; z++) {
                            tCardsABC[0] = carte1;
                            tCardsABC[1] = carte2;
                            tCardsABC[2] = pCards[z][0];

                            console.log('HS 4x4 : ');
                            console.log('carte1 :', tCardsABC[0]);
                            console.log('carte2 :', tCardsABC[1]);
                            console.log('carte3 :', tCardsABC[2]);

                            cardVirtualABC[z] = this.generateVirtualCard45(tCardsABC);

                            console.log('carteVirtuelle :', cardVirtualABC[z]);


                            if (carte4.color !== tCardsABC[2].color && carte4.shape !== tCardsABC[2].shape && carte4.fill !== tCardsABC[2].fill && carte4.number !== tCardsABC[2].number) {

                                tCardsDGE[0] = carte4;
                                tCardsDGE[1] = cardVirtualABC[z][0];
                                tCardsDGE[2] = [];
                                console.log('carte4 :', tCardsDGE[0]);


                                colors2 = $.map(tCardsDGE, function (el) {
                                    return el.color;
                                });
                                shapes2 = $.map(tCardsDGE, function (el) {
                                    return el.shape;
                                });
                                fills2 = $.map(tCardsDGE, function (el) {
                                    return el.fill;
                                });
                                borders2 = $.map(tCardsDGE, function (el) {
                                    return el.border;
                                });
                                numbers2 = $.map(tCardsDGE, function (el) {
                                    return el.number;
                                });

                                pCards2 = self.generer16Cartes5(colors2, shapes2, fills2, borders2, numbers2, this.cardsHS3);

                                for (var w = 0; w < pCards2.length; w++) {
                                    tCardsDGE[2] = pCards2[w][0];
                                    console.log('carte5 :', tCardsDGE[2]);
                                    carte6[w] = this.generateVirtualCard45(tCardsDGE);

                                }
                            }

                        }

                    }


                    for(var x=0; x< carte6.length; x++) {
                        for (a = k + 1; a < self.cardsHS3.length; a++) {
                            if (carte6[x][0].color !== undefined && carte6[x][0].shape !== undefined && carte6[x][0].fill !== undefined  && carte6[x][0].border !== undefined  && carte6[x][0].number !== undefined) {
                                if (carte6[x][0].shape === shapesall[a] && carte6[x][0].color === colorsall[a] && carte6[x][0].fill === fillsall[a] && carte6[x][0].border === bordersall[a] && carte6[x][0].number === numbersall[a]) {
                                    console.log('carte6 :', carte6[x][0]);
                                    //console.log('ab');
                                    //console.log(carte6[x][0].shape, carte6[x][0].color, carte6[x][0].fill, carte6[x][0].number);
                                    //console.log(carte1);
                                    //console.log(carte2);
                                    self.setHS3 = self.setHS3 + 1;
                                }
                            }
                        }
                    }

                    carte6 = [];
                    pCards = [];
                    pCards2 = [];


                }
            }
        }

        this.$nbSetsHS3.html(self.setHS3);
    },


    generer16Cartes55: function(colors, shapes, fills, borders, numbers, allCards) {
        var i = 0;
        var possibleCards = [];
        var colors1 = [];
        var shapes1 = [];
        var fills1 = [];
        var borders1 = [];
        var numbers1 = [];
        var colors2 = [];
        var shapes2 = [];
        var fills2 = [];
        var borders2 = [];
        var numbers2 = [];

        var card1 = [];
        var card2 = [];
        var card3 = [];
        var card4 = [];
        var card5 = [];
        var card6 = [];
        var card7 = [];
        var card8 = [];
        var card9 = [];
        var card10 = [];
        var card11 = [];
        var card12 = [];
        var card13 = [];
        var card14 = [];
        var card15 = [];
        var card16 = [];
        var card17 = [];
        var card18 = [];
        var card19 = [];
        var card20 = [];
        var card21 = [];
        var card22 = [];
        var card23 = [];
        var card24 = [];
        var card25 = [];
        var card26 = [];
        var card27 = [];
        var card28 = [];
        var card29 = [];
        var card30 = [];
        var card31 = [];
        var card32 = [];




        var colorsall = $.map(allCards, function (el) {
            return el.color;
        });
        var shapesall = $.map(allCards, function (el) {
            return el.shape;
        });
        var fillsall = $.map(allCards, function (el) {
            return el.fill;
        });
        var bordersall = $.map(allCards, function (el) {
            return el.border;
        });
        var numbersall = $.map(allCards, function (el) {
            return el.number;
        });

        var colorsSet = new Set(colors);
        var shapesSet = new Set(shapes);
        var fillsSet = new Set(fills);
        var bordersSet = new Set(borders);
        var numbersSet = new Set(numbers);

        if (colors[0] === colors[1]) {
            colors1.push(colors[1]);
        } else {
            if (colorsSet.has('green') && colorsSet.has('red') && colorsSet.has('yellow')) {
                colors1.push('purple');
                colors2.push('lightblue');
            }
            if (colorsSet.has('green') && colorsSet.has('purple') && colorsSet.has('yellow')) {
                colors1.push('red');
                colors2.push('lightblue');
            }
            if (colorsSet.has('green') && colorsSet.has('lightblue') && colorsSet.has('yellow')) {
                colors1.push('red');
                colors2.push('purple');
            }
            if (colorsSet.has('red') && colorsSet.has('purple') && colorsSet.has('yellow')) {
                colors1.push('green');
                colors2.push('lightblue');
            }
            if (colorsSet.has('red') && colorsSet.has('lightblue') && colorsSet.has('yellow')) {
                colors1.push('green');
                colors2.push('purple');
            }
            if (colorsSet.has('purple') && colorsSet.has('lightblue') && colorsSet.has('yellow')) {
                colors1.push('green');
                colors2.push('red');
            }
            if (colorsSet.has('red') && colorsSet.has('purple') && colorsSet.has('lightblue')) {
                colors1.push('green');
                colors2.push('yellow');
            }
            if (colorsSet.has('red') && colorsSet.has('lightblue') && colorsSet.has('green')) {
                colors1.push('yellow');
                colors2.push('purple');
            }
            if (colorsSet.has('red') && colorsSet.has('purple') && colorsSet.has('green')) {
                colors1.push('yellow');
                colors2.push('lightblue');
            }
            if (colorsSet.has('purple') && colorsSet.has('lightblue') && colorsSet.has('green')) {
                colors1.push('yellow');
                colors2.push('red');
            }
        }
        if (shapes[0] === shapes[1]) {
            shapes1.push(shapes[1]);
        } else {
            if (shapesSet.has('oval') && shapesSet.has('diamond') && shapesSet.has('triangle')) {
                shapes1.push('rectangle');
                shapes2.push('wave');
            }
            if (shapesSet.has('oval') && shapesSet.has('rectangle') && shapesSet.has('triangle')) {
                shapes1.push('diamond');
                shapes2.push('wave');
            }
            if (shapesSet.has('oval') && shapesSet.has('wave') && shapesSet.has('triangle')) {
                shapes1.push('diamond');
                shapes2.push('rectangle');
            }
            if (shapesSet.has('diamond') && shapesSet.has('rectangle') && shapesSet.has('triangle')) {
                shapes1.push('oval');
                shapes2.push('wave');
            }
            if (shapesSet.has('diamond') && shapesSet.has('wave') && shapesSet.has('triangle')) {
                shapes1.push('oval');
                shapes2.push('rectangle');
            }
            if (shapesSet.has('rectangle') && shapesSet.has('wave') && shapesSet.has('triangle')) {
                shapes1.push('diamond');
                shapes2.push('oval');
            }
            if (shapesSet.has('oval') && shapesSet.has('wave') && shapesSet.has('rectangle')) {
                shapes1.push('diamond');
                shapes2.push('triangle');
            }
            if (shapesSet.has('oval') && shapesSet.has('rectangle') && shapesSet.has('diamond')) {
                shapes1.push('triangle');
                shapes2.push('wave');
            }
            if (shapesSet.has('diamond') && shapesSet.has('wave') && shapesSet.has('rectangle')) {
                shapes1.push('oval');
                shapes2.push('triangle');
            }
            if (shapesSet.has('wave') && shapesSet.has('oval') && shapesSet.has('diamond')) {
                shapes1.push('triangle');
                shapes2.push('rectangle');
            }
        }

        if (fills[0] === fills[1]) {
            fills1.push(fills[1]);
        } else {
            if (fillsSet.has('solid') && fillsSet.has('open') && fillsSet.has('pointille')) {
                fills1.push('quadrillage');
                fills2.push('stripped');
            }
            if (fillsSet.has('solid') && fillsSet.has('quadrillage') && fillsSet.has('pointille')) {
                fills1.push('open');
                fills2.push('stripped');
            }
            if (fillsSet.has('solid') && fillsSet.has('stripped') && fillsSet.has('pointille')) {
                fills1.push('quadrillage');
                fills2.push('open');
            }
            if (fillsSet.has('open') && fillsSet.has('quadrillage') && fillsSet.has('pointille')) {
                fills1.push('solid');
                fills2.push('stripped');
            }
            if (fillsSet.has('open') && fillsSet.has('stripped') && fillsSet.has('pointille')) {
                fills1.push('solid');
                fills2.push('quadrillage');
            }
            if (fillsSet.has('stripped') && fillsSet.has('quadrillage') && fillsSet.has('pointille')) {
                fills1.push('solid');
                fills2.push('open');
            }


            if (fillsSet.has('solid') && fillsSet.has('stripped') && fillsSet.has('open')) {
                fills1.push('quadrillage');
                fills2.push('pointille');
            }
            if (fillsSet.has('open') && fillsSet.has('quadrillage') && fillsSet.has('stripped')) {
                fills1.push('solid');
                fills2.push('pointille');
            }
            if (fillsSet.has('open') && fillsSet.has('quadrillage') && fillsSet.has('solid')) {
                fills1.push('stripped');
                fills2.push('pointille');
            }
            if (fillsSet.has('stripped') && fillsSet.has('quadrillage') && fillsSet.has('solid')) {
                fills1.push('pointille');
                fills2.push('open');
            }
        }


        if (borders[0] === borders[1]) {
            borders1.push(borders[1]);
        } else {
            if (bordersSet.has('plein') && bordersSet.has('point') && bordersSet.has('hachure')) {
                borders1.push('rond');
                borders2.push('zigzag');
            }
            if (bordersSet.has('plein') && bordersSet.has('rond') && bordersSet.has('hachure')) {
                borders1.push('point');
                borders2.push('zigzag');
            }
            if (bordersSet.has('plein') && bordersSet.has('zigzag') && bordersSet.has('hachure')) {
                borders1.push('point');
                borders2.push('rond');
            }
            if (bordersSet.has('point') && bordersSet.has('rond') && bordersSet.has('hachure')) {
                borders1.push('plein');
                borders2.push('zigzag');
            }
            if (bordersSet.has('point') && bordersSet.has('zigzag') && bordersSet.has('hachure')) {
                borders1.push('plein');
                borders2.push('rond');
            }
            if (bordersSet.has('rond') && bordersSet.has('zigzag') && bordersSet.has('hachure')) {
                borders1.push('plein');
                borders2.push('point');
            }


            if (bordersSet.has('plein') && bordersSet.has('zigzag') && bordersSet.has('point')) {
                borders1.push('hachure');
                borders2.push('rond');
            }
            if (bordersSet.has('point') && bordersSet.has('rond') && bordersSet.has('plein')) {
                borders1.push('hachure');
                borders2.push('zigzag');
            }
            if (bordersSet.has('point') && bordersSet.has('zigzag') && bordersSet.has('rond')) {
                borders1.push('plein');
                borders2.push('hachure');
            }
            if (bordersSet.has('rond') && bordersSet.has('zigzag') && bordersSet.has('plein')) {
                borders1.push('hachure');
                borders2.push('point');
            }
        }


        if (numbers[0] === numbers[1]) {
            numbers1.push(numbers[1]);
        } else {
            if (numbersSet.has(1) && numbersSet.has(2) && numbersSet.has(5)) {
                numbers1.push(3);
                numbers2.push(4);
            }
            if (numbersSet.has(1) && numbersSet.has(3) && numbersSet.has(5)) {
                numbers1.push(2);
                numbers2.push(4);
            }
            if (numbersSet.has(1) && numbersSet.has(4) && numbersSet.has(5)) {
                numbers1.push(2);
                numbers2.push(3);
            }
            if (numbersSet.has(2) && numbersSet.has(3) && numbersSet.has(5)) {
                numbers1.push(1);
                numbers2.push(4);
            }
            if (numbersSet.has(2) && numbersSet.has(4) && numbersSet.has(5)) {
                numbers1.push(1);
                numbers2.push(3);
            }
            if (numbersSet.has(3) && numbersSet.has(4) && numbersSet.has(5)) {
                numbers1.push(1);
                numbers2.push(2);
            }


            if (numbersSet.has(1) && numbersSet.has(4) && numbersSet.has(3)) {
                numbers1.push(2);
                numbers2.push(5);
            }
            if (numbersSet.has(2) && numbersSet.has(3) && numbersSet.has(1)) {
                numbers1.push(5);
                numbers2.push(4);
            }
            if (numbersSet.has(2) && numbersSet.has(4) && numbersSet.has(1)) {
                numbers1.push(5);
                numbers2.push(3);
            }
            if (numbersSet.has(3) && numbersSet.has(4) && numbersSet.has(2)) {
                numbers1.push(1);
                numbers2.push(5);
            }
        }


        if(shapes1[0] === undefined && colors1[0] === undefined && fills1[0] === undefined && borders1[0] === undefined && numbers1[0] === undefined) {
            return undefined;
        }else{
            for (var a = 0; a < allCards.length; a++) {
                if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    card1.push({
                        color: colors1[0],
                        shape: shapes1[0],
                        fill: fills1[0],
                        border: borders1[0],
                        number: numbers1[0]
                    });
                    possibleCards[i] = card1;
                    i = i + 1;
                }
                if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (colors2[0] !== undefined) {
                        card2.push({
                            color: colors2[0],
                            shape: shapes1[0],
                            fill: fills1[0],
                            border: borders1[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card2;
                        i = i + 1;
                    }
                }
                if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (shapes2[0] !== undefined) {
                        card3.push({
                            color: colors1[0],
                            shape: shapes2[0],
                            fill: fills1[0],
                            border: borders1[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card3;
                        i = i + 1;
                    }
                }
                if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (fills2[0] !== undefined) {
                        card4.push({
                            color: colors1[0],
                            shape: shapes1[0],
                            fill: fills2[0],
                            border: borders1[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card4;
                        i = i + 1;
                    }
                }
                if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (borders2[0] !== undefined) {
                        card5.push({
                            color: colors1[0],
                            shape: shapes1[0],
                            fill: fills1[0],
                            border: borders2[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card5;
                        i = i + 1;
                    }
                }
                if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (numbers2[0] !== undefined) {
                        card6.push({
                            color: colors1[0],
                            shape: shapes1[0],
                            fill: fills1[0],
                            border: borders1[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card6;
                        i = i + 1;
                    }
                }


                if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (borders2[0] !== undefined && numbers2[0] !== undefined) {
                        card7.push({
                            color: colors1[0],
                            shape: shapes1[0],
                            fill: fills1[0],
                            border: borders2[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card7;
                        i = i + 1;
                    }
                }
                if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (fills2[0] !== undefined && numbers2[0] !== undefined) {
                        card8.push({
                            color: colors1[0],
                            shape: shapes1[0],
                            fill: fills2[0],
                            border: borders1[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card8;
                        i = i + 1;
                    }
                }
                if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (shapes2[0] !== undefined && numbers2[0] !== undefined) {
                        card9.push({
                            color: colors1[0],
                            shape: shapes2[0],
                            fill: fills1[0],
                            border: borders1[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card9;
                        i = i + 1;
                    }
                }
                if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && numbers2[0] !== undefined) {
                        card10.push({
                            color: colors2[0],
                            shape: shapes1[0],
                            fill: fills1[0],
                            border: borders1[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card10;
                        i = i + 1;
                    }
                }


                if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (fills2[0] !== undefined && borders2[0] !== undefined) {
                        card11.push({
                            color: colors1[0],
                            shape: shapes1[0],
                            fill: fills2[0],
                            border: borders2[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card11;
                        i = i + 1;
                    }
                }
                if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (shapes2[0] !== undefined && borders2[0] !== undefined) {
                        card12.push({
                            color: colors1[0],
                            shape: shapes2[0],
                            fill: fills1[0],
                            border: borders2[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card12;
                        i = i + 1;
                    }
                }
                if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && borders2[0] !== undefined) {
                        card13.push({
                            color: colors2[0],
                            shape: shapes1[0],
                            fill: fills1[0],
                            border: borders2[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card13;
                        i = i + 1;
                    }
                }


                if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (shapes2[0] !== undefined && fills2[0] !== undefined) {
                        card14.push({
                            color: colors1[0],
                            shape: shapes2[0],
                            fill: fills2[0],
                            border: borders1[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card14;
                        i = i + 1;
                    }
                }
                if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (fills2[0] !== undefined && colors2[0] !== undefined) {
                        card15.push({
                            color: colors2[0],
                            shape: shapes1[0],
                            fill: fills2[0],
                            border: borders1[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card15;
                        i = i + 1;
                    }
                }

                if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && shapes2[0] !== undefined) {
                        card16.push({
                            color: colors2[0],
                            shape: shapes2[0],
                            fill: fills1[0],
                            border: borders1[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card16;
                        i = i + 1;
                    }
                }


                if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && shapes2[0] !== undefined && fills2[0] !== undefined) {
                        card17.push({
                            color: colors2[0],
                            shape: shapes2[0],
                            fill: fills2[0],
                            border: borders1[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card17;
                        i = i + 1;
                    }
                }
                if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && shapes2[0] !== undefined && borders2[0] !== undefined) {
                        card18.push({
                            color: colors2[0],
                            shape: shapes2[0],
                            fill: fills1[0],
                            border: borders2[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card18;
                        i = i + 1;
                    }
                }

                if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && shapes2[0] !== undefined && numbers2[0] !== undefined) {
                        card19.push({
                            color: colors2[0],
                            shape: shapes2[0],
                            fill: fills1[0],
                            border: borders1[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card19;
                        i = i + 1;
                    }
                }
                if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && fills2[0] !== undefined && borders2[0] !== undefined) {
                        card20.push({
                            color: colors2[0],
                            shape: shapes1[0],
                            fill: fills2[0],
                            border: borders2[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card20;
                        i = i + 1;
                    }
                }
                if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && fills2[0] !== undefined && numbers2[0] !== undefined) {
                        card21.push({
                            color: colors2[0],
                            shape: shapes1[0],
                            fill: fills2[0],
                            border: borders1[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card21;
                        i = i + 1;
                    }
                }

                if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                        card22.push({
                            color: colors2[0],
                            shape: shapes1[0],
                            fill: fills1[0],
                            border: borders2[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card22;
                        i = i + 1;
                    }
                }

                    if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                        if (shapes2[0] !== undefined && fills2[0] !== undefined && borders2[0] !== undefined) {
                            card23.push({
                                color: colors1[0],
                                shape: shapes2[0],
                                fill: fills2[0],
                                border: borders2[0],
                                number: numbers1[0]
                            });
                            possibleCards[i] = card23;
                            i = i + 1;
                        }
                    }


                if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (shapes2[0] !== undefined && fills2[0] !== undefined && numbers2[0] !== undefined) {
                        card24.push({
                            color: colors1[0],
                            shape: shapes2[0],
                            fill: fills2[0],
                            border: borders1[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card24;
                        i = i + 1;
                    }
                }
                if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (shapes2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                        card25.push({
                            color: colors1[0],
                            shape: shapes2[0],
                            fill: fills1[0],
                            border: borders2[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card25;
                        i = i + 1;
                    }
                }

                if (shapes1[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (fills2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                        card26.push({
                            color: colors1[0],
                            shape: shapes1[0],
                            fill: fills2[0],
                            border: borders2[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card26;
                        i = i + 1;
                    }
                }


                if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && shapes2[0] !== undefined && fills2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                        card27.push({
                            color: colors2[0],
                            shape: shapes2[0],
                            fill: fills2[0],
                            border: borders2[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card27;
                        i = i + 1;
                    }
                }


                if (shapes2[0] === shapesall[a] && colors1[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (shapes2[0] !== undefined && fills2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                        card28.push({
                            color: colors1[0],
                            shape: shapes2[0],
                            fill: fills2[0],
                            border: borders2[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card28;
                        i = i + 1;
                    }
                }

                if (shapes1[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && borders2[0] !== undefined && fills2[0] !== undefined && numbers2[0] !== undefined) {
                        card29.push({
                            color: colors2[0],
                            shape: shapes1[0],
                            fill: fills2[0],
                            border: borders2[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card29;
                        i = i + 1;
                    }
                }

                if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills1[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && shapes2[0] !== undefined && borders2[0] !== undefined && numbers2[0] !== undefined) {
                        card30.push({
                            color: colors2[0],
                            shape: shapes2[0],
                            fill: fills1[0],
                            border: borders2[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card30;
                        i = i + 1;
                    }
                }

                if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders1[0] === bordersall[a] && numbers2[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && shapes2[0] !== undefined && fills2[0] !== undefined && numbers2[0] !== undefined) {
                        card31.push({
                            color: colors2[0],
                            shape: shapes2[0],
                            fill: fills2[0],
                            border: borders1[0],
                            number: numbers2[0]
                        });
                        possibleCards[i] = card31;
                        i = i + 1;
                    }
                }

                if (shapes2[0] === shapesall[a] && colors2[0] === colorsall[a] && fills2[0] === fillsall[a] && borders2[0] === bordersall[a] && numbers1[0] === numbersall[a]) {
                    if (colors2[0] !== undefined && shapes2[0] !== undefined && fills2[0] !== undefined && borders2[0] !== undefined) {
                        card32.push({
                            color: colors2[0],
                            shape: shapes2[0],
                            fill: fills2[0],
                            border: borders2[0],
                            number: numbers1[0]
                        });
                        possibleCards[i] = card32;
                        i = i + 1;
                    }
                }
            }
        }
        return possibleCards;
    },




        existingHyperSet4: function() {
        var colors1 = [];
        var shapes1 =[];
        var fills1= [];
        var borders1 =[];
        var numbers1= [];
        var colors2 = [];
        var shapes2 =[];
        var fills2= [];
        var borders2 =[];
        var numbers2= [];


        var self = this;
        var cartes = self.cardsHS4;
        var cartes1 = cartes;
        var cartes2 = cartes;
        var cartes3 = cartes;
        var cartes4 = cartes;

        var carte1, carte5, carte6, carte3, carte8 =[];
        var carte2;

        var cardVirtualABC = [];

        var tCardsABC = [];

        var tCardsDGE = [];
        var pCards = [];
        var pCards2 = [];

        var i, j, a, k ,z, l, m;

        var colorsall= $.map(this.cardsHS4, function(el) { return el.color;});
        var shapesall = $.map(this.cardsHS4, function(el) { return el.shape; });
        var fillsall= $.map(this.cardsHS4,function(el) { return el.fill; });
        var bordersall= $.map(this.cardsHS4,function(el) { return el.border; });
        var numbersall = $.map(this.cardsHS4, function(el) { return el.number;});


        for(i=0; i < cartes.length-2; i++) {
            for (j = i + 1; j < cartes1.length - 1; j++) {
                for (k = j + 1; k < cartes2.length; k++) {
                    for (l= k + 1; l < cartes3.length; l++) {
                       for (m = l+ 1; m < cartes4.length; m++) {


                            carte1 = cartes[i];
                            carte2 = cartes1[j];
                            carte3 = cartes2[k];

                            carte5 = cartes3[l];
                            carte6 = cartes4[m];


                            colors1.splice(0, 1, carte1.color);
                            colors1.splice(1, 1, carte2.color);
                            colors1.splice(2, 1, carte3.color);


                            shapes1.splice(0, 1, carte1.shape);
                            shapes1.splice(1, 1, carte2.shape);
                            shapes1.splice(2, 1, carte3.shape);


                            fills1.splice(0, 1, carte1.fill);
                            fills1.splice(1, 1, carte2.fill);
                            fills1.splice(2, 1, carte3.fill);


                            borders1.splice(0, 1, carte1.border);
                            borders1.splice(1, 1, carte2.border);
                            borders1.splice(2, 1, carte3.border);


                            numbers1.splice(0, 1, carte1.number);
                            numbers1.splice(1, 1, carte2.number);
                            numbers1.splice(2, 1, carte3.number);


                            pCards = self.generer16Cartes55(colors1, shapes1, fills1, borders1, numbers1, this.cardsHS4);

                            if(pCards !== undefined) {
                                if (pCards.length > 0) {

                                    for (z = 0; z < pCards.length; z++) {
                                        tCardsABC[0] = carte1;
                                        tCardsABC[1] = carte2;
                                        tCardsABC[2] = carte3;
                                        tCardsABC[3] = pCards[z][0];

                                        cardVirtualABC[z] = this.generateVirtualCard55(tCardsABC);


                                        if (carte5.color !== tCardsABC[2].color && carte5.shape !== tCardsABC[2].shape && carte5.fill !== tCardsABC[2].fill && carte5.number !== tCardsABC[2].number &&
                                            carte5.color !== tCardsABC[3].color && carte5.shape !== tCardsABC[3].shape && carte5.fill !== tCardsABC[3].fill && carte5.number !== tCardsABC[3].number) {

                                            tCardsDGE[0] = carte5;
                                            tCardsDGE[1] = carte6;
                                            tCardsDGE[2] = cardVirtualABC[z][0];
                                            tCardsDGE[3] = [];


                                            colors2 = $.map(tCardsDGE, function (el) {
                                                return el.color;
                                            });
                                            shapes2 = $.map(tCardsDGE, function (el) {
                                                return el.shape;
                                            });
                                            fills2 = $.map(tCardsDGE, function (el) {
                                                return el.fill;
                                            });
                                            borders2 = $.map(tCardsDGE, function (el) {
                                                return el.border;
                                            });
                                            numbers2 = $.map(tCardsDGE, function (el) {
                                                return el.number;
                                            });

                                            pCards2 = self.generer16Cartes55(colors2, shapes2, fills2, borders2, numbers2, this.cardsHS4);

                                            if(pCards2 !== undefined) {
                                                for (var w = 0; w < pCards2.length; w++) {
                                                    tCardsDGE[3] = pCards2[w][0];
                                                    carte8[w] = this.generateVirtualCard55(tCardsDGE);

                                                }
                                            }
                                        }

                                    }

                                }

                                for (var x = 0; x < carte8.length; x++) {
                                    for (a = k + 1; a < self.cardsHS4.length; a++) {

                                        if (carte8[x][0].color !== undefined && carte8[x][0].shape !== undefined && carte8[x][0].fill !== undefined && carte8[x][0].border !== undefined && carte8[x][0].number !== undefined) {
                                            if (carte8[x][0].shape === shapesall[a] && carte8[x][0].color === colorsall[a] && carte8[x][0].fill === fillsall[a] && carte8[x][0].border === bordersall[a] && carte8[x][0].number === numbersall[a]) {
                                                //console.log('ab');
                                                //console.log(carte6[x][0].shape, carte6[x][0].color, carte6[x][0].fill, carte6[x][0].number);
                                                //console.log(carte1);
                                                //console.log(carte2);
                                                self.setHS4 = self.setHS4 + 1;
                                            }
                                        }
                                    }
                                }

                                carte8 = [];
                                pCards = [];
                                pCards2 = [];
                            }
                       }
                    }
                }
            }
        }

        this.$nbSetsHS4.html(self.setHS4);
    },

/*


 existingHyperSet3: function() {
        var colors1 = [];
        var shapes1 =[];
        var fills1= [];
        var borders1= [];
        var numbers1= [];
        var colors2 = [];
        var shapes2 =[];
        var fills2= [];
        var borders2= [];
        var numbers2= [];
        var colors3 = [];
        var shapes3 =[];
        var fills3= [];
        var borders3= [];
        var numbers3= [];

        var colors4 = [];
        var shapes4 =[];
        var fills4= [];
        var borders4= [];
        var numbers4= [];
        var colors5 = [];
        var shapes5 =[];
        var fills5= [];
        var borders5= [];
        var numbers5= [];
        var colors6 = [];
        var shapes6 =[];
        var fills6= [];
        var borders6= [];
        var numbers6= [];
        var colors7 = [];
        var shapes7 =[];
        var fills7= [];
        var borders7= [];
        var numbers7= [];
        var colors8 = [];
        var shapes8 =[];
        var fills8= [];
        var borders8= [];
        var numbers8= [];



        var self = this;
        var cartes = self.cardsHS3;
        var cartes1 = cartes;
        var cartes2 = cartes;
        var cartes3 = cartes;
        var cartes4 = cartes;


        var carte1, carte3, carte4, carte5;
        var carte2;
        var card6ABC = [];
        var card6ABD = [];
        var card6ABE = [];
        var card6ACD = [];
        var card6ACE = [];
        var card6BCD = [];
        var card6BCE = [];
        var card6CDE = [];

        var cardVirtualABC = [];
        var cardVirtualABD = [];
        var cardVirtualABE = [];
        var cardVirtualACD = [];
        var cardVirtualACE = [];
        var cardVirtualBCD = [];
        var cardVirtualBCE = [];
        var cardVirtualCDE = [];

        var tCardsABC = [];
        var tCardsABD = [];
        var tCardsABE = [];
        var tCardsACD = [];
        var tCardsACE = [];
        var tCardsBCD = [];
        var tCardsBCE = [];
        var tCardsCDE = [];



        var i, j, a,l, m, k;

        var colorsall= $.map(this.cardsHS3, function(el) { return el.color;});
        var shapesall = $.map(this.cardsHS3, function(el) { return el.shape; });
        var fillsall= $.map(this.cardsHS3,function(el) { return el.fill; });
        var bordersall= $.map(this.cardsHS3,function(el) { return el.border; });
        var numbersall = $.map(this.cardsHS3, function(el) { return el.number;});


        for(i=0; i < cartes.length-4; i++) {
            for (j = i + 1; j < cartes1.length - 3; j++) {
                for (k = j + 1; k < cartes2.length-2; k++) {
                    for (l = k + 1; l < cartes3.length-1; l++) {
                        for (m = l + 1; m < cartes4.length; m++) {


                            carte1 = cartes[i];
                            carte2 = cartes1[j];
                            carte3 = cartes2[k];
                            carte4 = cartes3[l];
                            carte5 = cartes4[m];


                            tCardsABC[0] = carte1;
                            tCardsABC[1] = carte2;
                            tCardsABC[2] = carte3;


                            tCardsABD[0] = carte1;
                            tCardsABD[1] = carte2;
                            tCardsABD[2] = carte4;

                            tCardsABE[0] = carte1;
                            tCardsABE[1] = carte2;
                            tCardsABE[2] = carte5;

                            tCardsACD[0] = carte1;
                            tCardsACD[1] = carte3;
                            tCardsACD[2] = carte4;

                            tCardsACE[0] = carte1;
                            tCardsACE[1] = carte3;
                            tCardsACE[2] = carte5;

                            tCardsBCD[0] = carte2;
                            tCardsBCD[1] = carte3;
                            tCardsBCD[2] = carte4;

                            tCardsBCE[0] = carte2;
                            tCardsBCE[1] = carte3;
                            tCardsBCE[2] = carte5;

                            tCardsCDE[0] = carte3;
                            tCardsCDE[1] = carte4;
                            tCardsCDE[2] = carte5;

                            cardVirtualABC = this.generateVirtualCard44(tCardsABC);
                            cardVirtualABD = this.generateVirtualCard44(tCardsABD);
                            cardVirtualABE = this.generateVirtualCard44(tCardsABE);
                            cardVirtualACD = this.generateVirtualCard44(tCardsACD);
                            cardVirtualACE = this.generateVirtualCard44(tCardsACE);
                            cardVirtualBCD = this.generateVirtualCard44(tCardsBCD);
                            cardVirtualBCE = this.generateVirtualCard44(tCardsBCE);
                            cardVirtualCDE = this.generateVirtualCard44(tCardsCDE);


                            tCardsABC[0] = carte4;
                            tCardsABC[1] = carte5;
                            tCardsABC[2] = cardVirtualABC[0];

                            tCardsABD[0] = carte3;
                            tCardsABD[1] = carte5;
                            tCardsABD[2] = cardVirtualABD[0];

                            tCardsABE[0] = carte3;
                            tCardsABE[1] = carte4;
                            tCardsABE[2] = cardVirtualABE[0];

                            tCardsACD[0] = carte2;
                            tCardsACD[1] = carte5;
                            tCardsACD[2] = cardVirtualACD[0];

                            tCardsACE[0] = carte2;
                            tCardsACE[1] = carte4;
                            tCardsACE[2] = cardVirtualACE[0];

                            tCardsBCD[0] = carte1;
                            tCardsBCD[1] = carte5;
                            tCardsBCD[2] = cardVirtualBCD[0];

                            tCardsBCE[0] = carte1;
                            tCardsBCE[1] = carte4;
                            tCardsBCE[2] = cardVirtualBCE[0];

                            tCardsCDE[0] = carte1;
                            tCardsCDE[1] = carte2;
                            tCardsCDE[2] = cardVirtualCDE[0];

                            card6ABC = this.generateVirtualCard44(tCardsABC);
                            card6ABD = this.generateVirtualCard44(tCardsABD);
                            card6ABE = this.generateVirtualCard44(tCardsABE);
                            card6ACD = this.generateVirtualCard44(tCardsACD);
                            card6ACE = this.generateVirtualCard44(tCardsACE);
                            card6BCD = this.generateVirtualCard44(tCardsBCD);
                            card6BCE = this.generateVirtualCard44(tCardsBCE);
                            card6CDE = this.generateVirtualCard44(tCardsCDE);

                            colors1.splice(0, 1, carte4.color);
                            colors1.splice(1, 1, carte5.color);
                            colors1.splice(2, 1, card6ABC[0].color);
                            colors1.splice(3, 1, cardVirtualABC[0].color);

                            shapes1.splice(0, 1, carte4.shape);
                            shapes1.splice(1, 1, carte5.shape);
                            shapes1.splice(2, 1, card6ABC[0].shape);
                            shapes1.splice(3, 1, cardVirtualABC[0].shape);

                            fills1.splice(0, 1, carte4.fill);
                            fills1.splice(1, 1, carte5.fill);
                            fills1.splice(2, 1, card6ABC[0].fill);
                            fills1.splice(3, 1, cardVirtualABC[0].fill);

                            borders1.splice(0, 1, carte4.border);
                            borders1.splice(1, 1, carte5.border);
                            borders1.splice(2, 1, card6ABC[0].border);
                            borders1.splice(3, 1, cardVirtualABC[0].border);

                            numbers1.splice(0, 1, carte4.number);
                            numbers1.splice(1, 1, carte5.number);
                            numbers1.splice(2, 1, card6ABC[0].number);
                            numbers1.splice(3, 1, cardVirtualABC[0].number);


                            colors2.splice(0, 1, carte3.color);
                            colors1.splice(1, 1, carte5.color);
                            colors2.splice(2, 1, card6ABD[0].color);
                            colors2.splice(3, 1, cardVirtualABD[0].color);

                            shapes2.splice(0, 1, carte3.shape);
                            shapes1.splice(1, 1, carte5.shape);
                            shapes2.splice(2, 1, card6ABD[0].shape);
                            shapes2.splice(3, 1, cardVirtualABD[0].shape);

                            fills2.splice(0, 1, carte3.fill);
                            fills1.splice(1, 1, carte5.fill);
                            fills2.splice(2, 1, card6ABD[0].fill);
                            fills2.splice(3, 1, cardVirtualABD[0].fill);

                            borders2.splice(0, 1, carte3.border);
                            borders2.splice(1, 1, carte5.border);
                            borders2.splice(2, 1, card6ABD[0].border);
                            borders2.splice(3, 1, cardVirtualABD[0].border);

                            numbers2.splice(0, 1, carte3.number);
                            numbers1.splice(1, 1, carte5.number);
                            numbers2.splice(2, 1, card6ABD[0].number);
                            numbers2.splice(3, 1, cardVirtualABD[0].number);


                            colors3.splice(0, 1, carte3.color);
                            colors3.splice(1, 1, carte4.color);
                            colors3.splice(2, 1, card6ABE[0].color);
                            colors3.splice(3, 1, cardVirtualABE[0].color);

                            shapes3.splice(0, 1, carte3.shape);
                            shapes3.splice(1, 1, carte4.shape);
                            shapes3.splice(2, 1, card6ABE[0].shape);
                            shapes3.splice(3, 1, cardVirtualABE[0].shape);

                            fills3.splice(0, 1, carte3.fill);
                            fills3.splice(1, 1, carte4.fill);
                            fills3.splice(2, 1, card6ABE[0].fill);
                            fills3.splice(3, 1, cardVirtualABE[0].fill);

                            borders3.splice(0, 1, carte3.border);
                            borders3.splice(1, 1, carte4.border);
                            borders3.splice(2, 1, card6ABE[0].border);
                            borders3.splice(3, 1, cardVirtualABE[0].border);

                            numbers3.splice(0, 1, carte3.number);
                            numbers3.splice(1, 1, carte4.number);
                            numbers3.splice(2, 1, card6ABE[0].number);
                            numbers3.splice(3, 1, cardVirtualABE[0].number);




                            colors4.splice(0, 1, carte2.color);
                            colors4.splice(1, 1, carte5.color);
                            colors4.splice(2, 1, card6ACD[0].color);
                            colors4.splice(3, 1, cardVirtualACD[0].color);

                            shapes4.splice(0, 1, carte2.shape);
                            shapes4.splice(1, 1, carte5.shape);
                            shapes4.splice(2, 1, card6ACD[0].shape);
                            shapes4.splice(3, 1, cardVirtualACD[0].shape);

                            fills4.splice(0, 1, carte2.fill);
                            fills4.splice(1, 1, carte5.fill);
                            fills4.splice(2, 1, card6ACD[0].fill);
                            fills4.splice(3, 1, cardVirtualACD[0].fill);

                            borders4.splice(0, 1, carte2.border);
                            borders4.splice(1, 1, carte5.border);
                            borders4.splice(2, 1, card6ACD[0].border);
                            borders4.splice(3, 1, cardVirtualACD[0].border);

                            numbers4.splice(0, 1, carte2.number);
                            numbers4.splice(1, 1, carte5.number);
                            numbers4.splice(2, 1, card6ACD[0].number);
                            numbers4.splice(3, 1, cardVirtualACD[0].number);


                            colors5.splice(0, 1, carte2.color);
                            colors5.splice(1, 1, carte4.color);
                            colors5.splice(2, 1, card6ACE[0].color);
                            colors5.splice(3, 1, cardVirtualACE[0].color);

                            shapes5.splice(0, 1, carte2.shape);
                            shapes5.splice(1, 1, carte4.shape);
                            shapes5.splice(2, 1, card6ACE[0].shape);
                            shapes5.splice(3, 1, cardVirtualACE[0].shape);

                            fills5.splice(0, 1, carte2.fill);
                            fills5.splice(1, 1, carte4.fill);
                            fills5.splice(2, 1, card6ACE[0].fill);
                            fills5.splice(3, 1, cardVirtualACE[0].fill);

                            borders5.splice(0, 1, carte2.border);
                            borders5.splice(1, 1, carte4.border);
                            borders5.splice(2, 1, card6ACE[0].border);
                            borders5.splice(3, 1, cardVirtualACE[0].border);

                            numbers5.splice(0, 1, carte2.number);
                            numbers5.splice(1, 1, carte4.number);
                            numbers5.splice(2, 1, card6ACE[0].number);
                            numbers5.splice(3, 1, cardVirtualACE[0].number);


                            colors6.splice(0, 1, carte1.color);
                            colors6.splice(1, 1, carte5.color);
                            colors6.splice(2, 1, card6BCD[0].color);
                            colors6.splice(3, 1, cardVirtualBCD[0].color);

                            shapes6.splice(0, 1, carte1.shape);
                            shapes6.splice(1, 1, carte5.shape);
                            shapes6.splice(2, 1, card6BCD[0].shape);
                            shapes6.splice(3, 1, cardVirtualBCD[0].shape);

                            fills6.splice(0, 1, carte1.fill);
                            fills6.splice(1, 1, carte5.fill);
                            fills6.splice(2, 1, card6BCD[0].fill);
                            fills6.splice(3, 1, cardVirtualBCD[0].fill);

                            borders6.splice(0, 1, carte1.border);
                            borders6.splice(1, 1, carte5.border);
                            borders6.splice(2, 1, card6BCD[0].border);
                            borders6.splice(3, 1, cardVirtualBCD[0].border);

                            numbers6.splice(0, 1, carte1.number);
                            numbers6.splice(1, 1, carte5.number);
                            numbers6.splice(2, 1, card6BCD[0].number);
                            numbers6.splice(3, 1, cardVirtualBCD[0].number);



                            colors7.splice(0, 1, carte1.color);
                            colors7.splice(1, 1, carte4.color);
                            colors7.splice(2, 1, card6BCE[0].color);
                            colors7.splice(3, 1, cardVirtualBCE[0].color);

                            shapes7.splice(0, 1, carte1.shape);
                            shapes7.splice(1, 1, carte4.shape);
                            shapes7.splice(2, 1, card6BCE[0].shape);
                            shapes7.splice(3, 1, cardVirtualBCE[0].shape);

                            fills7.splice(0, 1, carte1.fill);
                            fills7.splice(1, 1, carte4.fill);
                            fills7.splice(2, 1, card6BCE[0].fill);
                            fills7.splice(3, 1, cardVirtualBCE[0].fill);

                            borders7.splice(0, 1, carte1.border);
                            borders7.splice(1, 1, carte4.border);
                            borders7.splice(2, 1, card6BCE[0].border);
                            borders7.splice(3, 1, cardVirtualBCE[0].border);

                            numbers7.splice(0, 1, carte1.number);
                            numbers7.splice(1, 1, carte4.number);
                            numbers7.splice(2, 1, card6BCE[0].number);
                            numbers7.splice(3, 1, cardVirtualBCE[0].number);



                            colors8.splice(0, 1, carte1.color);
                            colors8.splice(1, 1, carte2.color);
                            colors8.splice(2, 1, card6CDE[0].color);
                            colors8.splice(3, 1, cardVirtualCDE[0].color);

                            shapes8.splice(0, 1, carte1.shape);
                            shapes8.splice(1, 1, carte2.shape);
                            shapes8.splice(2, 1, card6CDE[0].shape);
                            shapes8.splice(3, 1, cardVirtualCDE[0].shape);

                            fills8.splice(0, 1, carte1.fill);
                            fills8.splice(1, 1, carte2.fill);
                            fills8.splice(2, 1, card6CDE[0].fill);
                            fills8.splice(3, 1, cardVirtualCDE[0].fill);

                            borders8.splice(0, 1, carte1.border);
                            borders8.splice(1, 1, carte2.border);
                            borders8.splice(2, 1, card6CDE[0].border);
                            borders8.splice(3, 1, cardVirtualCDE[0].border);

                            numbers8.splice(0, 1, carte1.number);
                            numbers8.splice(1, 1, carte2.number);
                            numbers8.splice(2, 1, card6CDE[0].number);
                            numbers8.splice(3, 1, cardVirtualCDE[0].number);

                            //console.log('Card4 : ' + shapes1[0] + colors1[0] +fills1[0]+ numbers1[0] );

                            //if(this.cardInListOfCards44(shapes1[3],colors1[3],fills1[3],numbers1[3] )){}

                            for (a = k + 1; a < self.cardsHS3.length; a++) {
                                if (shapes1[2] !== undefined && colors1[2] !== undefined && fills1[2] !== undefined && borders1[2] !== undefined  && numbers1[2] !== undefined) {
                                    if (shapes1[3] !== undefined && colors1[3] !== undefined && fills1[3] !== undefined  && borders1[3] !== undefined && numbers1[3] !== undefined) {
                                        if (shapes1[2] === shapesall[a] && colors1[2] === colorsall[a] && fills1[2] === fillsall[a] && borders1[2] === bordersall[a] && numbers1[2] === numbersall[a]) {
                                            //console.log('ab');
                                            //console.log(shapes1, colors1, fills1, borders1, numbers1);
                                            //console.log( 'carte1 :'+  carte1);
                                            //console.log( carte2 : ' + carte2);
                                            self.setHS3 = self.setHS3 + 1;
                                        }
                                    }
                                }
                            }


                            for (a = k + 1; a < self.cardsHS3.length; a++) {
                                if (shapes2[2] !== undefined && colors2[2] !== undefined && fills2[2] !== undefined && borders2[2] !== undefined && numbers2[2] !== undefined) {
                                    if (shapes2[3] !== undefined && colors2[3] !== undefined && fills2[3] !== undefined  && borders2[3] !== undefined && numbers2[3] !== undefined) {
                                        if (shapes2[2] === shapesall[a] && colors2[2] === colorsall[a] && fills2[2] === fillsall[a] && borders2[2] === bordersall[a] && numbers2[2] === numbersall[a]) {
                                            //console.log('ac');
                                            //console.log(shapes2, colors2, fills2, borders2, numbers2);
                                            self.setHS3 = self.setHS3 + 1;
                                        }
                                    }
                                }
                            }


                            for (a = k + 1; a < self.cardsHS3.length; a++) {
                                if (shapes3[2] !== undefined && colors3[2] !== undefined && fills3[2] !== undefined && borders3[2] !== undefined && numbers3[2] !== undefined) {
                                    if (shapes3[3] !== undefined && colors3[3] !== undefined && fills3[3] !== undefined && borders3[3] !== undefined && numbers3[3] !== undefined) {
                                        if (shapes3[2] === shapesall[a] && colors3[2] === colorsall[a] && fills3[2] === fillsall[a] && borders3[2] === bordersall[a] && numbers3[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS3 = self.setHS3 + 1;
                                        }
                                    }
                                }
                            }



                            for (a = k + 1; a < self.cardsHS3.length; a++) {
                                if (shapes4[2] !== undefined && colors4[2] !== undefined && fills4[2] !== undefined  && borders4[2] !== undefined  &&  numbers4[2] !== undefined) {
                                    if (shapes4[3] !== undefined && colors4[3] !== undefined && fills4[3] !== undefined && borders4[3] !== undefined  && numbers4[3] !== undefined) {
                                        if (shapes4[2] === shapesall[a] && colors4[2] === colorsall[a] && fills4[2] === fillsall[a] && borders4[2] === bordersall[a] && numbers4[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS3 = self.setHS3 + 1;
                                        }
                                    }
                                }
                            }
                            for (a = k + 1; a < self.cardsHS3.length; a++) {
                                if (shapes5[2] !== undefined && colors5[2] !== undefined && fills5[2] !== undefined && borders5[2] !== undefined  && numbers5[2] !== undefined) {
                                    if (shapes5[3] !== undefined && colors5[3] !== undefined && fills5[3] !== undefined && borders5[3] !== undefined  && numbers5[3] !== undefined) {
                                        if (shapes5[2] === shapesall[a] && colors5[2] === colorsall[a] && fills5[2] === fillsall[a] && borders5[2] === bordersall[a] && numbers5[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS3 = self.setHS3 + 1;
                                        }
                                    }
                                }
                            }
                            for (a = k + 1; a < self.cardsHS3.length; a++) {
                                if (shapes6[2] !== undefined && colors6[2] !== undefined && fills6[2] !== undefined && borders6[2] !== undefined  && numbers6[2] !== undefined) {
                                    if (shapes6[3] !== undefined && colors6[3] !== undefined && fills6[3] !== undefined && borders6[3] !== undefined  && numbers6[3] !== undefined) {
                                        if (shapes6[2] === shapesall[a] && colors6[2] === colorsall[a] && fills6[2] === fillsall[a] && borders6[2] === bordersall[a] && numbers6[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS3 = self.setHS3 + 1;
                                        }
                                    }
                                }
                            }

                            for (a = k + 1; a < self.cardsHS3.length; a++) {
                                if (shapes7[2] !== undefined && colors7[2] !== undefined && fills7[2] !== undefined && borders7[2] !== undefined  && numbers7[2] !== undefined) {
                                    if (shapes7[3] !== undefined && colors7[3] !== undefined && fills7[3] !== undefined && borders7[3] !== undefined  && numbers7[3] !== undefined) {
                                        if (shapes7[2] === shapesall[a] && colors7[2] === colorsall[a] && fills7[2] === fillsall[a]&& borders7[2] === bordersall[a] && numbers7[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS3 = self.setHS3 + 1;
                                        }
                                    }
                                }
                            }


                            for (a = k + 1; a < self.cardsHS3.length; a++) {
                                if (shapes8[2] !== undefined && colors8[2] !== undefined && fills8[2] !== undefined  && borders8[2] !== undefined  && numbers8[2] !== undefined) {
                                    if (shapes8[3] !== undefined && colors8[3] !== undefined && fills8[3] !== undefined && borders8[3] !== undefined  && numbers8[3] !== undefined) {
                                        if (shapes8[2] === shapesall[a] && colors8[2] === colorsall[a] && fills8[2] === fillsall[a] && borders8[2] === bordersall[a] && numbers8[2] === numbersall[a]) {
                                            //console.log('bc');
                                            //console.log(shapes3, colors3, fills3, borders3, numbers3);
                                            self.setHS3 = self.setHS3 + 1;
                                        }
                                    }
                                }
                            }


                        }
                    }
                }
            }
        }

        this.$nbSetsHS3.html(self.setHS3);
    },



*/



	setCardListeners0: function() {
		var self = this;

		// what happens when a card is clicked:
		this.$board0.on('click', '.card', function(e) {
			e.stopImmediatePropagation();
			var card = e.currentTarget;

			// if card is new, add it, otherwise remove it
			var ids = $.map(self.selected0, function(el) { return $(el).data("id");});
			if (ids.indexOf($(card).data('id')) >= 0) {
				self.deselectCard(self.selected0,card);
			} else {
				self.selectCard3(self.selected0,card);
			}

			if (self.selected0.length === 3) {
				self.silentSubmission0();
			}
		});
	},

	setCardListeners: function() {
		var self = this;

		// what happens when a card is clicked:
		this.$board.on('click', '.card', function(e) {
			e.stopImmediatePropagation();
			var card = e.currentTarget;

			// if card is new, add it, otherwise remove it
			var ids = $.map(self.selected, function(el) { return $(el).data("id");});
			if (ids.indexOf($(card).data('id')) >= 0) {
				self.deselectCard(self.selected,card);
			} else {
				self.selectCard3(self.selected,card);
			}

			if (self.selected.length === 3) {
				self.silentSubmission();
			}
		});
	},

	setCardListeners1: function() {
		var self = this;

		// what happens when a card is clicked:
		this.$board1.on('click', '.card', function(e) {
			e.stopImmediatePropagation();
			var card = e.currentTarget;

			// if card is new, add it, otherwise remove it
			var ids = $.map(self.selected1, function(el) { return $(el).data("id");});
			if (ids.indexOf($(card).data('id')) >= 0) {
				self.deselectCard(self.selected1,card);
			} else {
				self.selectCard3(self.selected1,card);
			}

			if (self.selected1.length === 3) {
				self.silentSubmission1();
			}
		});
	},
	setCardListeners2: function() {
		var self = this;

		// what happens when a card is clicked:
		this.$board2.on('click', '.card', function(e) {
			e.stopImmediatePropagation();
			var card = e.currentTarget;

			// if card is new, add it, otherwise remove it
			var ids = $.map(self.selected2, function(el) { return $(el).data("id");});
			if (ids.indexOf($(card).data('id')) >= 0) {
				self.deselectCard(self.selected2,card);
			} else {
				self.selectCard4(self.selected2,card);
			}

			if (self.selected2.length === 4) {
				self.silentSubmission2();
			}
		});
	},
	setCardListeners3: function() {
		var self = this;

		// what happens when a card is clicked:
		this.$board3.on('click', '.card', function(e) {
			e.stopImmediatePropagation();
			var card = e.currentTarget;

			// if card is new, add it, otherwise remove it
			var ids = $.map(self.selected3, function(el) { return $(el).data("id");});
			if (ids.indexOf($(card).data('id')) >= 0) {
				self.deselectCard(self.selected3,card);
			} else {
				self.selectCard4(self.selected3,card);
			}

			if (self.selected3.length === 4) {
				self.silentSubmission3();
			}
		});
	},
	setCardListeners4: function() {
		var self = this;

		// what happens when a card is clicked:
		this.$board4.on('click', '.card', function(e) {
			e.stopImmediatePropagation();
			var card = e.currentTarget;

			// if card is new, add it, otherwise remove it
			var ids = $.map(self.selected4, function(el) { return $(el).data("id");});
			if (ids.indexOf($(card).data('id')) >= 0) {
				self.deselectCard(self.selected4,card);
			} else {
				self.selectCard5(self.selected4,card);
			}

			if (self.selected4.length === 5) {
				self.silentSubmission4();
			}
		});
	},

	setCardListenersHS: function() {
		var self = this;

		// what happens when a card is clicked:
		this.$boardHS.on('click', '.card', function(e) {
			e.stopImmediatePropagation();
			var card = e.currentTarget;

			// if card is new, add it, otherwise remove it
			var ids = $.map(self.selectedHS, function(el) { return $(el).data("id");});
			if (ids.indexOf($(card).data('id')) >= 0) {
				self.deselectCard(self.selectedHS,card);
			} else {
				self.selectCard4(self.selectedHS,card);
			}

			if (self.selectedHS.length === 4) {
				self.silentSubmissionHS();
			}
		});
	},

	setCardListenersHS0: function() {
		var self = this;

		// what happens when a card is clicked:
		this.$boardHS0.on('click', '.card', function(e) {
			e.stopImmediatePropagation();
			var card = e.currentTarget;

			// if card is new, add it, otherwise remove it
			var ids = $.map(self.selectedHS0, function(el) { return $(el).data("id");});
			if (ids.indexOf($(card).data('id')) >= 0) {
				self.deselectCard(self.selectedHS0,card);
			} else {
				self.selectCard4(self.selectedHS0,card);
			}

			if (self.selectedHS0.length === 4) {
				self.silentSubmissionHS0();
			}
		});
	},


	setCardListenersHS1: function() {
		var self = this;

		// what happens when a card is clicked:
		this.$boardHS1.on('click', '.card', function(e) {
			e.stopImmediatePropagation();
			var card = e.currentTarget;

			// if card is new, add it, otherwise remove it
			var ids = $.map(self.selectedHS1, function(el) { return $(el).data("id");});
			if (ids.indexOf($(card).data('id')) >= 0) {
				self.deselectCard(self.selectedHS1,card);
			} else {
				self.selectCard4(self.selectedHS1,card);
			}

			if (self.selectedHS1.length === 4) {
				self.silentSubmissionHS1();
			}
		});
	},
	setCardListenersHS2: function() {
		var self = this;

		// what happens when a card is clicked:
		this.$boardHS2.on('click', '.card', function(e) {
			e.stopImmediatePropagation();
			var card = e.currentTarget;

			// if card is new, add it, otherwise remove it
			var ids = $.map(self.selectedHS2, function(el) { return $(el).data("id");});
			if (ids.indexOf($(card).data('id')) >= 0) {
				self.deselectCard(self.selectedHS2,card);
			} else {
				self.selectCard6(self.selectedHS2,card);
			}

			if (self.selectedHS2.length === 6) {
				self.silentSubmissionHS2();
			}
		});

	},


    setCardListenersHS3: function() {
        var self = this;

        // what happens when a card is clicked:
        this.$boardHS3.on('click', '.card', function(e) {
            e.stopImmediatePropagation();
            var card = e.currentTarget;

            // if card is new, add it, otherwise remove it
            var ids = $.map(self.selectedHS3, function(el) { return $(el).data("id");});
            if (ids.indexOf($(card).data('id')) >= 0) {
                self.deselectCard(self.selectedHS3,card);
            } else {
                self.selectCard6(self.selectedHS3,card);
            }

            if (self.selectedHS3.length === 6) {
                self.silentSubmissionHS3();
            }
        });
    },
    setCardListenersHS4: function() {
        var self = this;

        // what happens when a card is clicked:
        this.$boardHS4.on('click', '.card', function(e) {
            e.stopImmediatePropagation();
            var card = e.currentTarget;

            // if card is new, add it, otherwise remove it
            var ids = $.map(self.selectedHS4, function(el) { return $(el).data("id");});
            if (ids.indexOf($(card).data('id')) >= 0) {
                self.deselectCard(self.selectedHS4,card);
            } else {
                self.selectCard8(self.selectedHS4,card);
            }

            if (self.selectedHS4.length === 8) {
                self.silentSubmissionHS4();
            }
        });
    },





	selectCard3: function(selectedCard, card) {
		if (selectedCard.length < 3) {
			$(card).addClass('selected');
			selectedCard.push(card);

		}

	},

	selectCard4: function(selectedCard, card) {
		if (selectedCard.length < 4) {
			$(card).addClass('selected');
			selectedCard.push(card);

		}

	},
	selectCard5: function(selectedCard, card) {
		if (selectedCard.length < 5) {
			$(card).addClass('selected');
			selectedCard.push(card);

		}

	},
	selectCard6: function(selectedCard, card) {
		if (selectedCard.length < 6) {
			$(card).addClass('selected');
			selectedCard.push(card);

		}

	},
    selectCard8: function(selectedCard, card) {
        if (selectedCard.length < 8) {
            $(card).addClass('selected');
            selectedCard.push(card);

        }

    },


	deselectCard: function(selectedCards,card) {

		var index = selectedCards.indexOf(card);
		if (index > -1) {
			selectedCards.splice(index, 1);
		}
		$(card).removeClass('selected');
	},


	clearSelections: function(selectedCards) {
		$.each(selectedCards, function(index, card) {
			$(card).removeClass('selected');
		});
        selectedCards = [];
	},














	generateVirtualCard34: function(cards1){

		var colors2= $.map(cards1, function(el) { return $(el).data("color");});
		var shapes2 = $.map(cards1, function(el) { return $(el).data("shape");});
		var fills2 = $.map(cards1, function(el) { return $(el).data("fill");});
		var numbers2 = $.map(cards1, function(el) { return $(el).data("number");});

		if (shapes2[0] === undefined  && colors2[0] === undefined && fills2[0] === undefined && numbers2[0] === undefined) {
			colors2= $.map(cards1, function(el) { return el.color;});
			shapes2 = $.map(cards1, function(el) { return el.shape;});
			fills2 = $.map(cards1, function(el) { return el.fill;});
			numbers2 = $.map(cards1, function(el) { return el.number;});
		}


		if(colors2[0] === colors2 [1]){
			colors2.push(colors2[0]);
		}
		else{
			if(colors2[0] === 'red' && colors2[1] === 'green'){
				colors2.push('purple');
			}

			if(colors2[0] === 'red' && colors2[1] === 'purple'){
				colors2.push('green');
			}

			if(colors2[0] === 'green' && colors2[1] === 'purple'){
				colors2.push('red');
			}
			if(colors2[0] === 'green' && colors2[1] === 'red'){
				colors2.push('purple');
			}

			if(colors2[0] === 'purple' && colors2[1] === 'red'){
				colors2.push('green');
			}
			if(colors2[0] === 'purple' && colors2[1] === 'green'){
				colors2.push('red');
			}

		}
		if(shapes2[0] === shapes2 [1]){
			shapes2.push(shapes2[0]);
		}
		else{
			if(shapes2[0] === 'oval' && shapes2[1] === 'diamond'){
				shapes2.push('wave');
			}

			if(shapes2[0] === 'oval' && shapes2[1] === 'wave'){
				shapes2.push('diamond');
			}

			if(shapes2[0] === 'diamond' && shapes2[1] === 'oval'){
				shapes2.push('wave');

			}
			if(shapes2[0] === 'diamond' && shapes2[1] === 'wave'){
				shapes2.push('oval');
			}

			if(shapes2[0] === 'wave' && shapes2[1] === 'diamond'){
				shapes2.push('oval');
			}
			if(shapes2[0] === 'wave' && shapes2[1] === 'oval'){
				shapes2.push('diamond');
			}
		}
		if(fills2[0] === fills2 [1]){
			fills2.push(fills2[0]);
		}
		else{
			if(fills2[0] === 'solid' && fills2[1] === 'stripped'){
				fills2.push('open');
			}

			if(fills2[0] === 'solid' && fills2[1] === 'open'){
				fills2.push('stripped');
			}

			if(fills2[0] === 'stripped' && fills2[1] === 'open'){
				fills2.push('solid');
			}
			if(fills2[0] === 'open' && fills2[1] === 'stripped'){
				fills2.push('solid');
			}

			if(fills2[0] === 'stripped' && fills2[1] === 'solid'){
				fills2.push('open');
			}
			if(fills2[0] === 'open' && fills2[1] === 'solid'){
				fills2.push('stripped');
			}
		}
		if(numbers2[0] === numbers2[1]){
			numbers2.push(numbers2[0]);
		}
		else{
			if(numbers2[0] === 1 && numbers2[1] === 2){
				numbers2.push(3);
			}

			if(numbers2[0] === 1 && numbers2[1] === 3){
				numbers2.push(2);
			}

			if(numbers2[0] === 2 && numbers2[1] === 1){
				numbers2.push(3);
			}
			if(numbers2[0] === 2 && numbers2[1] === 3){
				numbers2.push(1);
			}

			if(numbers2[0] === 3 && numbers2[1] === 1){
				numbers2.push(2);
			}
			if(numbers2[0] === 3 && numbers2[1] === 2){
				numbers2.push(1);
			}

		}

        var cards4 = [];

        cards4.push({
            shape: shapes2[2],
            fill: fills2[2],
            color: colors2[2],
            number: numbers2[2]
        });

        return cards4;
	},


	generateVirtualCard33: function(cards1){
		var colors2= $.map(cards1, function(el) { return $(el).data("color");});
		var shapes2 = $.map(cards1, function(el) { return $(el).data("shape");});
		var numbers2 = $.map(cards1, function(el) { return $(el).data("number");});

		if (shapes2[0] === undefined  && colors2[0] === undefined  && numbers2[0] === undefined) {
			colors2= $.map(cards1, function(el) { return el.color;});
			shapes2 = $.map(cards1, function(el) { return el.shape;});
			numbers2 = $.map(cards1, function(el) { return el.number;});
		}


		if(colors2[0] === colors2 [1]){
			colors2.push(colors2[0]);
		}
		else{
			if(colors2[0] === 'red' && colors2[1] === 'green'){
				colors2.push('purple');
			}

			if(colors2[0] === 'red' && colors2[1] === 'purple'){
				colors2.push('green');
			}

			if(colors2[0] === 'green' && colors2[1] === 'purple'){
				colors2.push('red');
			}
			if(colors2[0] === 'green' && colors2[1] === 'red'){
				colors2.push('purple');
			}

			if(colors2[0] === 'purple' && colors2[1] === 'red'){
				colors2.push('green');
			}
			if(colors2[0] === 'purple' && colors2[1] === 'green'){
				colors2.push('red');
			}

		}
		if(shapes2[0] === shapes2 [1]){
			shapes2.push(shapes2[0]);
		}
		else{
			if(shapes2[0] === 'oval' && shapes2[1] === 'diamond'){
				shapes2.push('wave');
			}

			if(shapes2[0] === 'oval' && shapes2[1] === 'wave'){
				shapes2.push('diamond');
			}

			if(shapes2[0] === 'diamond' && shapes2[1] === 'oval'){
				shapes2.push('wave');

			}
			if(shapes2[0] === 'diamond' && shapes2[1] === 'wave'){
				shapes2.push('oval');
			}

			if(shapes2[0] === 'wave' && shapes2[1] === 'diamond'){
				shapes2.push('oval');
			}
			if(shapes2[0] === 'wave' && shapes2[1] === 'oval'){
				shapes2.push('diamond');
			}
		}

		if(numbers2[0] === numbers2[1]){
			numbers2.push(numbers2[0]);
		}
		else{
			if(numbers2[0] === 1 && numbers2[1] === 2){
				numbers2.push(3);
			}

			if(numbers2[0] === 1 && numbers2[1] === 3){
				numbers2.push(2);
			}

			if(numbers2[0] === 2 && numbers2[1] === 1){
				numbers2.push(3);
			}
			if(numbers2[0] === 2 && numbers2[1] === 3){
				numbers2.push(1);
			}

			if(numbers2[0] === 3 && numbers2[1] === 1){
				numbers2.push(2);
			}
			if(numbers2[0] === 3 && numbers2[1] === 2){
				numbers2.push(1);
			}

		}

        var cards4 = [];

        cards4.push({
            shape: shapes2[2],
            color: colors2[2],
            number: numbers2[2]
        });

        return cards4;
	},

	generateVirtualCard35: function(cards1){

		// modalité des combinaison de carte (ab ac ad ...) pour verifier si forme un set
		var colors2= $.map(cards1, function(el) { return $(el).data("color");});
		var shapes2 = $.map(cards1, function(el) { return $(el).data("shape");});
		var fills2 = $.map(cards1, function(el) { return $(el).data("fill");});
		var borders2 = $.map(cards1, function(el) { return $(el).data("border");});
		var numbers2 = $.map(cards1, function(el) { return $(el).data("number");});

		if (shapes2[0] === undefined  && colors2[0] === undefined && fills2[0] === undefined && numbers2[0] === undefined && borders2[0] === undefined) {
			colors2= $.map(cards1, function(el) { return el.color;});
			shapes2 = $.map(cards1, function(el) { return el.shape;});
			fills2 = $.map(cards1, function(el) { return el.fill;});
			numbers2 = $.map(cards1, function(el) { return el.number;});
			borders2 = $.map(cards1, function(el) { return el.border;});

		}



		if(colors2[0] === colors2 [1]){
			colors2.push(colors2[0]);
		}
		else{
			if(colors2[0] === 'red' && colors2[1] === 'green'){
				colors2.push('purple');
			}

			if(colors2[0] === 'red' && colors2[1] === 'purple'){
				colors2.push('green');
			}

			if(colors2[0] === 'green' && colors2[1] === 'purple'){
				colors2.push('red');
			}
			if(colors2[0] === 'green' && colors2[1] === 'red'){
				colors2.push('purple');
			}

			if(colors2[0] === 'purple' && colors2[1] === 'red'){
				colors2.push('green');
			}
			if(colors2[0] === 'purple' && colors2[1] === 'green'){
				colors2.push('red');
			}

		}
		if(shapes2[0] === shapes2 [1]){
			shapes2.push(shapes2[0]);
		}
		else{
			if(shapes2[0] === 'oval' && shapes2[1] === 'diamond'){
				shapes2.push('wave');
			}

			if(shapes2[0] === 'oval' && shapes2[1] === 'wave'){
				shapes2.push('diamond');
			}

			if(shapes2[0] === 'diamond' && shapes2[1] === 'oval'){
				shapes2.push('wave');

			}
			if(shapes2[0] === 'diamond' && shapes2[1] === 'wave'){
				shapes2.push('oval');
			}

			if(shapes2[0] === 'wave' && shapes2[1] === 'diamond'){
				shapes2.push('oval');
			}
			if(shapes2[0] === 'wave' && shapes2[1] === 'oval'){
				shapes2.push('diamond');
			}
		}
		if(fills2[0] === fills2 [1]){
			fills2.push(fills2[0]);
		}
		else{
			if(fills2[0] === 'solid' && fills2[1] === 'stripped'){
				fills2.push('open');
			}

			if(fills2[0] === 'solid' && fills2[1] === 'open'){
				fills2.push('stripped');
			}

			if(fills2[0] === 'stripped' && fills2[1] === 'open'){
				fills2.push('solid');
			}
			if(fills2[0] === 'open' && fills2[1] === 'stripped'){
				fills2.push('solid');
			}

			if(fills2[0] === 'stripped' && fills2[1] === 'solid'){
				fills2.push('open');
			}
			if(fills2[0] === 'open' && fills2[1] === 'solid'){
				fills2.push('stripped');
			}
		}

		if(borders2[0] === borders2 [1]){
			borders2.push(borders2[0]);
		}
		else{
			if(borders2[0] === 'plein' && borders2[1] === 'point'){
				borders2.push('rond');
			}

			if(borders2[0] === 'point' && borders2[1] === 'plein'){
				borders2.push('rond');
			}

			if(borders2[0] === 'rond' && borders2[1] === 'plein'){
				borders2.push('point');
			}
			if(borders2[0] === 'plein' && borders2[1] === 'rond'){
				borders2.push('point');
			}

			if(borders2[0] === 'rond' && borders2[1] === 'point'){
				borders2.push('plein');
			}
			if(borders2[0] === 'point' && borders2[1] === 'rond'){
				borders2.push('plein');
			}
		}

		if(numbers2[0] === numbers2[1]){
			numbers2.push(numbers2[0]);
		}
		else{
			if(numbers2[0] === 1 && numbers2[1] === 2){
				numbers2.push(3);
			}

			if(numbers2[0] === 1 && numbers2[1] === 3){
				numbers2.push(2);
			}

			if(numbers2[0] === 2 && numbers2[1] === 1){
				numbers2.push(3);
			}
			if(numbers2[0] === 2 && numbers2[1] === 3){
				numbers2.push(1);
			}

			if(numbers2[0] === 3 && numbers2[1] === 1){
				numbers2.push(2);
			}
			if(numbers2[0] === 3 && numbers2[1] === 2){
				numbers2.push(1);
			}

		}
        var cards4 = [];

        cards4.push({
            shape: shapes2[2],
            fill: fills2[2],
            color: colors2[2],
			border: borders2[2],
            number: numbers2[2]
        });

        return cards4;
	},


	generateVirtualCard44: function(cards1){

        var colors2= $.map(cards1, function(el) { return $(el).data("color");});
        var shapes2 = $.map(cards1, function(el) { return $(el).data("shape");});
        var fills2 = $.map(cards1, function(el) { return $(el).data("fill");});
        var numbers2 = $.map(cards1, function(el) { return $(el).data("number");});

		if (shapes2[0] === undefined  && colors2[0] === undefined && fills2[0] === undefined && numbers2[0] === undefined) {
			colors2= $.map(cards1, function(el) { return el.color;});
			shapes2 = $.map(cards1, function(el) { return el.shape;});
			fills2 = $.map(cards1, function(el) { return el.fill;});
			numbers2 = $.map(cards1, function(el) { return el.number;});
		}


		if (colors2[0] === colors2[1] && colors2[0] === colors2[2]) {
            colors2.push(colors2[0]);

        } else {
            if (colors2[0] === 'red' && colors2[1] === 'green' && colors2[2] === 'purple') {
                colors2.push('lightblue');
            }

            if (colors2[0] === 'red' && colors2[1] === 'purple' && colors2[2] === 'green') {
                colors2.push('lightblue');
            }

            if (colors2[0] === 'purple' && colors2[1] === 'red' && colors2[2] === 'green') {
                colors2.push('lightblue');
            }
            if (colors2[0] === 'purple' && colors2[1] === 'green' && colors2[2] === 'red') {
                colors2.push('lightblue');
            }

            if (colors2[0] === 'green' && colors2[1] === 'purple' && colors2[2] === 'red') {
                colors2.push('lightblue');
            }
            if (colors2[0] === 'green' && colors2[1] === 'red' && colors2[2] === 'purple') {
                colors2.push('lightblue');
            }


            if (colors2[0] === 'green' && colors2[1] === 'purple' && colors2[2] === 'lightblue') {
                colors2.push('red');

            }

            if (colors2[0] === 'green' && colors2[1] === 'lightblue' && colors2[2] === 'purple') {
                colors2.push('red');
            }

            if (colors2[0] === 'purple' && colors2[1] === 'lightblue' && colors2[2] === 'green') {
                colors2.push('red');
            }
            if (colors2[0] === 'purple' && colors2[1] === 'green' && colors2[2] === 'lightblue') {
                colors2.push('red');
            }

            if (colors2[0] === 'lightblue' && colors2[1] === 'purple' && colors2[2] === 'green') {
                colors2.push('red');
            }
            if (colors2[0] === 'lightblue' && colors2[1] === 'green' && colors2[2] === 'purple') {
                colors2.push('red');
            }


            if (colors2[0] === 'red' && colors2[1] === 'green' && colors2[2] === 'lightblue') {
                colors2.push('purple');
            }

            if (colors2[0] === 'red' && colors2[1] === 'lightblue' && colors2[2] === 'green') {
                colors2.push('purple');
            }

            if (colors2[0] === 'lightblue' && colors2[1] === 'green' && colors2[2] === 'red') {
                colors2.push('purple');
            }
            if (colors2[0] === 'lightblue' && colors2[1] === 'red' && colors2[2] === 'green') {
                colors2.push('purple');
            }

            if (colors2[0] === 'green' && colors2[1] === 'lightblue' && colors2[2] === 'red') {
                colors2.push('purple');
            }
            if (colors2[0] === 'green' && colors2[1] === 'red' && colors2[2] === 'lightblue') {
                colors2.push('purple');
            }


            if (colors2[0] === 'red' && colors2[1] === 'purple' && colors2[2] === 'lightblue') {
                colors2.push('green');
            }

            if (colors2[0] === 'red' && colors2[1] === 'lightblue' && colors2[2] === 'purple') {
                colors2.push('green');
            }

            if (colors2[0] === 'lightblue' && colors2[1] === 'purple' && colors2[2] === 'red') {
                colors2.push('green');
            }
            if (colors2[0] === 'lightblue' && colors2[1] === 'red' && colors2[2] === 'purple') {
                colors2.push('green');
            }

            if (colors2[0] === 'purple' && colors2[1] === 'lightblue' && colors2[2] === 'red') {
                colors2.push('green');
            }
            if (colors2[0] === 'purple' && colors2[1] === 'red' && colors2[2] === 'lightblue') {
                colors2.push('green');
            }

        }


        if (shapes2[0] === shapes2 [1] && shapes2[0] === shapes2 [2]) {
            shapes2.push(shapes2[0]);
        } else {
            if (shapes2[0] === 'oval' && shapes2[1] === 'diamond' && shapes2[2] === 'wave') {
                shapes2.push('rectangle');
            }

            if (shapes2[0] === 'oval' && shapes2[1] === 'wave' && shapes2[2] === 'diamond') {
                shapes2.push('rectangle');
            }

            if (shapes2[0] === 'wave' && shapes2[1] === 'diamond' && shapes2[2] === 'oval') {
                shapes2.push('rectangle');

            }
            if (shapes2[0] === 'wave' && shapes2[1] === 'oval' && shapes2[2] === 'diamond') {
                shapes2.push('rectangle');
            }

            if (shapes2[0] === 'diamond' && shapes2[1] === 'oval' && shapes2[2] === 'wave') {
                shapes2.push('rectangle');
            }
            if (shapes2[0] === 'diamond' && shapes2[1] === 'wave' && shapes2[2] === 'oval') {
                shapes2.push('rectangle');
            }


            if (shapes2[0] === 'oval' && shapes2[1] === 'diamond' && shapes2[2] === 'rectangle') {
                shapes2.push('wave');
            }

            if (shapes2[0] === 'oval' && shapes2[1] === 'rectangle' && shapes2[2] === 'diamond') {
                shapes2.push('wave');
            }

            if (shapes2[0] === 'rectangle' && shapes2[1] === 'oval' && shapes2[2] === 'diamond') {
                shapes2.push('wave');

            }
            if (shapes2[0] === 'rectangle' && shapes2[1] === 'diamond' && shapes2[2] === 'oval') {
                shapes2.push('wave');
            }

            if (shapes2[0] === 'diamond' && shapes2[1] === 'oval' && shapes2[2] === 'rectangle') {
                shapes2.push('wave');
            }
            if (shapes2[0] === 'diamond' && shapes2[1] === 'rectangle' && shapes2[2] === 'oval') {
                shapes2.push('wave');
            }


            if (shapes2[0] === 'oval' && shapes2[1] === 'wave' && shapes2[2] === 'rectangle') {
                shapes2.push('diamond');
            }

            if (shapes2[0] === 'oval' && shapes2[1] === 'rectangle' && shapes2[2] === 'wave') {
                shapes2.push('diamond');
            }

            if (shapes2[0] === 'rectangle' && shapes2[1] === 'wave' && shapes2[2] === 'oval') {
                shapes2.push('diamond');

            }
            if (shapes2[0] === 'rectangle' && shapes2[1] === 'oval' && shapes2[2] === 'wave') {
                shapes2.push('diamond');
            }

            if (shapes2[0] === 'wave' && shapes2[1] === 'oval' && shapes2[2] === 'rectangle') {
                shapes2.push('diamond');
            }
            if (shapes2[0] === 'wave' && shapes2[1] === 'rectangle' && shapes2[2] === 'oval') {
                shapes2.push('diamond');
            }


            if (shapes2[0] === 'diamond' && shapes2[1] === 'wave' && shapes2[2] === 'rectangle') {
                shapes2.push('oval');
            }

            if (shapes2[0] === 'diamond' && shapes2[1] === 'rectangle' && shapes2[2] === 'wave') {
                shapes2.push('oval');
            }

            if (shapes2[0] === 'rectangle' && shapes2[1] === 'wave' && shapes2[2] === 'diamond') {
                shapes2.push('oval');

            }
            if (shapes2[0] === 'rectangle' && shapes2[1] === 'diamond' && shapes2[2] === 'wave') {
                shapes2.push('oval');
            }

            if (shapes2[0] === 'wave' && shapes2[1] === 'diamond' && shapes2[2] === 'rectangle') {
                shapes2.push('oval');
            }
            if (shapes2[0] === 'wave' && shapes2[1] === 'rectangle' && shapes2[2] === 'diamond') {
                shapes2.push('oval');
            }

        }
        if (fills2[0] === fills2 [1] && fills2[0] === fills2 [2]) {
            fills2.push(fills2[0]);
        } else {
            if (fills2[0] === 'solid' && fills2[1] === 'stripped' && fills2[2] === 'open') {
                fills2.push('quadrillage');
            }

            if (fills2[0] === 'solid' && fills2[1] === 'open' && fills2[2] === 'stripped') {
                fills2.push('quadrillage');
            }

            if (fills2[0] === 'stripped' && fills2[1] === 'open' && fills2[2] === 'solid') {
                fills2.push('quadrillage');
            }
            if (fills2[0] === 'stripped' && fills2[1] === 'solid' && fills2[2] === 'open') {
                fills2.push('quadrillage');
            }

            if (fills2[0] === 'open' && fills2[1] === 'stripped' && fills2[2] === 'solid') {
                fills2.push('quadrillage');
            }
            if (fills2[0] === 'open' && fills2[1] === 'solid' && fills2[2] === 'stripped') {
                fills2.push('quadrillage');
            }


            if (fills2[0] === 'solid' && fills2[1] === 'stripped' && fills2[2] === 'quadrillage') {
                fills2.push('open');
            }

            if (fills2[0] === 'solid' && fills2[1] === 'quadrillage' && fills2[2] === 'stripped') {
                fills2.push('open');
            }

            if (fills2[0] === 'quadrillage' && fills2[1] === 'solid' && fills2[2] === 'stripped') {
                fills2.push('open');
            }
            if (fills2[0] === 'quadrillage' && fills2[1] === 'stripped' && fills2[2] === 'solid') {
                fills2.push('open');
            }

            if (fills2[0] === 'stripped' && fills2[1] === 'quadrillage' && fills2[2] === 'solid') {
                fills2.push('open');
            }
            if (fills2[0] === 'stripped' && fills2[1] === 'solid' && fills2[2] === 'quadrillage') {
                fills2.push('open');
            }


            if (fills2[0] === 'solid' && fills2[1] === 'open' && fills2[2] === 'quadrillage') {
                fills2.push('stripped');
            }

            if (fills2[0] === 'solid' && fills2[1] === 'quadrillage' && fills2[2] === 'open') {
                fills2.push('stripped');
            }

            if (fills2[0] === 'quadrillage' && fills2[1] === 'solid' && fills2[2] === 'open') {
                fills2.push('stripped');
            }
            if (fills2[0] === 'quadrillage' && fills2[1] === 'open' && fills2[2] === 'solid') {
                fills2.push('stripped');
            }

            if (fills2[0] === 'open' && fills2[1] === 'quadrillage' && fills2[2] === 'solid') {
                fills2.push('stripped');
            }
            if (fills2[0] === 'open' && fills2[1] === 'solid' && fills2[2] === 'quadrillage') {
                fills2.push('stripped');
            }


            if (fills2[0] === 'stripped' && fills2[1] === 'open' && fills2[2] === 'quadrillage') {
                fills2.push('solid');
            }

            if (fills2[0] === 'stripped' && fills2[1] === 'quadrillage' && fills2[2] === 'open') {
                fills2.push('solid');
            }

            if (fills2[0] === 'quadrillage' && fills2[1] === 'stripped' && fills2[2] === 'open') {
                fills2.push('solid');
            }
            if (fills2[0] === 'quadrillage' && fills2[1] === 'open' && fills2[2] === 'stripped') {
                fills2.push('solid');
            }

            if (fills2[0] === 'open' && fills2[1] === 'quadrillage' && fills2[2] === 'stripped') {
                fills2.push('solid');
            }
            if (fills2[0] === 'open' && fills2[1] === 'stripped' && fills2[2] === 'quadrillage') {
                fills2.push('solid');
            }


        }
        if (numbers2[0] === numbers2[1] && numbers2[0] === numbers2[2]) {
            numbers2.push(numbers2[0]);
        } else {
            if (numbers2[0] === 1 && numbers2[1] === 2 && numbers2[2] === 3) {
                numbers2.push(4);
            }

            if (numbers2[0] === 1 && numbers2[1] === 3 && numbers2[2] === 2) {
                numbers2.push(4);
            }

            if (numbers2[0] === 2 && numbers2[1] === 1 && numbers2[2] === 3) {
                numbers2.push(4);
            }
            if (numbers2[0] === 2 && numbers2[1] === 3 && numbers2[2] === 1) {
                numbers2.push(4);
            }

            if (numbers2[0] === 3 && numbers2[1] === 1 && numbers2[2] === 2) {
                numbers2.push(4);
            }
            if (numbers2[0] === 3 && numbers2[1] === 2 && numbers2[2] === 1) {
                numbers2.push(4);
            }


            if (numbers2[0] === 1 && numbers2[1] === 2 && numbers2[2] === 4) {
                numbers2.push(3);
            }

            if (numbers2[0] === 1 && numbers2[1] === 4 && numbers2[2] === 2) {
                numbers2.push(3);
            }

            if (numbers2[0] === 2 && numbers2[1] === 1 && numbers2[2] === 4) {
                numbers2.push(3);
            }
            if (numbers2[0] === 2 && numbers2[1] === 4 && numbers2[2] === 1) {
                numbers2.push(3);
            }

            if (numbers2[0] === 4 && numbers2[1] === 1 && numbers2[2] === 2) {
                numbers2.push(3);
            }
            if (numbers2[0] === 4 && numbers2[1] === 2 && numbers2[2] === 1) {
                numbers2.push(3);
            }


            if (numbers2[0] === 1 && numbers2[1] === 3 && numbers2[2] === 4) {
                numbers2.push(2);
            }

            if (numbers2[0] === 1 && numbers2[1] === 4 && numbers2[2] === 3) {
                numbers2.push(2);
            }

            if (numbers2[0] === 3 && numbers2[1] === 1 && numbers2[2] === 4) {
                numbers2.push(2);
            }
            if (numbers2[0] === 3 && numbers2[1] === 4 && numbers2[2] === 1) {
                numbers2.push(2);
            }

            if (numbers2[0] === 4 && numbers2[1] === 1 && numbers2[2] === 3) {
                numbers2.push(2);
            }
            if (numbers2[0] === 4 && numbers2[1] === 3 && numbers2[2] === 1) {
                numbers2.push(2);
            }


            if (numbers2[0] === 2 && numbers2[1] === 3 && numbers2[2] === 4) {
                numbers2.push(1);
            }

            if (numbers2[0] === 2 && numbers2[1] === 4 && numbers2[2] === 3) {
                numbers2.push(1);
            }

            if (numbers2[0] === 3 && numbers2[1] === 2 && numbers2[2] === 4) {
                numbers2.push(1);
            }
            if (numbers2[0] === 3 && numbers2[1] === 4 && numbers2[2] === 2) {
                numbers2.push(1);
            }

            if (numbers2[0] === 4 && numbers2[1] === 2 && numbers2[2] === 3) {
                numbers2.push(1);
            }
            if (numbers2[0] === 4 && numbers2[1] === 3 && numbers2[2] === 2) {
                numbers2.push(1);
            }
        }

        var cards4 =[];
        /*cards4.push({
            shape: shapes2[0],
            fill: fills2[0],
            color: colors2[0],
            number: numbers2[0]
        });
        cards4.push({
            shape: shapes2[1],
            fill: fills2[1],
            color: colors2[1],
            number: numbers2[1]
        });

        cards4.push({
            shape: shapes2[2],
            fill: fills2[2],
            color: colors2[2],
            number: numbers2[2]
        });*/

        cards4.push({
            shape: shapes2[3],
            fill: fills2[3],
            color: colors2[3],
            number: numbers2[3]
        });

        return cards4;
    },

    generateVirtualCard45: function(cards1){

        var colors2= $.map(cards1, function(el) { return $(el).data("color");});
        var shapes2 = $.map(cards1, function(el) { return $(el).data("shape");});
        var fills2 = $.map(cards1, function(el) { return $(el).data("fill");});
        var borders2 = $.map(cards1, function(el) { return $(el).data("border");});
        var numbers2 = $.map(cards1, function(el) { return $(el).data("number");});

        if (shapes2[0] === undefined  && colors2[0] === undefined && fills2[0] === undefined && borders2[0] === undefined && numbers2[0] === undefined) {
            colors2= $.map(cards1, function(el) { return el.color;});
            shapes2 = $.map(cards1, function(el) { return el.shape;});
            fills2 = $.map(cards1, function(el) { return el.fill;});
            borders2 = $.map(cards1, function(el) { return el.border;});
            numbers2 = $.map(cards1, function(el) { return el.number;});
        }


        if (colors2[0] === colors2[1] && colors2[0] === colors2[2]) {
            colors2.push(colors2[0]);

        } else {
            if (colors2[0] === 'red' && colors2[1] === 'green' && colors2[2] === 'purple') {
                colors2.push('lightblue');
            }

            if (colors2[0] === 'red' && colors2[1] === 'purple' && colors2[2] === 'green') {
                colors2.push('lightblue');
            }

            if (colors2[0] === 'purple' && colors2[1] === 'red' && colors2[2] === 'green') {
                colors2.push('lightblue');
            }
            if (colors2[0] === 'purple' && colors2[1] === 'green' && colors2[2] === 'red') {
                colors2.push('lightblue');
            }

            if (colors2[0] === 'green' && colors2[1] === 'purple' && colors2[2] === 'red') {
                colors2.push('lightblue');
            }
            if (colors2[0] === 'green' && colors2[1] === 'red' && colors2[2] === 'purple') {
                colors2.push('lightblue');
            }


            if (colors2[0] === 'green' && colors2[1] === 'purple' && colors2[2] === 'lightblue') {
                colors2.push('red');

            }

            if (colors2[0] === 'green' && colors2[1] === 'lightblue' && colors2[2] === 'purple') {
                colors2.push('red');
            }

            if (colors2[0] === 'purple' && colors2[1] === 'lightblue' && colors2[2] === 'green') {
                colors2.push('red');
            }
            if (colors2[0] === 'purple' && colors2[1] === 'green' && colors2[2] === 'lightblue') {
                colors2.push('red');
            }

            if (colors2[0] === 'lightblue' && colors2[1] === 'purple' && colors2[2] === 'green') {
                colors2.push('red');
            }
            if (colors2[0] === 'lightblue' && colors2[1] === 'green' && colors2[2] === 'purple') {
                colors2.push('red');
            }


            if (colors2[0] === 'red' && colors2[1] === 'green' && colors2[2] === 'lightblue') {
                colors2.push('purple');
            }

            if (colors2[0] === 'red' && colors2[1] === 'lightblue' && colors2[2] === 'green') {
                colors2.push('purple');
            }

            if (colors2[0] === 'lightblue' && colors2[1] === 'green' && colors2[2] === 'red') {
                colors2.push('purple');
            }
            if (colors2[0] === 'lightblue' && colors2[1] === 'red' && colors2[2] === 'green') {
                colors2.push('purple');
            }

            if (colors2[0] === 'green' && colors2[1] === 'lightblue' && colors2[2] === 'red') {
                colors2.push('purple');
            }
            if (colors2[0] === 'green' && colors2[1] === 'red' && colors2[2] === 'lightblue') {
                colors2.push('purple');
            }


            if (colors2[0] === 'red' && colors2[1] === 'purple' && colors2[2] === 'lightblue') {
                colors2.push('green');
            }

            if (colors2[0] === 'red' && colors2[1] === 'lightblue' && colors2[2] === 'purple') {
                colors2.push('green');
            }

            if (colors2[0] === 'lightblue' && colors2[1] === 'purple' && colors2[2] === 'red') {
                colors2.push('green');
            }
            if (colors2[0] === 'lightblue' && colors2[1] === 'red' && colors2[2] === 'purple') {
                colors2.push('green');
            }

            if (colors2[0] === 'purple' && colors2[1] === 'lightblue' && colors2[2] === 'red') {
                colors2.push('green');
            }
            if (colors2[0] === 'purple' && colors2[1] === 'red' && colors2[2] === 'lightblue') {
                colors2.push('green');
            }

        }


        if (shapes2[0] === shapes2 [1] && shapes2[0] === shapes2 [2]) {
            shapes2.push(shapes2[0]);
        } else {
            if (shapes2[0] === 'oval' && shapes2[1] === 'diamond' && shapes2[2] === 'wave') {
                shapes2.push('rectangle');
            }

            if (shapes2[0] === 'oval' && shapes2[1] === 'wave' && shapes2[2] === 'diamond') {
                shapes2.push('rectangle');
            }

            if (shapes2[0] === 'wave' && shapes2[1] === 'diamond' && shapes2[2] === 'oval') {
                shapes2.push('rectangle');

            }
            if (shapes2[0] === 'wave' && shapes2[1] === 'oval' && shapes2[2] === 'diamond') {
                shapes2.push('rectangle');
            }

            if (shapes2[0] === 'diamond' && shapes2[1] === 'oval' && shapes2[2] === 'wave') {
                shapes2.push('rectangle');
            }
            if (shapes2[0] === 'diamond' && shapes2[1] === 'wave' && shapes2[2] === 'oval') {
                shapes2.push('rectangle');
            }


            if (shapes2[0] === 'oval' && shapes2[1] === 'diamond' && shapes2[2] === 'rectangle') {
                shapes2.push('wave');
            }

            if (shapes2[0] === 'oval' && shapes2[1] === 'rectangle' && shapes2[2] === 'diamond') {
                shapes2.push('wave');
            }

            if (shapes2[0] === 'rectangle' && shapes2[1] === 'oval' && shapes2[2] === 'diamond') {
                shapes2.push('wave');

            }
            if (shapes2[0] === 'rectangle' && shapes2[1] === 'diamond' && shapes2[2] === 'oval') {
                shapes2.push('wave');
            }

            if (shapes2[0] === 'diamond' && shapes2[1] === 'oval' && shapes2[2] === 'rectangle') {
                shapes2.push('wave');
            }
            if (shapes2[0] === 'diamond' && shapes2[1] === 'rectangle' && shapes2[2] === 'oval') {
                shapes2.push('wave');
            }


            if (shapes2[0] === 'oval' && shapes2[1] === 'wave' && shapes2[2] === 'rectangle') {
                shapes2.push('diamond');
            }

            if (shapes2[0] === 'oval' && shapes2[1] === 'rectangle' && shapes2[2] === 'wave') {
                shapes2.push('diamond');
            }

            if (shapes2[0] === 'rectangle' && shapes2[1] === 'wave' && shapes2[2] === 'oval') {
                shapes2.push('diamond');

            }
            if (shapes2[0] === 'rectangle' && shapes2[1] === 'oval' && shapes2[2] === 'wave') {
                shapes2.push('diamond');
            }

            if (shapes2[0] === 'wave' && shapes2[1] === 'oval' && shapes2[2] === 'rectangle') {
                shapes2.push('diamond');
            }
            if (shapes2[0] === 'wave' && shapes2[1] === 'rectangle' && shapes2[2] === 'oval') {
                shapes2.push('diamond');
            }


            if (shapes2[0] === 'diamond' && shapes2[1] === 'wave' && shapes2[2] === 'rectangle') {
                shapes2.push('oval');
            }

            if (shapes2[0] === 'diamond' && shapes2[1] === 'rectangle' && shapes2[2] === 'wave') {
                shapes2.push('oval');
            }

            if (shapes2[0] === 'rectangle' && shapes2[1] === 'wave' && shapes2[2] === 'diamond') {
                shapes2.push('oval');

            }
            if (shapes2[0] === 'rectangle' && shapes2[1] === 'diamond' && shapes2[2] === 'wave') {
                shapes2.push('oval');
            }

            if (shapes2[0] === 'wave' && shapes2[1] === 'diamond' && shapes2[2] === 'rectangle') {
                shapes2.push('oval');
            }
            if (shapes2[0] === 'wave' && shapes2[1] === 'rectangle' && shapes2[2] === 'diamond') {
                shapes2.push('oval');
            }

        }
        if (fills2[0] === fills2 [1] && fills2[0] === fills2 [2]) {
            fills2.push(fills2[0]);
        } else {
            if (fills2[0] === 'solid' && fills2[1] === 'stripped' && fills2[2] === 'open') {
                fills2.push('quadrillage');
            }

            if (fills2[0] === 'solid' && fills2[1] === 'open' && fills2[2] === 'stripped') {
                fills2.push('quadrillage');
            }

            if (fills2[0] === 'stripped' && fills2[1] === 'open' && fills2[2] === 'solid') {
                fills2.push('quadrillage');
            }
            if (fills2[0] === 'stripped' && fills2[1] === 'solid' && fills2[2] === 'open') {
                fills2.push('quadrillage');
            }

            if (fills2[0] === 'open' && fills2[1] === 'stripped' && fills2[2] === 'solid') {
                fills2.push('quadrillage');
            }
            if (fills2[0] === 'open' && fills2[1] === 'solid' && fills2[2] === 'stripped') {
                fills2.push('quadrillage');
            }


            if (fills2[0] === 'solid' && fills2[1] === 'stripped' && fills2[2] === 'quadrillage') {
                fills2.push('open');
            }

            if (fills2[0] === 'solid' && fills2[1] === 'quadrillage' && fills2[2] === 'stripped') {
                fills2.push('open');
            }

            if (fills2[0] === 'quadrillage' && fills2[1] === 'solid' && fills2[2] === 'stripped') {
                fills2.push('open');
            }
            if (fills2[0] === 'quadrillage' && fills2[1] === 'stripped' && fills2[2] === 'solid') {
                fills2.push('open');
            }

            if (fills2[0] === 'stripped' && fills2[1] === 'quadrillage' && fills2[2] === 'solid') {
                fills2.push('open');
            }
            if (fills2[0] === 'stripped' && fills2[1] === 'solid' && fills2[2] === 'quadrillage') {
                fills2.push('open');
            }


            if (fills2[0] === 'solid' && fills2[1] === 'open' && fills2[2] === 'quadrillage') {
                fills2.push('stripped');
            }

            if (fills2[0] === 'solid' && fills2[1] === 'quadrillage' && fills2[2] === 'open') {
                fills2.push('stripped');
            }

            if (fills2[0] === 'quadrillage' && fills2[1] === 'solid' && fills2[2] === 'open') {
                fills2.push('stripped');
            }
            if (fills2[0] === 'quadrillage' && fills2[1] === 'open' && fills2[2] === 'solid') {
                fills2.push('stripped');
            }

            if (fills2[0] === 'open' && fills2[1] === 'quadrillage' && fills2[2] === 'solid') {
                fills2.push('stripped');
            }
            if (fills2[0] === 'open' && fills2[1] === 'solid' && fills2[2] === 'quadrillage') {
                fills2.push('stripped');
            }


            if (fills2[0] === 'stripped' && fills2[1] === 'open' && fills2[2] === 'quadrillage') {
                fills2.push('solid');
            }

            if (fills2[0] === 'stripped' && fills2[1] === 'quadrillage' && fills2[2] === 'open') {
                fills2.push('solid');
            }

            if (fills2[0] === 'quadrillage' && fills2[1] === 'stripped' && fills2[2] === 'open') {
                fills2.push('solid');
            }
            if (fills2[0] === 'quadrillage' && fills2[1] === 'open' && fills2[2] === 'stripped') {
                fills2.push('solid');
            }

            if (fills2[0] === 'open' && fills2[1] === 'quadrillage' && fills2[2] === 'stripped') {
                fills2.push('solid');
            }
            if (fills2[0] === 'open' && fills2[1] === 'stripped' && fills2[2] === 'quadrillage') {
                fills2.push('solid');
            }


        }


        if (borders2[0] === borders2 [1] && borders2[0] === borders2 [2]) {
            borders2.push(borders2[0]);
        } else {
            if (borders2[0] === 'rond' && borders2[1] === 'point' && borders2[2] === 'plein') {
                borders2.push('zigzag');
            }

            if (borders2[0] === 'rond' && borders2[1] === 'plein' && borders2[2] === 'point') {
                borders2.push('zigzag');
            }

            if (borders2[0] === 'plein' && borders2[1] === 'rond' && borders2[2] === 'point') {
                borders2.push('zigzag');
            }
            if (borders2[0] === 'plein' && borders2[1] === 'point' && borders2[2] === 'rond') {
                borders2.push('zigzag');
            }

            if (borders2[0] === 'point' && borders2[1] === 'rond' && borders2[2] === 'plein') {
                borders2.push('zigzag');
            }
            if (borders2[0] === 'point' && borders2[1] === 'plein' && borders2[2] === 'rond') {
                borders2.push('zigzag');
            }


            if (borders2[0] === 'rond' && borders2[1] === 'point' && borders2[2] === 'zigzag') {
                borders2.push('plein');
            }

            if (borders2[0] === 'rond' && borders2[1] === 'zigzag' && borders2[2] === 'point') {
                borders2.push('plein');
            }

            if (borders2[0] === 'zigzag' && borders2[1] === 'point' && borders2[2] === 'rond') {
                borders2.push('plein');
            }
            if (borders2[0] === 'zigzag' && borders2[1] === 'rond' && borders2[2] === 'point') {
                borders2.push('plein');
            }

            if (borders2[0] === 'point' && borders2[1] === 'zigzag' && borders2[2] === 'rond') {
                borders2.push('plein');
            }
            if (borders2[0] === 'point' && borders2[1] === 'rond' && borders2[2] === 'zigzag') {
                borders2.push('plein');
            }


            if (borders2[0] === 'rond' && borders2[1] === 'zigzag' && borders2[2] === 'plein') {
                borders2.push('point');
            }

            if (borders2[0] === 'rond' && borders2[1] === 'plein' && borders2[2] === 'zigzag') {
                borders2.push('point');
            }

            if (borders2[0] === 'zigzag' && borders2[1] === 'rond' && borders2[2] === 'plein') {
                borders2.push('point');
            }
            if (borders2[0] === 'zigzag' && borders2[1] === 'plein' && borders2[2] === 'rond') {
                borders2.push('point');
            }

            if (borders2[0] === 'plein' && borders2[1] === 'zigzag' && borders2[2] === 'rond') {
                borders2.push('point');
            }
            if (borders2[0] === 'plein' && borders2[1] === 'rond' && borders2[2] === 'zigzag') {
                borders2.push('point');
            }


            if (borders2[0] === 'plein' && borders2[1] === 'zigzag' && borders2[2] === 'point') {
                borders2.push('rond');
            }

            if (borders2[0] === 'plein' && borders2[1] === 'point' && borders2[2] === 'zigzag') {
                borders2.push('rond');
            }

            if (borders2[0] === 'point' && borders2[1] === 'zigzag' && borders2[2] === 'plein') {
                borders2.push('rond');
            }
            if (borders2[0] === 'point' && borders2[1] === 'plein' && borders2[2] === 'zigzag') {
                borders2.push('rond');
            }

            if (borders2[0] === 'zigzag' && borders2[1] === 'point' && borders2[2] === 'plein') {
                borders2.push('rond');
            }
            if (borders2[0] === 'zigzag' && borders2[1] === 'plein' && borders2[2] === 'point') {
                borders2.push('rond');
            }


        }



        if (numbers2[0] === numbers2[1] && numbers2[0] === numbers2[2]) {
            numbers2.push(numbers2[0]);
        } else {
            if (numbers2[0] === 1 && numbers2[1] === 2 && numbers2[2] === 3) {
                numbers2.push(4);
            }

            if (numbers2[0] === 1 && numbers2[1] === 3 && numbers2[2] === 2) {
                numbers2.push(4);
            }

            if (numbers2[0] === 2 && numbers2[1] === 1 && numbers2[2] === 3) {
                numbers2.push(4);
            }
            if (numbers2[0] === 2 && numbers2[1] === 3 && numbers2[2] === 1) {
                numbers2.push(4);
            }

            if (numbers2[0] === 3 && numbers2[1] === 1 && numbers2[2] === 2) {
                numbers2.push(4);
            }
            if (numbers2[0] === 3 && numbers2[1] === 2 && numbers2[2] === 1) {
                numbers2.push(4);
            }


            if (numbers2[0] === 1 && numbers2[1] === 2 && numbers2[2] === 4) {
                numbers2.push(3);
            }

            if (numbers2[0] === 1 && numbers2[1] === 4 && numbers2[2] === 2) {
                numbers2.push(3);
            }

            if (numbers2[0] === 2 && numbers2[1] === 1 && numbers2[2] === 4) {
                numbers2.push(3);
            }
            if (numbers2[0] === 2 && numbers2[1] === 4 && numbers2[2] === 1) {
                numbers2.push(3);
            }

            if (numbers2[0] === 4 && numbers2[1] === 1 && numbers2[2] === 2) {
                numbers2.push(3);
            }
            if (numbers2[0] === 4 && numbers2[1] === 2 && numbers2[2] === 1) {
                numbers2.push(3);
            }


            if (numbers2[0] === 1 && numbers2[1] === 3 && numbers2[2] === 4) {
                numbers2.push(2);
            }

            if (numbers2[0] === 1 && numbers2[1] === 4 && numbers2[2] === 3) {
                numbers2.push(2);
            }

            if (numbers2[0] === 3 && numbers2[1] === 1 && numbers2[2] === 4) {
                numbers2.push(2);
            }
            if (numbers2[0] === 3 && numbers2[1] === 4 && numbers2[2] === 1) {
                numbers2.push(2);
            }

            if (numbers2[0] === 4 && numbers2[1] === 1 && numbers2[2] === 3) {
                numbers2.push(2);
            }
            if (numbers2[0] === 4 && numbers2[1] === 3 && numbers2[2] === 1) {
                numbers2.push(2);
            }


            if (numbers2[0] === 2 && numbers2[1] === 3 && numbers2[2] === 4) {
                numbers2.push(1);
            }

            if (numbers2[0] === 2 && numbers2[1] === 4 && numbers2[2] === 3) {
                numbers2.push(1);
            }

            if (numbers2[0] === 3 && numbers2[1] === 2 && numbers2[2] === 4) {
                numbers2.push(1);
            }
            if (numbers2[0] === 3 && numbers2[1] === 4 && numbers2[2] === 2) {
                numbers2.push(1);
            }

            if (numbers2[0] === 4 && numbers2[1] === 2 && numbers2[2] === 3) {
                numbers2.push(1);
            }
            if (numbers2[0] === 4 && numbers2[1] === 3 && numbers2[2] === 2) {
                numbers2.push(1);
            }
        }

        var cards4 =[];
        /*cards4.push({
            shape: shapes2[0],
            fill: fills2[0],
            color: colors2[0],
            number: numbers2[0]
        });
        cards4.push({
            shape: shapes2[1],
            fill: fills2[1],
            color: colors2[1],
            number: numbers2[1]
        });

        cards4.push({
            shape: shapes2[2],
            fill: fills2[2],
            color: colors2[2],
            number: numbers2[2]
        });*/

        cards4.push({
            shape: shapes2[3],
            fill: fills2[3],
            color: colors2[3],
            border: borders2[3],
            number: numbers2[3]
        });

        return cards4;
    },


    generateVirtualCard55: function(cards1){

        var colors2= $.map(cards1, function(el) { return $(el).data("color");});
        var shapes2 = $.map(cards1, function(el) { return $(el).data("shape");});
        var fills2 = $.map(cards1, function(el) { return $(el).data("fill");});
        var borders2 = $.map(cards1, function(el) { return $(el).data("border");});
        var numbers2 = $.map(cards1, function(el) { return $(el).data("number");});

        if (shapes2[0] === undefined  && colors2[0] === undefined && fills2[0] === undefined && borders2[0] === undefined && numbers2[0] === undefined) {
            colors2= $.map(cards1, function(el) { return el.color;});
            shapes2 = $.map(cards1, function(el) { return el.shape;});
            fills2 = $.map(cards1, function(el) { return el.fill;});
            borders2 = $.map(cards1, function(el) { return el.border;});
            numbers2 = $.map(cards1, function(el) { return el.number;});
        }
        var colorsSet = new Set(colors2);
        var shapesSet = new Set(shapes2);
        var fillsSet = new Set(fills2);
        var bordersSet = new Set(borders2);
        var numbersSet = new Set(numbers2);

        if (colors2[0] === colors2[1] && colors2[0] === colors2[2] && colors2[0] === colors2[3]) {
            colors2.push(colors2[0]);

        }else {
            if(colorsSet.has('green') && colorsSet.has('red') && colorsSet.has('lightblue') && colorsSet.has('yellow')){
                colors2.push('purple');
            }
            if(colorsSet.has('green') && colorsSet.has('red') && colorsSet.has('yellow') && colorsSet.has('purple')){
                colors2.push('lightblue');
            }
            if(colorsSet.has('green') && colorsSet.has('yellow') && colorsSet.has('lightblue')&& colorsSet.has('purple')){
                colors2.push('red');
            }
            if(colorsSet.has('green') && colorsSet.has('red') && colorsSet.has('lightblue')&& colorsSet.has('purple')){
                colors2.push('yellow');
            }
            if(colorsSet.has('yellow') && colorsSet.has('red') && colorsSet.has('lightblue')&& colorsSet.has('purple')){
                colors2.push('green');
            }

        }

        if (shapes2[0] === shapes2 [1] && shapes2[0] === shapes2 [2]&& shapes2[0] === shapes2 [3]) {
            shapes2.push(shapes2[0]);
        } else {
            if(shapesSet.has('diamond') && shapesSet.has('rectangle') && shapesSet.has('triangle') && shapesSet.has('oval')){
                shapes2.push('wave');
        }
            if(shapesSet.has('wave') && shapesSet.has('rectangle') && shapesSet.has('triangle') && shapesSet.has('oval')){
                shapes2.push('diamond');
            }
            if(shapesSet.has('diamond') && shapesSet.has('wave') && shapesSet.has('triangle')&& shapesSet.has('oval')){
                shapes2.push('rectangle');
            }
            if(shapesSet.has('diamond') && shapesSet.has('rectangle') && shapesSet.has('wave')&& shapesSet.has('oval')){
                shapes2.push('triangle');
            }
            if(shapesSet.has('diamond') && shapesSet.has('rectangle') && shapesSet.has('triangle')&& shapesSet.has('wave')){
                shapes2.push('oval');
            }
        }

        if (fills2[0] === fills2 [1] && fills2[0] === fills2 [2]&& fills2[0] === fills2 [3]) {
            fills2.push(fills2[0]);
        } else {
            if(fillsSet.has('open') && fillsSet.has('solid') && fillsSet.has('stripped') && fillsSet.has('quadrillage')){
                fills2.push('pointille');
            }
            if(fillsSet.has('pointille') && fillsSet.has('solid') && fillsSet.has('stripped') && fillsSet.has('quadrillage')){
                fills2.push('open');
            }
            if(fillsSet.has('open') && fillsSet.has('pointille') && fillsSet.has('stripped')&& fillsSet.has('quadrillage')){
                fills2.push('solid');
            }
            if(fillsSet.has('open') && fillsSet.has('solid') && fillsSet.has('pointille')&& fillsSet.has('quadrillage')){
                fills2.push('stripped');
            }
            if(fillsSet.has('open') && fillsSet.has('solid') && fillsSet.has('stripped')&& fillsSet.has('pointille')){
                fills2.push('quadrillage');
            }
        }


        if (borders2[0] === borders2 [1] && borders2[0] === borders2 [2]&& borders2[0] === borders2 [3]) {
            borders2.push(borders2[0]);
        }else {
            if (bordersSet.has('plein') && bordersSet.has('point') && bordersSet.has('rond') && bordersSet.has('zigzag')) {
                borders2.push('hachure');
            }
            if (bordersSet.has('hachure') && bordersSet.has('point') && bordersSet.has('rond') && bordersSet.has('zigzag')) {
                borders2.push('plein');
            }
            if (bordersSet.has('plein') && bordersSet.has('hachure') && bordersSet.has('rond') && bordersSet.has('zigzag')) {
                borders2.push('point');
            }
            if (bordersSet.has('plein') && bordersSet.has('point') && bordersSet.has('hachure') && bordersSet.has('zigzag')) {
                borders2.push('rond');
            }
            if (bordersSet.has('plein') && bordersSet.has('point') && bordersSet.has('rond') && bordersSet.has('hachure')) {
                borders2.push('zigzag');
            }
        }

        if (numbers2[0] === numbers2 [1] && numbers2[0] === numbers2 [2]&& numbers2[0] === numbers2 [3]) {
            numbers2.push(numbers2[0]);
        }else {
            if (numbersSet.has(1) && numbersSet.has(2) && numbersSet.has(3) && numbersSet.has(4)) {
                numbers2.push(5);
            }
            if (numbersSet.has(5) && numbersSet.has(2) && numbersSet.has(3) && numbersSet.has(4)) {
                numbers2.push(1);
            }
            if (numbersSet.has(1) && numbersSet.has(5) && numbersSet.has(3) && numbersSet.has(4)) {
                numbers2.push(2);
            }
            if (numbersSet.has(1) && numbersSet.has(2) && numbersSet.has(5) && numbersSet.has(4)) {
                numbers2.push(3);
            }
            if (numbersSet.has(1) && numbersSet.has(2) && numbersSet.has(3) && numbersSet.has(5)) {
                numbers2.push(4);
            }
        }


        var card5 = [];
        card5.push({
            shape: shapes2[4],
            fill: fills2[4],
            color: colors2[4],
            border: borders2[4],
            number: numbers2[4]
        });

        return card5;
    },




    comparaison2Carte4:function (carte1, carte2){
        if( carte1[0].color !== undefined && carte1[0].shape !== undefined && carte1[0].fill !== undefined && carte1[0].number !== undefined && carte2[0].color !== undefined && carte2[0].shape !== undefined && carte2[0].fill !== undefined && carte2[0].number !== undefined){
            return carte1[0].color === carte2[0].color && carte1[0].shape === carte2[0].shape && carte1[0].number === carte2[0].number && carte1[0].fill === carte2[0].fill ;
        }
        else {
            return false;
        }


    },
	comparaison2Carte3:function (carte1, carte2) {
		if( carte1[0].color !== undefined && carte1[0].shape !== undefined && carte1[0].number !== undefined && carte2[0].color !== undefined && carte2[0].shape !== undefined  && carte2[0].number !== undefined){
			return carte1[0].color === carte2[0].color && carte1[0].shape === carte2[0].shape && carte1[0].number === carte2[0].number  ;

		}
	},
	comparaison2Carte5:function (carte1, carte2) {
		if( carte1[0].color !== undefined && carte1[0].border!== undefined && carte1[0].shape !== undefined && carte1[0].fill !== undefined && carte1[0].number !== undefined && carte2[0].color !== undefined && carte2[0].border !== undefined && carte2[0].shape !== undefined && carte2[0].fill !== undefined && carte2[0].number !== undefined){
			return carte1[0].color === carte2[0].color && carte1[0].shape === carte2[0].shape && carte1[0].number === carte2[0].number && carte1[0].fill === carte2[0].fill  && carte1[0].border === carte2[0].border;

		}
	},








	validateHyperSet0: function() {
		var carteAB = [];
		var carteCD = [];
		var carteAC = [];
		var carteBD = [];
		var carteAD = [];
		var carteBC = [];

		carteAB.splice(0,1,this.selectedHS0[0]);
		carteAB.splice(1,1,this.selectedHS0[1]);
		carteCD.splice(0,1,this.selectedHS0[2]);
		carteCD.splice(1,1,this.selectedHS0[3]);

		carteAC.splice(0,1,this.selectedHS0[0]);
		carteAC.splice(1,1,this.selectedHS0[2]);
		carteBD.splice(0,1,this.selectedHS0[1]);
		carteBD.splice(1,1,this.selectedHS0[3]);

		carteAD.splice(0,1,this.selectedHS0[0]);
		carteAD.splice(1,1,this.selectedHS0[3]);
		carteBC.splice(0,1,this.selectedHS0[1]);
		carteBC.splice(1,1,this.selectedHS0[2]);

        var card3 = this.generateVirtualCard33(carteAB);
        var card4 = this.generateVirtualCard33(carteCD);
        if (this.comparaison2Carte3(card3,card4)){
            return true;
        }

        var card5 = this.generateVirtualCard33(carteAC);
        var card6 = this.generateVirtualCard33(carteBD);
        if (this.comparaison2Carte3(card5,card6)){
            return true;
        }

        var card7 = this.generateVirtualCard33(carteAD);
        var card8 = this.generateVirtualCard33(carteBC);
        if (this.comparaison2Carte3(card7,card8)){
            return true;
        }

        return false;

	},

	validateHyperSet: function() {
		var carteAB = [];
		var carteCD = [];
		var carteAC = [];
		var carteBD = [];
		var carteAD = [];
		var carteBC = [];

		carteAB.splice(0,1,this.selectedHS[0]);
		carteAB.splice(1,1,this.selectedHS[1]);
		carteCD.splice(0,1,this.selectedHS[2]);
		carteCD.splice(1,1,this.selectedHS[3]);

		carteAC.splice(0,1,this.selectedHS[0]);
		carteAC.splice(1,1,this.selectedHS[2]);
		carteBD.splice(0,1,this.selectedHS[1]);
		carteBD.splice(1,1,this.selectedHS[3]);

		carteAD.splice(0,1,this.selectedHS[0]);
		carteAD.splice(1,1,this.selectedHS[3]);
		carteBC.splice(0,1,this.selectedHS[1]);
		carteBC.splice(1,1,this.selectedHS[2]);



        var card3 = this.generateVirtualCard34(carteAB);
        var card4 = this.generateVirtualCard34(carteCD);
        if (this.comparaison2Carte4(card3,card4)){
            return true;
        }

        var card5 = this.generateVirtualCard34(carteAC);
        var card6 = this.generateVirtualCard34(carteBD);
        if (this.comparaison2Carte4(card5,card6)){
            return true;
        }

        var card7 = this.generateVirtualCard34(carteAD);
        var card8 = this.generateVirtualCard34(carteBC);
        if (this.comparaison2Carte4(card7,card8)){
            return true;
        }

        return false;


	},

	validateHyperSet1: function() {
		var carteAB = [];
		var carteCD = [];
		var carteAC = [];
		var carteBD = [];
		var carteAD = [];
		var carteBC = [];

		carteAB.splice(0,1,this.selectedHS1[0]);
		carteAB.splice(1,1,this.selectedHS1[1]);
		carteCD.splice(0,1,this.selectedHS1[2]);
		carteCD.splice(1,1,this.selectedHS1[3]);

		carteAC.splice(0,1,this.selectedHS1[0]);
		carteAC.splice(1,1,this.selectedHS1[2]);
		carteBD.splice(0,1,this.selectedHS1[1]);
		carteBD.splice(1,1,this.selectedHS1[3]);

		carteAD.splice(0,1,this.selectedHS1[0]);
		carteAD.splice(1,1,this.selectedHS1[3]);
		carteBC.splice(0,1,this.selectedHS1[1]);
		carteBC.splice(1,1,this.selectedHS1[2]);


        var card3 = this.generateVirtualCard35(carteAB);
        var card4 = this.generateVirtualCard35(carteCD);
        if (this.comparaison2Carte5(card3,card4)){
            return true;
        }

        var card5 = this.generateVirtualCard35(carteAC);
        var card6 = this.generateVirtualCard35(carteBD);
        if (this.comparaison2Carte5(card5,card6)){
            return true;
        }

        var card7 = this.generateVirtualCard35(carteAD);
        var card8 = this.generateVirtualCard35(carteBC);
        if (this.comparaison2Carte5(card7,card8)){
            return true;
        }

        return false;


	},


	validateHyperSet2: function() {


		var carteABC = [];
		var carteDEF = [];

		var carteABD = [];
		var carteCEF = [];

		var carteABE = [];
		var carteCDF = [];

		var carteABF = [];
		var carteCDE = [];

		var carteACD = [];
		var carteBEF = [];

		var carteACE = [];
		var carteBDF = [];

		var carteACF = [];
		var carteBDE = [];

		var carteADE = [];
		var carteBCF = [];

		var carteADF = [];
		var carteBCE = [];

		var carteAEF = [];
		var carteBCD = [];


		carteABC.splice(0, 1, this.selectedHS2[0]);
		carteABC.splice(1, 1, this.selectedHS2[1]);
		carteABC.splice(2, 1, this.selectedHS2[2]);
		carteDEF.splice(0, 1, this.selectedHS2[3]);
		carteDEF.splice(1, 1, this.selectedHS2[4]);
		carteDEF.splice(2, 1, this.selectedHS2[5]);

		carteABD.splice(0, 1, this.selectedHS2[0]);
		carteABD.splice(1, 1, this.selectedHS2[1]);
		carteABD.splice(2, 1, this.selectedHS2[3]);
		carteCEF.splice(0, 1, this.selectedHS2[2]);
		carteCEF.splice(1, 1, this.selectedHS2[4]);
		carteCEF.splice(2, 1, this.selectedHS2[5]);

		carteABE.splice(0, 1, this.selectedHS2[0]);
		carteABE.splice(1, 1, this.selectedHS2[1]);
		carteABE.splice(2, 1, this.selectedHS2[4]);
		carteCDF.splice(0, 1, this.selectedHS2[2]);
		carteCDF.splice(1, 1, this.selectedHS2[3]);
		carteCDF.splice(2, 1, this.selectedHS2[5]);

		carteABF.splice(0, 1, this.selectedHS2[0]);
		carteABF.splice(1, 1, this.selectedHS2[1]);
		carteABF.splice(2, 1, this.selectedHS2[5]);
		carteCDE.splice(0, 1, this.selectedHS2[2]);
		carteCDE.splice(1, 1, this.selectedHS2[3]);
		carteCDE.splice(2, 1, this.selectedHS2[4]);

		carteACD.splice(0, 1, this.selectedHS2[0]);
		carteACD.splice(1, 1, this.selectedHS2[2]);
		carteACD.splice(2, 1, this.selectedHS2[3]);
		carteBEF.splice(0, 1, this.selectedHS2[1]);
		carteBEF.splice(1, 1, this.selectedHS2[4]);
		carteBEF.splice(2, 1, this.selectedHS2[5]);

		carteACE.splice(0, 1, this.selectedHS2[0]);
		carteACE.splice(1, 1, this.selectedHS2[2]);
		carteACE.splice(2, 1, this.selectedHS2[4]);
		carteBDF.splice(0, 1, this.selectedHS2[1]);
		carteBDF.splice(1, 1, this.selectedHS2[3]);
		carteBDF.splice(2, 1, this.selectedHS2[5]);

		carteACF.splice(0, 1, this.selectedHS2[0]);
		carteACF.splice(1, 1, this.selectedHS2[2]);
		carteACF.splice(2, 1, this.selectedHS2[5]);
		carteBDE.splice(0, 1, this.selectedHS2[1]);
		carteBDE.splice(1, 1, this.selectedHS2[3]);
		carteBDE.splice(2, 1, this.selectedHS2[4]);

		carteADE.splice(0, 1, this.selectedHS2[0]);
		carteADE.splice(1, 1, this.selectedHS2[3]);
		carteADE.splice(2, 1, this.selectedHS2[4]);
		carteBCF.splice(0, 1, this.selectedHS2[1]);
		carteBCF.splice(1, 1, this.selectedHS2[2]);
		carteBCF.splice(2, 1, this.selectedHS2[5]);

		carteADF.splice(0, 1, this.selectedHS2[0]);
		carteADF.splice(1, 1, this.selectedHS2[3]);
		carteADF.splice(2, 1, this.selectedHS2[5]);
		carteBCE.splice(0, 1, this.selectedHS2[1]);
		carteBCE.splice(1, 1, this.selectedHS2[2]);
		carteBCE.splice(2, 1, this.selectedHS2[4]);

		carteAEF.splice(0, 1, this.selectedHS2[0]);
		carteAEF.splice(1, 1, this.selectedHS2[4]);
		carteAEF.splice(2, 1, this.selectedHS2[5]);
		carteBCD.splice(0, 1, this.selectedHS2[1]);
		carteBCD.splice(1, 1, this.selectedHS2[2]);
		carteBCD.splice(2, 1, this.selectedHS2[3]);


        var card3 = this.generateVirtualCard44(carteABC);
        var card4 = this.generateVirtualCard44(carteDEF);
        if (this.comparaison2Carte4(card3,card4)){
            return true;
        }

        var card5 = this.generateVirtualCard44(carteABD);
        var card6 = this.generateVirtualCard44(carteCEF);
        if (this.comparaison2Carte4(card5,card6)){
            return true;
        }
        var card7 = this.generateVirtualCard44(carteABE);
        var card8 = this.generateVirtualCard44(carteCDF);
        if (this.comparaison2Carte4(card7,card8)){
            return true;
        }
        var card9 = this.generateVirtualCard44(carteABF);
        var card10 = this.generateVirtualCard44(carteCDE);
        if (this.comparaison2Carte4(card9,card10)){
            return true;
        }
        var card11 = this.generateVirtualCard44(carteACD);
        var card12 = this.generateVirtualCard44(carteBEF);
        if (this.comparaison2Carte4(card11,card12)){
            return true;
        }
        var card13 = this.generateVirtualCard44(carteACE);
        var card14 = this.generateVirtualCard44(carteBDF);
        if (this.comparaison2Carte4(card13,card14)){
            return true;
        }
        var card15 = this.generateVirtualCard44(carteACF);
        var card16 = this.generateVirtualCard44(carteBDE);
        if (this.comparaison2Carte4(card15,card16)){
            return true;
        }
        var card17 = this.generateVirtualCard44(carteADE);
        var card18 = this.generateVirtualCard44(carteBCF);
        if (this.comparaison2Carte4(card17,card18)){
            return true;
        }
        var card19= this.generateVirtualCard44(carteADF);
        var card20 = this.generateVirtualCard44(carteBCE);
        if (this.comparaison2Carte4(card19,card20)){
            return true;
        }
        var card21 = this.generateVirtualCard44(carteAEF);
        var card22 = this.generateVirtualCard44(carteBCD);
        if (this.comparaison2Carte4(card21,card22)){
            return true;
        }
        return false;


	},

    validateHyperSet3: function() {


        var carteABC = [];
        var carteDEF = [];

        var carteABD = [];
        var carteCEF = [];

        var carteABE = [];
        var carteCDF = [];

        var carteABF = [];
        var carteCDE = [];

        var carteACD = [];
        var carteBEF = [];

        var carteACE = [];
        var carteBDF = [];

        var carteACF = [];
        var carteBDE = [];

        var carteADE = [];
        var carteBCF = [];

        var carteADF = [];
        var carteBCE = [];

        var carteAEF = [];
        var carteBCD = [];


        carteABC.splice(0, 1, this.selectedHS3[0]);
        carteABC.splice(1, 1, this.selectedHS3[1]);
        carteABC.splice(2, 1, this.selectedHS3[2]);
        carteDEF.splice(0, 1, this.selectedHS3[3]);
        carteDEF.splice(1, 1, this.selectedHS3[4]);
        carteDEF.splice(2, 1, this.selectedHS3[5]);

        carteABD.splice(0, 1, this.selectedHS3[0]);
        carteABD.splice(1, 1, this.selectedHS3[1]);
        carteABD.splice(2, 1, this.selectedHS3[3]);
        carteCEF.splice(0, 1, this.selectedHS3[2]);
        carteCEF.splice(1, 1, this.selectedHS3[4]);
        carteCEF.splice(2, 1, this.selectedHS3[5]);

        carteABE.splice(0, 1, this.selectedHS3[0]);
        carteABE.splice(1, 1, this.selectedHS3[1]);
        carteABE.splice(2, 1, this.selectedHS3[4]);
        carteCDF.splice(0, 1, this.selectedHS3[2]);
        carteCDF.splice(1, 1, this.selectedHS3[3]);
        carteCDF.splice(2, 1, this.selectedHS3[5]);

        carteABF.splice(0, 1, this.selectedHS3[0]);
        carteABF.splice(1, 1, this.selectedHS3[1]);
        carteABF.splice(2, 1, this.selectedHS3[5]);
        carteCDE.splice(0, 1, this.selectedHS3[2]);
        carteCDE.splice(1, 1, this.selectedHS3[3]);
        carteCDE.splice(2, 1, this.selectedHS3[4]);

        carteACD.splice(0, 1, this.selectedHS3[0]);
        carteACD.splice(1, 1, this.selectedHS3[2]);
        carteACD.splice(2, 1, this.selectedHS3[3]);
        carteBEF.splice(0, 1, this.selectedHS3[1]);
        carteBEF.splice(1, 1, this.selectedHS3[4]);
        carteBEF.splice(2, 1, this.selectedHS3[5]);

        carteACE.splice(0, 1, this.selectedHS3[0]);
        carteACE.splice(1, 1, this.selectedHS3[2]);
        carteACE.splice(2, 1, this.selectedHS3[4]);
        carteBDF.splice(0, 1, this.selectedHS3[1]);
        carteBDF.splice(1, 1, this.selectedHS3[3]);
        carteBDF.splice(2, 1, this.selectedHS3[5]);

        carteACF.splice(0, 1, this.selectedHS3[0]);
        carteACF.splice(1, 1, this.selectedHS3[2]);
        carteACF.splice(2, 1, this.selectedHS3[5]);
        carteBDE.splice(0, 1, this.selectedHS3[1]);
        carteBDE.splice(1, 1, this.selectedHS3[3]);
        carteBDE.splice(2, 1, this.selectedHS3[4]);

        carteADE.splice(0, 1, this.selectedHS3[0]);
        carteADE.splice(1, 1, this.selectedHS3[3]);
        carteADE.splice(2, 1, this.selectedHS3[4]);
        carteBCF.splice(0, 1, this.selectedHS3[1]);
        carteBCF.splice(1, 1, this.selectedHS3[2]);
        carteBCF.splice(2, 1, this.selectedHS3[5]);

        carteADF.splice(0, 1, this.selectedHS3[0]);
        carteADF.splice(1, 1, this.selectedHS3[3]);
        carteADF.splice(2, 1, this.selectedHS3[5]);
        carteBCE.splice(0, 1, this.selectedHS3[1]);
        carteBCE.splice(1, 1, this.selectedHS3[2]);
        carteBCE.splice(2, 1, this.selectedHS3[4]);

        carteAEF.splice(0, 1, this.selectedHS3[0]);
        carteAEF.splice(1, 1, this.selectedHS3[4]);
        carteAEF.splice(2, 1, this.selectedHS3[5]);
        carteBCD.splice(0, 1, this.selectedHS3[1]);
        carteBCD.splice(1, 1, this.selectedHS3[2]);
        carteBCD.splice(2, 1, this.selectedHS3[3]);


        var card3 = this.generateVirtualCard45(carteABC);
        var card4 = this.generateVirtualCard45(carteDEF);
        if (this.comparaison2Carte5(card3,card4)){
            return true;
        }

        var card5 = this.generateVirtualCard45(carteABD);
        var card6 = this.generateVirtualCard45(carteCEF);
        if (this.comparaison2Carte5(card5,card6)){
            return true;
        }
        var card7 = this.generateVirtualCard45(carteABE);
        var card8 = this.generateVirtualCard45(carteCDF);
        if (this.comparaison2Carte5(card7,card8)){
            return true;
        }
        var card9 = this.generateVirtualCard45(carteABF);
        var card10 = this.generateVirtualCard45(carteCDE);
        if (this.comparaison2Carte5(card9,card10)){
            return true;
        }
        var card11 = this.generateVirtualCard45(carteACD);
        var card12 = this.generateVirtualCard45(carteBEF);
        if (this.comparaison2Carte5(card11,card12)){
            return true;
        }
        var card13 = this.generateVirtualCard45(carteACE);
        var card14 = this.generateVirtualCard45(carteBDF);
        if (this.comparaison2Carte5(card13,card14)){
            return true;
        }
        var card15 = this.generateVirtualCard45(carteACF);
        var card16 = this.generateVirtualCard45(carteBDE);
        if (this.comparaison2Carte5(card15,card16)){
            return true;
        }
        var card17 = this.generateVirtualCard45(carteADE);
        var card18 = this.generateVirtualCard45(carteBCF);
        if (this.comparaison2Carte5(card17,card18)){
            return true;
        }
        var card19= this.generateVirtualCard45(carteADF);
        var card20 = this.generateVirtualCard45(carteBCE);
        if (this.comparaison2Carte5(card19,card20)){
            return true;
        }
        var card21 = this.generateVirtualCard45(carteAEF);
        var card22 = this.generateVirtualCard45(carteBCD);
        if (this.comparaison2Carte5(card21,card22)){
            return true;
        }
        return false;


    },



	validateSet0: function() {
		var self = this;

		var colors = $.map(self.selected0, function(el) { return $(el).data("color");});
		var shapes = $.map(self.selected0, function(el) { return $(el).data("shape"); });
		var numbers= $.map(self.selected0, function(el) { return $(el).data("number"); });

		return (self.isSet(colors) && self.isSet(shapes) && self.isSet(numbers));
	},

	validateSet: function() {
		var self = this;

		var colors = $.map(self.selected, function(el) { return $(el).data("color");});
		var shapes = $.map(self.selected, function(el) { return $(el).data("shape"); });
		var fills= $.map(self.selected, function(el) { return $(el).data("fill"); });
		var numbers = $.map(self.selected, function(el) { return $(el).data("number"); });

		return (self.isSet(colors) && self.isSet(shapes) && self.isSet(fills) && self.isSet(numbers));
	},

	validateSet35: function() {
		var self = this;

		var colors = $.map(self.selected1, function(el) { return $(el).data("color");});
		var shapes = $.map(self.selected1, function(el) { return $(el).data("shape"); });
		var fills= $.map(self.selected1, function(el) { return $(el).data("fill"); });
		var borders= $.map(self.selected1, function(el) { return $(el).data("border"); });
		var numbers = $.map(self.selected1, function(el) { return $(el).data("number"); });


		return (self.isSet(colors) && self.isSet(shapes) && self.isSet(fills) &&  self.isSet(borders) && self.isSet(numbers));
	},

	validateSet44: function() {
		var self = this;

		var colors = $.map(self.selected2, function(el) { return $(el).data("color");});
		var shapes = $.map(self.selected2, function(el) { return $(el).data("shape"); });
		var fills= $.map(self.selected2, function(el) { return $(el).data("fill"); });
		var numbers = $.map(self.selected2, function(el) { return $(el).data("number"); });


		return (self.isSet4(colors) && self.isSet4(shapes) && self.isSet4(fills) && self.isSet4(numbers));
	},

	validateSet45: function() {
		var self = this;

		var colors = $.map(self.selected3, function(el) { return $(el).data("color");});
		var shapes = $.map(self.selected3, function(el) { return $(el).data("shape"); });
		var fills= $.map(self.selected3, function(el) { return $(el).data("fill"); });
		var borders= $.map(self.selected3, function(el) { return $(el).data("border"); });
		var numbers = $.map(self.selected3, function(el) { return $(el).data("number"); });


		return (self.isSet4(colors) && self.isSet4(shapes) && self.isSet4(borders) &&  self.isSet4(fills) && self.isSet4(numbers));
	},

	validateSet55: function() {
		var self = this;

		var colors = $.map(self.selected4, function(el) { return $(el).data("color");});
		var shapes = $.map(self.selected4, function(el) { return $(el).data("shape"); });
		var fills= $.map(self.selected4, function(el) { return $(el).data("fill"); });
		var borders= $.map(self.selected4, function(el) { return $(el).data("border"); });
		var numbers = $.map(self.selected4, function(el) { return $(el).data("number"); });


		return (self.isSet5(colors) && self.isSet5(shapes) && self.isSet5(fills) && self.isSet5(borders) && self.isSet5(numbers));
	},





	isSet: function(arr) {
		var unique = [];
		$.each(arr, function(i, el){
			if($.inArray(el, unique) === -1) unique.push(el);
		});
		return unique.length === 1 || unique.length === 3;

	},

	isSet4: function(arr) {
		var unique = [];
		$.each(arr, function(i, el){
			if($.inArray(el, unique) === -1) unique.push(el);
		});

		return unique.length === 1 || unique.length === 4;

	},

	isSet5: function(arr) {
		var unique = [];
		$.each(arr, function(i, el){
			if($.inArray(el, unique) === -1) unique.push(el);
		});
		return unique.length === 1 || unique.length === 5;

	},





	silentSubmission0: function() {
		var valid = this.validateSet0();
		if (valid) {
			this.submitSet0();
		}
	},
	silentSubmission: function() {
		var valid = this.validateSet();
		if (valid) {
			this.submitSet();
		}
	},
	silentSubmission1: function() {
		var valid = this.validateSet35();
		if (valid) {
			this.submitSet1();
		}
	},
	silentSubmission2: function() {
		var valid = this.validateSet44();
		if (valid) {
			this.submitSet2();
		}
	},
	silentSubmission3: function() {
		var valid = this.validateSet45();
		if (valid) {
			this.submitSet3();
		}
	},
	silentSubmission4: function() {
		var valid = this.validateSet55();
		if (valid) {
			this.submitSet4();
		}
	},





	silentSubmissionHS: function() {
		var valid = this.validateHyperSet();
		if (valid) {
			this.submitSetHS();
		}
	},
	silentSubmissionHS0: function() {
		var valid = this.validateHyperSet0();
		if (valid) {
			this.submitSetHS0();
		}
	},
	silentSubmissionHS1: function() {
		var valid = this.validateHyperSet1();
		if (valid) {
			this.submitSetHS1();
		}
	},
	silentSubmissionHS2: function() {
		var valid = this.validateHyperSet2();
		if (valid) {
			this.submitSetHS2();
		}
	},
    silentSubmissionHS3: function() {
        var valid = this.validateHyperSet3();
        if (valid) {
            this.submitSetHS3();
        }
    },
    silentSubmissionHS4: function() {
        var valid = this.validateHyperSet4();
        if (valid) {
            this.submitSetHS4();
        }
    },





	submitSet0: function() {
		var self = this;
		var ids = $.map(self.selected0, function(el) { return $(el).data("id");});


		// ajax request to get initial set of cards
		var newCardRequest = $.ajax({
			url: 'set.php?action=submit0',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				self.clearCards0(ids, data);

				// to do - implement game complete check on server
				if (!data.gameComplete) {
					self.set0 = 0;
					//self.updateCards(data);
					self.existingSet0();
					self.increaseScore0();
                    if (self.cards0.length !== 0 && self.set0 === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
                    if (self.score0 === 9){
                        document.getElementById('GameWon').style.display='block';
                    }
                    console.log(self.score0 + self.nbScore0);
                    if (self.score0 == self.nbScore0){
                        document.getElementById('GameWon').style.display='block';
                    }

				} else {
					self.gameWon();
				}
			},
			error: function() {
				console.log(arguments);
			}
		});

		this.clearSelections(this.selected0);
	},

	submitSet: function() {
		var self = this;
		var ids = $.map(self.selected, function(el) { return $(el).data("id");});

		// ajax request to get initial set of cards
		var newCardRequest = $.ajax({
			url: 'set.php?action=submit',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				self.clearCards(ids, data);

				// to do - implement game complete check on server
				if (!data.gameComplete) {
					self.set = 0;
					//self.updateCards(data);
					self.existingSet();
					self.increaseScore();
                    if (self.cards.length !== 0 && self.set === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
                    if (self.score === 27){
                        document.getElementById('GameWon').style.display='block';
                    }
				} else {
					self.gameWon();
				}
			},
			error: function() {
				console.log(arguments);
			}
		});

        this.clearSelections(this.selected);
	},

	submitSet1: function() {
		var self = this;
		var ids = $.map(self.selected1, function(el) { return $(el).data("id");});

		// ajax request to get initial set of cards
		var newCardRequest = $.ajax({
			url: 'set.php?action=submit1',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				self.clearCards1(ids,data);
				// to do - implement game complete check on server
				if (!data.gameComplete) {
					self.set1 = 0;
					//self.updateCards1(data);
					self.existingSet1();
					self.increaseScore1();
                    if (self.cards1.length !== 0 && self.set1 === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
                    if (self.score1 === 81){
                        document.getElementById('GameWon').style.display='block';
                    }
				} else {
					self.gameWon();
				}
			},
			error: function() {
				console.log(arguments);
			}
		});

        this.clearSelections(this.selected1);
	},


	submitSet2: function() {
		var self = this;
		var ids = $.map(self.selected2, function(el) { return $(el).data("id");});

		// ajax request to get initial set of cards
		var newCardRequest = $.ajax({
			url: 'set.php?action=submit2',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				self.set2 = 0;
				self.clearCards2(ids,data);
				// to do - implement game complete check on server
				if (!data.gameComplete) {

					//self.updateCards2(data);
					self.existingSet2();
					self.increaseScore2();
                    if (self.cards2.length !== 0 && self.set2 === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
                    if (self.score2 === 64){
                        document.getElementById('GameWon').style.display='block';
                    }
				} else {
					self.gameWon();
				}
			},
			error: function() {
				console.log(arguments);
			}
		});

        this.clearSelections(this.selected2);
	},


	submitSet3: function() {
		var self = this;
		var ids = $.map(self.selected3, function(el) { return $(el).data("id");});

		// ajax request to get initial set of cards
		var newCardRequest = $.ajax({
			url: 'set.php?action=submit3',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				self.clearCards3(ids,data);
				// to do - implement game complete check on server
				if (!data.gameComplete) {
					self.set3 = 0;
					//self.updateCards3(data);
					self.existingSet3();
					self.increaseScore3();
                    if (self.cards3.length !== 0 && self.set3 === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
                    if (self.score3 === 256){
                        document.getElementById('GameWon').style.display='block';
                    }
				} else {
					self.gameWon();
				}
			},
			error: function() {
				console.log(arguments);
			}
		});

        this.clearSelections(this.selected3);
	},



	submitSet4: function() {
		var self = this;
		var ids = $.map(self.selected4, function(el) { return $(el).data("id");});

		// ajax request to get initial set of cards
		var newCardRequest = $.ajax({
			url: 'set.php?action=submit4',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				console.log(data);
				self.clearCards4(ids,data);
				// to do - implement game complete check on server
				if (!data.gameComplete) {
					self.set4 = 0;
					//self.updateCards4(data);
					self.existingSet4();
					self.increaseScore4();
                    if (self.cards4.length !== 0 && self.set4 === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
                    if (self.score3 === 625){
                        document.getElementById('GameWon').style.display='block';
                    }
				} else {
					self.gameWon();
				}
			},
			error: function() {
				console.log(arguments);
			}
		});

        this.clearSelections(this.selected4);
	},



	submitSetHS: function() {
		var self = this;
		var ids = $.map(self.selectedHS, function(el) { return $(el).data("id");});


		// ajax request to get initial set of cards
		var newCardRequest = $.ajax({
			url: 'set.php?action=submitHS',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				self.clearCardsHS(ids, data);

				// to do - implement game complete check on server
				if (!data.gameComplete) {
					self.setHS = 0;
					//self.updateCards(data);
					self.existingHyperSet();
					self.increaseScoreHS();
                    if (self.cardsHS.length !== 0 && self.setHS === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
				} else {
					self.gameWon();
				}
			},
			error: function() {
				console.log(arguments);
			}
		});

        this.clearSelections(this.selectedHS);
	},


	submitSetHS0: function() {
		var self = this;
		var ids = $.map(self.selectedHS0, function(el) { return $(el).data("id");});


		// ajax request to get initial set of cards
		var newCardRequest = $.ajax({
			url: 'set.php?action=submitHS0',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				self.clearCardsHS0(ids, data);

				// to do - implement game complete check on server
				if (!data.gameComplete) {
					self.setHS0 = 0;
					//self.updateCards(data);
					self.existingHyperSet0();
					self.increaseScoreHS0();
                    if (self.cardsHS0.length !== 0 && self.setHS0 === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
				} else {
					self.gameWon();
				}
			},
			error: function() {
				console.log(arguments);
			}
		});

        this.clearSelections(this.selectedHS0);
	},

	submitSetHS1: function() {
		var self = this;
		var ids = $.map(self.selectedHS1, function(el) { return $(el).data("id");});


		// ajax request to get initial set of cards
		var newCardRequest = $.ajax({
			url: 'set.php?action=submitHS1',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				self.clearCardsHS1(ids, data);

				// to do - implement game complete check on server
				if (!data.gameComplete) {
					self.setHS1 = 0;
					//self.updateCards(data);
					self.existingHyperSet1();
					self.increaseScoreHS1();
                    if (self.cardsHS1.length !== 0 && self.setHS1 === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
				} else {
					self.gameWon();
				}
			},
			error: function() {
				console.log(arguments);
			}
		});

        this.clearSelections(this.selectedHS1);
	},
	submitSetHS2: function() {
		var self = this;
		var ids = $.map(self.selectedHS2, function(el) { return $(el).data("id");});


		// ajax request to get initial set of cards
		var newCardRequest = $.ajax({
			url: 'set.php?action=submitHS2',
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				self.clearCardsHS2(ids, data);

				// to do - implement game complete check on server
				if (!data.gameComplete) {
					self.setHS2 = 0;
					//self.updateCards(data);
					self.existingHyperSet2();
					self.increaseScoreHS2();
                    if (self.cardsHS2.length !== 0 && self.setHS2 === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
				} else {
					self.gameWon();
				}
			},
			error: function() {
				console.log(arguments);
			}
		});

        this.clearSelections(this.selectedHS2);
	},
    submitSetHS3: function() {
        var self = this;
        var ids = $.map(self.selectedHS3, function(el) { return $(el).data("id");});


        // ajax request to get initial set of cards
        var newCardRequest = $.ajax({
            url: 'set.php?action=submitHS3',
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                self.clearCardsHS3(ids, data);

                // to do - implement game complete check on server
                if (!data.gameComplete) {
                    self.setHS3 = 0;
                    //self.updateCards(data);
                    self.existingHyperSet3();
                    self.increaseScoreHS3();
                    if (self.cardsHS3.length !== 0 && self.setHS3 === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
                } else {
                    self.gameWon();
                }
            },
            error: function() {
                console.log(arguments);
            }
        });

        this.clearSelections(this.selectedHS3);
    },

    submitSetHS4: function() {
        var self = this;
        var ids = $.map(self.selectedHS4, function(el) { return $(el).data("id");});


        // ajax request to get initial set of cards
        var newCardRequest = $.ajax({
            url: 'set.php?action=submitHS4',
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                self.clearCardsHS4(ids, data);

                // to do - implement game complete check on server
                if (!data.gameComplete) {
                    self.setHS4 = 0;
                    //self.updateCards(data);
                    self.existingHyperSet4();
                    self.increaseScoreHS4();
                    if (self.cardsHS4.length !== 0 && self.setHS4 === 0){
                        document.getElementById('GameOver2').style.display='block';
                    }
                } else {
                    self.gameWon();
                }
            },
            error: function() {
                console.log(arguments);
            }
        });

        this.clearSelections(this.selectedHS4);
    },





    remplacerCarte : function(location, card, setOfCards){
	    setOfCards.splice(location,1,card);
    },






	clearCards0: function(ids,data) {
		var self = this;
		this.selected0 = [];
		this.$board0.empty();
		var cardIds = $.map(self.cards0, function(card) { return card.id; });
		$.each(ids, function(idx, id) {
			var location = cardIds.indexOf(id);
			if (location > -1) {
				cardIds.splice(location, 1);
				if(data[idx] != null){
				    self.remplacerCarte(location,data[idx],self.cards0);
					cardIds = $.map(self.cards0, function(card) { return card.id; });
				}
				else{
					self.cards0.splice(location, 1);


				}

			}
		});
		this.displayCards0();
		if(self.cards0.length === 0){
			alert("you won!");
		}
	},

	clearCards: function(ids,data) {
		var self = this;
		this.selected = [];
		this.$board.empty();
		var cardIds = $.map(self.cards, function(card) { return card.id; });
		$.each(ids, function(idx, id) {
			var location = cardIds.indexOf(id);
			if (location > -1) {
				cardIds.splice(location, 1);
				if(data[idx] != null){
                    self.remplacerCarte(location,data[idx],self.cards);
					cardIds = $.map(self.cards, function(card) { return card.id; });
				}
				else{
					self.cards.splice(location, 1);
				}
			}
		});
		this.displayCards();
		if(self.cards.length === 0){
			alert("you won!");
		}
	},
	clearCards1: function(ids,data) {
		// remove submitted cards game's card array and clear the board
		var self = this;
		this.selected1 = [];
		this.$board1.empty();
		var cardIds = $.map(self.cards1, function(card) { return card.id; });
		$.each(ids, function(idx, id) {
			var location = cardIds.indexOf(id);
			if (location > -1) {
				cardIds.splice(location, 1);
				if(data[idx] != null){
                    self.remplacerCarte(location,data[idx],self.cards1);
					cardIds = $.map(self.cards1, function(card) { return card.id; });
				}
				else{
					self.cards1.splice(location, 1);
				}
			}
		});
		this.displayCards1();
		if(self.cards1.length === 0){
			alert("you won!");
		}
	},
	clearCards2: function(ids, data) {
		// remove submitted cards game's card array and clear the board
		var self = this;
		this.selected2 = [];
		this.$board2.empty();
		var cardIds = $.map(self.cards2, function(card) { return card.id; });
		$.each(ids, function(idx, id) {
			var location = cardIds.indexOf(id);
			if (location > -1) {
				cardIds.splice(location, 1);
				if(data[idx] != null){
                    self.remplacerCarte(location,data[idx],self.cards2);
					cardIds = $.map(self.cards2, function(card) { return card.id; });
				}
				else{
					self.cards2.splice(location, 1);
				}
			}
		});
		this.displayCards2();
		if(self.cards2.length === 0){
			alert("you won!");
		}
	},
	clearCards3: function(ids,data) {
		// remove submitted cards game's card array and clear the board
		var self = this;
		this.selected3 = [];
		this.$board3.empty();
		var cardIds = $.map(self.cards3, function(card) { return card.id; });
		$.each(ids, function(idx, id) {
			var location = cardIds.indexOf(id);
			if (location > -1) {
				cardIds.splice(location, 1);
				if(data[idx] != null){
                    self.remplacerCarte(location,data[idx],self.cards3);
					cardIds = $.map(self.cards3, function(card) { return card.id; });
				}
				else{
					self.cards3.splice(location, 1);
				}
			}
		});
		this.displayCards3();
		if(self.cards3.length === 0){
			alert("you won!");
		}
	},
	clearCards4: function(ids,data) {
		// remove submitted cards game's card array and clear the board
		var self = this;
		this.selected4 = [];
		this.$board4.empty();
		var cardIds = $.map(self.cards4, function(card) { return card.id; });
		$.each(ids, function(idx, id) {
			var location = cardIds.indexOf(id);
			if (location > -1) {
				cardIds.splice(location, 1);
				if(data[idx] != null){
                    self.remplacerCarte(location,data[idx],self.cards4);
					cardIds = $.map(self.cards4, function(card) { return card.id; });
				}
				else{
					self.cards4.splice(location, 1);
				}
			}
		});
		this.displayCards4();
		if(self.cards4.length === 0){
			alert("you won!");
		}
	},

	clearCardsHS: function(ids,data) {
		var self = this;
		this.selectedHS = [];
		this.$boardHS.empty();
		var cardIds = $.map(self.cardsHS, function(card) { return card.id; });
		$.each(ids, function(idx, id) {
			var location = cardIds.indexOf(id);
			if (location > -1) {
				cardIds.splice(location, 1);
				if(data[idx] != null){
                    self.remplacerCarte(location,data[idx],self.cardsHS);
					cardIds = $.map(self.cardsHS, function(card) { return card.id; });
				}
				else{
					self.cardsHS.splice(location, 1);


				}

			}
		});
		this.displayCardsHS();
		if(self.cardsHS.length === 0){
			alert("you won!");
		}
	},

	clearCardsHS0: function(ids,data) {
		var self = this;
		this.selectedHS0 = [];
		this.$boardHS0.empty();
		var cardIds = $.map(self.cardsHS0, function(card) { return card.id; });
		$.each(ids, function(idx, id) {
			var location = cardIds.indexOf(id);
			if (location > -1) {
				cardIds.splice(location, 1);
				if(data[idx] != null){
                    self.remplacerCarte(location,data[idx],self.cardsHS0);
					cardIds = $.map(self.cardsHS0, function(card) { return card.id; });
				}
				else{
					self.cardsHS0.splice(location, 1);

				}
			}
		});
		this.displayCardsHS0();
		if(self.cardsHS0.length === 0){
			alert("you won!");
		}
	},


	clearCardsHS1: function(ids,data) {
		var self = this;
		this.selectedHS1 = [];
		this.$boardHS1.empty();
		var cardIds = $.map(self.cardsHS1, function(card) { return card.id; });
		$.each(ids, function(idx, id) {
			var location = cardIds.indexOf(id);
			if (location > -1) {
				cardIds.splice(location, 1);
				if(data[idx] != null){
                    self.remplacerCarte(location,data[idx],self.cardsHS1);
					cardIds = $.map(self.cardsHS1, function(card) { return card.id; });
				}
				else{
					self.cardsHS1.splice(location, 1);

				}
			}
		});
		this.displayCardsHS1();
		if(self.cardsHS1.length === 0){
			alert("you won!");
		}
	},
	clearCardsHS2: function(ids,data) {
		var self = this;
		this.selectedHS2 = [];
		this.$boardHS2.empty();
		var cardIds = $.map(self.cardsHS2, function(card) { return card.id; });
		$.each(ids, function(idx, id) {
			var location = cardIds.indexOf(id);
			if (location > -1) {
				cardIds.splice(location, 1);
				if(data[idx] != null){
                    self.remplacerCarte(location,data[idx],self.cardsHS2);
					cardIds = $.map(self.cardsHS2, function(card) { return card.id; });
				}
				else{
					self.cardsHS2.splice(location, 1);
				}
			}
		});
		this.displayCardsHS2();
		if(self.cardsHS2.length === 0){
			alert("you won!");
		}
	},

    clearCardsHS3: function(ids,data) {
        var self = this;
        this.selectedHS3 = [];
        this.$boardHS3.empty();
        var cardIds = $.map(self.cardsHS3, function(card) { return card.id; });
        $.each(ids, function(idx, id) {
            var location = cardIds.indexOf(id);
            if (location > -1) {
                cardIds.splice(location, 1);
                if(data[idx] != null){
                    self.remplacerCarte(location,data[idx],self.cardsHS3);
                    cardIds = $.map(self.cardsHS3, function(card) { return card.id; });
                }
                else{
                    self.cardsHS3.splice(location, 1);
                }
            }
        });
        this.displayCardsHS3();
        if(self.cardsHS3.length === 0){
            alert("you won!");
        }
    },

    clearCardsHS4: function(ids,data) {
        var self = this;
        this.selectedHS4 = [];
        this.$boardHS4.empty();
        var cardIds = $.map(self.cardsHS4, function(card) { return card.id; });
        $.each(ids, function(idx, id) {
            var location = cardIds.indexOf(id);
            if (location > -1) {
                cardIds.splice(location, 1);
                if(data[idx] != null){
                    self.remplacerCarte(location,data[idx],self.cardsHS4);
                    cardIds = $.map(self.cardsHS4, function(card) { return card.id; });
                }
                else{
                    self.cardsHS4.splice(location, 1);
                }
            }
        });
        this.displayCardsHS4();
        if(self.cardsHS4.length === 0){
            alert("you won!");
        }
    },


	increaseScore0: function() {
		this.$score0.html(++this.score0);
	},
	increaseScore: function() {
		this.$score.html(++this.score);
	},
	increaseScore1: function() {
		this.$score1.html(++this.score1);
	},

	increaseScore2: function() {
		this.$score2.html(++this.score2);
	},

	increaseScore3: function() {
		this.$score3.html(++this.score3);
	},

	increaseScore4: function() {
		this.$score4.html(++this.score4);
	},
	increaseScoreHS: function() {
		this.$scoreHS.html(++this.scoreHS);
	},
	increaseScoreHS0: function() {
		this.$scoreHS0.html(++this.scoreHS0);
	},
	increaseScoreHS1: function() {
		this.$scoreHS1.html(++this.scoreHS1);
	},
	increaseScoreHS2: function() {
		this.$scoreHS2.html(++this.scoreHS2);
	},
    increaseScoreHS3: function() {
        this.$scoreHS3.html(++this.scoreHS3);
    },
    increaseScoreHS4: function() {
        this.$scoreHS4.html(++this.scoreHS4);
    },

	startRound: function() {
		// todo
		// reset timer to 30 seconds
	},

	gameWon: function() {
		alert("you won!");
	},

	gameLost: function() {
		alert("you lost :(");
	}

};





/*
var canvases = document.getElementsByClassName(' red solid');

console.log(canvases);
for (let canvas of canvases) {
	console.log(canvas);

}


/*
var canvases = $('.red');
console.log(canvases);

canvases.each(function(i, c) {
	var context = c.getContext('2d');
	console.log(c);

	drawDiamond(context, 50, 50, 75, 100);
	context.fillStyle = "red";
	context.fill();
});

function drawDiamond(context, x, y, width, height){
	context.save();
	context.beginPath();
	context.moveTo(x, y);
	// top left edge
	context.lineTo(x - width / 2, y + height / 2);
	// bottom left edge
	context.lineTo(x, y + height);
	// bottom right edge
	context.lineTo(x + width / 2, y + height / 2);

	// closing the path automatically creates
	// the top right edge
	context.closePath();


	context.restore();
}

/*
var canvases = $( ".red" );

for (let canvas of canvases) {
	var ctx = canvas.getContext('2d');
	ctx.fillStyle = 'red';
	ctx.fillRect(10, 10, 100, 100);
}

/*
var canvases = document.getElementsByClassName('shape diamond red solid');
console.log(canvases);
for (let canvas of canvases) {
	var context = canvas.getContext('2d');

	drawDiamond(context, 50, 50, 75, 100);
	canvas.fillStyle("#FF0000");
	ctx.fillRect(0, 0, canvas.width, canvas.height);
}


function drawDiamond(context, x, y, width, height){
	context.save();
	context.beginPath();
	context.moveTo(x, y);
	// top left edge
	context.lineTo(x - width / 2, y + height / 2);
	// bottom left edge
	context.lineTo(x, y + height);
	// bottom right edge
	context.lineTo(x + width / 2, y + height / 2);

	// closing the path automatically creates
	// the top right edge
	context.closePath();

	context.fillStyle = "red";
	context.fill();
	context.restore();
}*/