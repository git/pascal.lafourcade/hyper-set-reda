<?php


class ScoreGateway
{

    private $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
    }

    public function getScore(){
        $sql = 'SELECT * FROM highScore order by score desc;';
        $scores = [];
        $stmt = $this->pdo->query($sql);

        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $scores [] = [
                'id' => $row ['id'],
                'pseudo' => $row ['pseudo'],
                'score' => $row ['score']
                ];

        }

        return $scores;
    }

    public function insert($pseudo,$score){
        $id = null;
        $sql = 'INSERT INTO highScore (id, pseudo, score) VALUES(:id, :pseudo,:score)';
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':pseudo', $pseudo);
        $stmt->bindValue(':score', $score);
        $stmt->execute();

    }
}