<?php

class SQLiteConnection {

    private $pdo;


    public function connect() {
        if ($this->pdo == null) {
            $this->pdo = new \PDO("sqlite:" . 'hyperSet.db');
        }
        return $this->pdo;
    }
}