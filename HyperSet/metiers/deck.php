<?php

include_once "cardAttributes.php";
include_once "card.php";
include_once "functions.php";

class Deck {

	public $cards = array();

	public function __construct($changes) {
		$this->createDeck($changes);
	}

	private function createDeck($changes)
    {


        if ($changes === 'deal') {
            $colors = array('green', 'red', 'purple');
            $shapes = array('oval', 'diamond', 'wave');
            $fills = array('solid', 'stripped', 'open');
            $border = 'simple';
            $numbers = array(1, 2, 3);

            $index = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    foreach ($fills as $fill) {
                        foreach ($numbers as $number) {
                            $cardAttributes = new CardAttributes($color, $shape, $fill, $border, $number, $index);
                            $card = new Card($cardAttributes, $this);
                            $index++;
                        }
                    }
                }

            }

        }elseif ($changes === 'deal1') {
            $colors = array('green', 'red', 'purple');
            $shapes = array('oval', 'diamond', 'wave');
            $fills = array('solid', 'stripped', 'open');
            $borders = array('plein', 'point', 'rond');
            $numbers = array(1, 2, 3);

            $index1 = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    foreach ($fills as $fill) {
                        foreach ($borders as $border) {
                            foreach ($numbers as $number) {
                                $cardAttributes = new CardAttributes($color, $shape, $fill, $border, $number, $index1);
                                $card = new Card($cardAttributes, $this);
                                $index1++;
                            }
                        }
                    }
                }

            }
        }elseif ($changes === 'deal2') {
            $colors = array('green', 'red', 'purple', 'lightblue');
            $shapes = array('oval', 'diamond', 'wave', 'rectangle');
            $fills = array('solid', 'stripped', 'open','quadrillage');
            $numbers = array(1, 2, 3,4);
            $border = 'simple';

            $index2 = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    foreach ($fills as $fill) {
                        foreach ($numbers as $number) {
                            $cardAttributes = new CardAttributes($color, $shape, $fill,$border, $number, $index2);
                            $card = new Card($cardAttributes, $this);
                            $index2++;
                        }
                    }
                }
            }
        }elseif ($changes === 'deal3') {
            $colors = array('green', 'red', 'purple', 'lightblue');
            $shapes = array('oval', 'diamond', 'wave', 'rectangle');
            $fills = array('solid', 'stripped', 'open','quadrillage');
            $borders = array ('plein','point','rond','zigzag');
            $numbers = array(1, 2, 3,4);

            $index3 = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    foreach ($fills as $fill) {
                        foreach ($borders as $border){
                            foreach ($numbers as $number) {
                                $cardAttributes = new CardAttributes($color, $shape, $fill, $border, $number, $index3);
                                $card = new Card($cardAttributes, $this);
                                $index3++;
                            }
                        }
                    }
                }

            }
        }elseif ($changes === 'deal4') {
            $colors = array('green', 'red', 'purple', 'lightblue', 'yellow');
            $shapes = array('oval', 'diamond', 'wave', 'rectangle','triangle');
            $fills = array('solid', 'stripped', 'open', 'quadrillage','pointille');
            $borders = array('plein', 'point', 'rond', 'zigzag','hachure');
            $numbers = array(1, 2, 3, 4, 5);

            $index4 = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    foreach ($fills as $fill) {
                        foreach ($borders as $border) {
                            foreach ($numbers as $number) {
                                $cardAttributes = new CardAttributes($color, $shape, $fill, $border,$number, $index4);
                                $card = new Card($cardAttributes, $this);
                                $index4++;
                            }
                        }
                    }
                }

            }
        }elseif ($changes === 'deal0') {
            $colors = array('green', 'red', 'purple');
            $shapes = array('oval', 'diamond', 'wave');
            $fill = 'open';
            $border = 'simple';
            $numbers = array(1, 2, 3);


            $index0 = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    //foreach ($fills as $fill) {
                        //foreach ($borders as $border) {
                            foreach ($numbers as $number) {
                                $cardAttributes = new CardAttributes($color, $shape, $fill, $border, $number, $index0);
                                $card = new Card($cardAttributes, $this);
                                $index0++;
                            }
                        //}
                    //}
                }

            }
        }
        elseif ($changes === 'dealHS') {
            $colors = array('green', 'red', 'purple');
            $shapes = array('oval', 'diamond', 'wave');
            $fills = array('solid', 'stripped', 'open');
            $border = 'simple';
            $numbers = array(1, 2, 3);

            $index = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    foreach ($fills as $fill) {
                        foreach ($numbers as $number) {
                            $cardAttributes = new CardAttributes($color, $shape, $fill, $border, $number, $index);
                            $card = new Card($cardAttributes, $this);
                            $index++;
                        }
                    }
                }

            }
        }elseif ($changes === 'dealHS0') {
            $colors = array('green', 'red', 'purple');
            $shapes = array('oval', 'diamond', 'wave');
            $fill = 'open';
            $border = 'simple';
            $numbers = array(1, 2, 3);


            $index0 = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    //foreach ($fills as $fill) {
                    //foreach ($borders as $border) {
                    foreach ($numbers as $number) {
                        $cardAttributes = new CardAttributes($color, $shape, $fill, $border, $number, $index0);
                        $card = new Card($cardAttributes, $this);
                        $index0++;
                    }
                    //}
                    //}
                }

            }
        }elseif ($changes === 'dealHS1') {
            $colors = array('green', 'red', 'purple');
            $shapes = array('oval', 'diamond', 'wave');
            $fills = array('solid', 'stripped', 'open');
            $borders = array('plein', 'point', 'rond');
            $numbers = array(1, 2, 3);

            $index1 = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    foreach ($fills as $fill) {
                        foreach ($borders as $border) {
                            foreach ($numbers as $number) {
                                $cardAttributes = new CardAttributes($color, $shape, $fill, $border, $number, $index1);
                                $card = new Card($cardAttributes, $this);
                                $index1++;
                            }
                        }
                    }
                }

            }
        }elseif ($changes === 'dealHS2') {
            $colors = array('green', 'red', 'purple', 'lightblue');
            $shapes = array('oval', 'diamond', 'wave', 'rectangle');
            $fills = array('solid', 'stripped', 'open','quadrillage');
            $numbers = array(1, 2, 3,4);
            $border = 'simple';

            $index2 = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    foreach ($fills as $fill) {
                        foreach ($numbers as $number) {
                            $cardAttributes = new CardAttributes($color, $shape, $fill,$border, $number, $index2);
                            $card = new Card($cardAttributes, $this);
                            $index2++;
                        }
                    }
                }
            }
        }elseif ($changes === 'dealHS3') {
            $colors = array('green', 'red', 'purple', 'lightblue');
            $shapes = array('oval', 'diamond', 'wave', 'rectangle');
            $fills = array('solid', 'stripped', 'open','quadrillage');
            $borders = array ('plein','point','rond','zigzag');
            $numbers = array(1, 2, 3, 4);

            $index3 = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    foreach ($fills as $fill) {
                        foreach ($borders as $border){
                            foreach ($numbers as $number) {
                                $cardAttributes = new CardAttributes($color, $shape, $fill, $border, $number, $index3);
                                $card = new Card($cardAttributes, $this);
                                $index3++;
                            }
                        }
                    }
                }

            }
        } elseif ($changes === 'dealHS4') {
            $colors = array('green', 'red', 'purple', 'lightblue', 'yellow');
            $shapes = array('oval', 'diamond', 'wave', 'rectangle','triangle');
            $fills = array('solid', 'stripped', 'open', 'quadrillage','pointille');
            $borders = array('plein', 'point', 'rond', 'zigzag','hachure');
            $numbers = array(1, 2, 3, 4, 5);

            $index4 = 1;

            foreach ($colors as $color) {
                foreach ($shapes as $shape) {
                    foreach ($fills as $fill) {
                        foreach ($borders as $border) {
                            foreach ($numbers as $number) {
                                $cardAttributes = new CardAttributes($color, $shape, $fill, $border,$number, $index4);
                                $card = new Card($cardAttributes, $this);
                                $index4++;
                            }
                        }
                    }
                }

            }
        }
    }

	private function shuffle() {
		shuffle($this->cards);
	}



	public function deal($nbCartes) {
		// shuffle the deck
		$this->shuffle();

		// remove 12 cards from the top and return them
		$dealtCards = array_chop($this->cards, $nbCartes);
		return $dealtCards;
	}

	public function threeMore() {
		return array_chop($this->cards, 3);
	}

    public function fourMore() {
        return array_chop($this->cards, 4);
    }

    public function fiveMore() {
        return array_chop($this->cards, 5);
    }
    public function sixMore() {
        return array_chop($this->cards, 6);
    }
    public function eightMore() {
        return array_chop($this->cards, 8);
    }
}