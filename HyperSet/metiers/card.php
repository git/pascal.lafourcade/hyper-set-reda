<?php

class Card {

	public function __construct(CardAttributes $attributes, Deck $deck) {
		
		foreach ($attributes as $attribute => $value) {
			$this->{$attribute} = $value;
		}

		$deck->cards[] = $this;
	}
}