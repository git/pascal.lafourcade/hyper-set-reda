<?php


class ModelScore
{
    private $gwScore;

    public function __construct(){
        $pdo = (new SQLiteConnection())->connect();
        $this->gwScore= new ScoreGateway($pdo);

    }

    public function getHScore(){
        return $this->gwScore->getScore();

    }


}